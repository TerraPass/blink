#ifndef BLINK_IO_DATA_TILESETKEY_H
#define BLINK_IO_DATA_TILESETKEY_H

#include "io/types.h"

namespace blink
{
    namespace io
    {
        namespace data
        {
            using TilesetKey = BasicKey;

            struct TilesetResolvedKey final
            {
                static constexpr const auto DISPLAY_NAME = "resolved tileset key";

                const FilePath metadataFilePath;
                const FilePath imageFilePath;

                TilesetResolvedKey(
                    const FilePath metadataFilePath,
                    const FilePath imageFilePath
                )
                : metadataFilePath(metadataFilePath), imageFilePath(imageFilePath)
                {
                    
                }
            };
        }
    }
}

#endif /* BLINK_IO_DATA_TILESETKEY_H */

