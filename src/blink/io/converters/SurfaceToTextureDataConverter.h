#ifndef BLINK_IO_CONVERTERS_SURFACETOTEXTUREDATACONVERTER_H
#define BLINK_IO_CONVERTERS_SURFACETOTEXTUREDATACONVERTER_H

#include "io/data/TextureData.h"

#include <sdl2utils/pointers.h>

namespace blink
{
    namespace io
    {
        namespace converters
        {
            class SurfaceToTextureDataConverter final
            {
                SDL_Renderer* const prenderer;

            public:
                explicit SurfaceToTextureDataConverter(SDL_Renderer* const prenderer);

                data::TextureData convert(sdl2utils::SDL_SurfacePtr psurface) const;
            };
        }
    }
}

#endif /* BLINK_IO_CONVERTERS_SURFACETOTEXTUREDATACONVERTER_H */

