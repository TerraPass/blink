#ifndef BLINK_COMPONENTS_POSITION_H
#define BLINK_COMPONENTS_POSITION_H

#include "common/types.h"

namespace blink
{
    namespace components
    {
        using Position = WorldPosition;
    }
}

#endif /* BLINK_COMPONENTS_POSITION_H */

