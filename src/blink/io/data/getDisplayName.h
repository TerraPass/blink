#ifndef BLINK_IO_DATA_GETDISPLAYNAME_H
#define BLINK_IO_DATA_GETDISPLAYNAME_H

#include <type_traits>

// TODO: I don't like these includes here, to be quite honest...
// Is there a way to get rid of them?
#include <nlohmann_json.hpp>
#include <sdl2utils/pointers.h>
#include "FontData.h"

#include "debug/typeinfo.h"

namespace blink
{
    namespace io
    {
        namespace data
        {
            namespace detail
            {
                template <typename Data>
                struct DisplayName final
                {

                };

// Display name specializations{
                template <>
                struct DisplayName<nlohmann::json> final
                {
                    static constexpr const auto value = "JSON";
                };

                template <>
                struct DisplayName<sdl2utils::SDL_SurfacePtr> final
                {
                    static constexpr const auto value = "SDL surface";
                };

                template <>
                struct DisplayName<sdl2utils::SDL_TexturePtr> final
                {
                    static constexpr const auto value = "SDL texture";
                };

                template <>
                struct DisplayName<typename FontData::FontPtr> final
                {
                    static constexpr const auto value = "SDL_ttf font";
                };
//}

                // SFINAE to determine whether a given type has DisplayName with value static constant specialized for it.
                template <typename Data>
                struct HasDisplayNameSpecialization final
                {
                private:
                    template <typename T, typename = typename std::enable_if_t<!std::is_member_pointer<decltype(&T::value)>::value>>
                    static std::true_type test(int);

                    template <typename>
                    static std::false_type test(...);

                public:
                    static constexpr const bool value = decltype(test<DisplayName<Data>>(0))::value;
                };

                // SFINAE to determine whether a given type has a static const member named DISPLAY_NAME.
                template <typename Data>
                struct HasDisplayNameConstant final
                {
                private:
                    template <typename T, typename = typename std::enable_if_t<!std::is_member_pointer<decltype(&T::DISPLAY_NAME)>::value>>
                    static std::true_type test(int);

                    template <typename>
                    static std::false_type test(...);

                public:
                    static constexpr const bool value = decltype(test<Data>(0))::value;
                };

                // Shorthand for checking if no custom display name is provided and getDisplayName() should just use typename.
                template <typename Data>
                struct NoDisplayNameAvailable final
                {
                    static constexpr const bool value = !HasDisplayNameSpecialization<Data>::value && !HasDisplayNameConstant<Data>::value;
                };
            }

            // This is the default overload, which simply returns the typename of Data.
            // It will be used if neither a static member constant DISPLAY_NAME,
            // nor a specialization of blink::io::data::detail::DisplayName for type Data is present.
            // (If both DisplayName specialization and DISPLAY_NAME member constant are present, the former will be used.)
            template <typename Data>
            inline std::enable_if_t<detail::NoDisplayNameAvailable<Data>::value, std::string> getDisplayName()
            {
                return blink::debug::getTypename<Data>();
            }

            // This overload returns the value specified in blink::io::data::detail::DisplayName specialization for type Data.
            // It will be used if the specialization of blink::io::data::detail::DisplayName for type Data is present and has a value constant,
            // regardless of whether Data type parameter has a DISPLAY_NAME member constant defined.
            // (If both DisplayName specialization and DISPLAY_NAME member constant are present, the former will be used.)
            template <typename Data>
            inline std::enable_if_t<detail::HasDisplayNameSpecialization<Data>::value, std::string> getDisplayName()
            {
                return detail::DisplayName<Data>::value;
            }

            // This overload returns the value of the Data::DISPLAY_NAME constant.
            // It will be used if Data has a member constant named DISPLAY_NAME, but there is no blink::io::data::detail::DisplayName specialization for type Data.
            // (If both DisplayName specialization and DISPLAY_NAME member constant are present, the former will be used.)
            template <typename Data>
            inline std::enable_if_t<detail::HasDisplayNameConstant<Data>::value && !detail::HasDisplayNameSpecialization<Data>::value, std::string> getDisplayName()
            {
                return Data::DISPLAY_NAME;
            }
        }
    }
}

#endif /* BLINK_IO_DATA_GETDISPLAYNAME_H */

