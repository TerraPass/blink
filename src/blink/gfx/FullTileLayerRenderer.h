#ifndef BLINK_GFX_FULLTILELAYERRENDERER_H
#define BLINK_GFX_FULLTILELAYERRENDERER_H

#include <SDL.h>

#include "tiling/TileLayer.h"

namespace blink
{
    namespace gfx
    {
        class FullTileLayerRenderer final
        {
            SDL_Renderer* const prenderer;

        public:
            static constexpr const ScreenPosition DEFAULT_OFFSET{0,0};

            explicit FullTileLayerRenderer(SDL_Renderer* const prenderer)
            : prenderer(prenderer)
            {
                
            }

            void renderLayer(const tiling::TileLayer& tileLayer, const U8 scale, const ScreenPosition offset = DEFAULT_OFFSET) const;
        };
    }
}

#endif /* BLINK_GFX_FULLTILELAYERRENDERER_H */

