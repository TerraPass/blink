#include "Engine.h"

#include "core/CatchUpGameLoop.h"
#include "core/TimeKeeper.h"
#include "config/constants.h"
#include "core/GameWorld.h"

#include <cassert>

#include <boost/format.hpp>

namespace blink
{
#ifndef NDEBUG
    bool Engine::alreadyInstantiated = false;
#endif

    using boost::str;
    using boost::format;
    
    Engine::Engine() try
        : sdlSystem(), sdlWindow(config::WINDOW_TILE, config::WINDOW_WIDTH, config::WINDOW_HEIGHT),
            ptimeKeeper(new core::TimeKeeper(false)),
            pinputSystem(new input::InputSystem()),
            pcontentManager(new core::ContentManager(sdlWindow.getRendererPtr())),
            pgameLoop(
                new core::CatchUpGameLoop(
                    this->ptimeKeeper,
                    this->pinputSystem,
                    std::make_shared<core::GameWorld>(this->sdlWindow, *pcontentManager, *ptimeKeeper, *pinputSystem)
                )
            )
    {
        assert(!alreadyInstantiated && "blink::Engine may not be constructed more than once");
#ifndef NDEBUG
        alreadyInstantiated = true;
#endif

        // If so configured, trap the mouse cursor within the game window
        sdlWindow.trapMouseCursor(config::TRAP_MOUSE_CURSOR);

        // The following observers make it possible to quit the application
        this->pinputSystem->getAppShutdownRequestedEvent().subscribe(
            [this](auto&)
            {
                this->pgameLoop->requestExit();
            }
        );

        BLINK_LOG_I("Engine initialization has been completed");
    }
    catch(const sdl2utils::SDLErrorException& e)
    {
        throw Engine::InitializationFailedException(
            str(format("SDL initialization failed (%1%)") % e.what())
        );
    }

    Engine::~Engine()
    {
        BLINK_LOG_I("Deinitializing the engine...");
    }
    
    void Engine::runGameLoop()
    {
        BLINK_LOG_I("Running the game loop...");
        this->pgameLoop->operator()();
        BLINK_LOG_I("Returned from the game loop");
    }
    
    Engine::InitializationFailedException::InitializationFailedException(const std::string& detail)
        : std::runtime_error(str(format(MESSAGE_FORMAT) % detail))
    {
        
    }
}