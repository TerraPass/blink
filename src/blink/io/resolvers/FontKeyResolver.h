#ifndef BLINK_IO_RESOLVERS_FONTFILEPATHRESOLVER_H
#define BLINK_IO_RESOLVERS_FONTFILEPATHRESOLVER_H

#include "io/types.h"
#include "io/data/extractBasicKey.h"
#include "io/data/FontKey.h"

#include "FilePathResolver.h"

namespace blink
{
    namespace io
    {
        namespace resolvers
        {
            class FontKeyResolver final
            {
                const FilePathResolver resolverImpl;

            public:
                explicit FontKeyResolver(const std::string baseRelativePath, const std::string fileExtension)
                : resolverImpl(baseRelativePath, fileExtension)
                {
                    
                }

                data::FontResolvedKey resolve(const data::FontKey& fontKey) const
                {
                    return data::FontResolvedKey(resolverImpl.resolve(fontKey.getBasicKey()), fontKey.size);
                }
            };
        }
    }
}

#endif /* BLINK_IO_RESOLVERS_FONTFILEPATHRESOLVER_H */

