#ifndef BLINK_INPUT_EVENT_ARGS_H
#define BLINK_INPUT_EVENT_ARGS_H

#include <ostream>

#include "types.h"
#include "MousePosition.h"
#include "MouseButton.h"

namespace blink
{
    namespace input
    {
        struct MouseEventArgs final
        {
            MouseButton button;
            MousePosition position;

            MouseEventArgs(const MouseButton button, const MousePosition position)
            : button(button), position(position)
            {
                
            }
        };

        struct KeyboardEventArgs final
        {
            Scancode scancode;
            Keycode keycode;
            bool pressed;

            KeyboardEventArgs(const Scancode scancode, const Keycode keycode, const bool pressed)
            : scancode(scancode), keycode(keycode), pressed(pressed)
            {
                
            }
        };

        std::ostream& operator<<(std::ostream& out, const KeyboardEventArgs& args);
    }
}

#endif /* BLINK_INPUT_EVENT_ARGS_H */

