#ifndef BLINK_CORE_CONTENTMANAGER_H
#define BLINK_CORE_CONTENTMANAGER_H

#include <sdl2utils/pointers.h>

#include "io/loading.h"
#include "io/data/FontKey.h"
#include "gfx/Sprite.h"
#include "gfx/Animation.h"
#include "gfx/AnimatedSprite.h"
#include "gfx/AnimatedSpriteFactory.h"
#include "gfx/Font.h"
#include "gfx/FontFactory.h"
#include "tiling/Tilemap.h"
#include "tiling/TilemapFactory.h"

namespace blink
{
    namespace core
    {
        // TODO: Separate into SpriteManager, WhateverManager...
        // TODO: Create an interface for ContentManager.
        class ContentManager final
        {
            // TODO: Figure out where to put these currently hardcoded (and poorly placed) values!
            static constexpr const auto SPARK_KEY = "spark-sprite";
            static const io::data::FontKey FPS_COUNTER_FONT_KEY;
            static constexpr const gfx::Font::RenderMode FPS_COUNTER_FONT_RENDER_MODE = gfx::Font::RenderMode::SOLID;
            static const gfx::Color FPS_COUNTER_FONT_COLOR;
            static constexpr const auto TEST_TILEMAP_KEY = "test";

            io::TextureDataLoader textureDataLoader;
            io::AnimationDataLoader animDataLoader;
            io::FontDataLoader fontDataLoader;
            io::TilemapDataLoader tilemapDataLoader;

            gfx::AnimatedSpriteFactory animatedSpriteFactory;
            gfx::FontFactory fontFactory;
            tiling::TilemapFactory tilemapFactory;
            
            // TODO: Of course, in reality there will be collections (vectors?) of textures (Sprite objects?)
            // and animation frames (Animation objects?) for this class to manage.
            const gfx::AnimatedSprite sparkAnimatedSprite;
            const gfx::Font fpsCounterFont;
            const tiling::Tilemap testTilemap;
            
        public:
            explicit ContentManager(SDL_Renderer* const prenderer);

            // TODO: In reality these methods will not be dedicated to just sparks,
            // they will accept a key (string/enum) identifying the required content

            inline const gfx::AnimatedSprite& getSparkAnimatedSprite() const
            {
                return this->sparkAnimatedSprite;
            }

            inline const gfx::Font& getFpsCounterFont() const
            {
                return this->fpsCounterFont;
            }

            inline const tiling::Tilemap& getTestTilemap() const
            {
                return this->testTilemap;
            }
        };
    }
}

#endif /* BLINK_CORE_CONTENTMANAGER_H */

