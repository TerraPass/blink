#include "ResettableExecutionTimer.h"

namespace blink
{
    namespace time
    {
        void ResettableExecutionTimer::resetImpl(const bool startPaused)
        {
            this->setAccumulatedDuration((Duration)0);
            this->setStartTime(getCurrentTime());
            this->setPaused(startPaused);
        }
    }
}
