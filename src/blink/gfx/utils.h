#ifndef BLINK_GFX_UTILS_H
#define BLINK_GFX_UTILS_H

#include <sdl2utils/guards.h>

namespace blink
{
    namespace gfx
    {   
        static inline Rect getEntireTextureRect(SDL_Texture* const pspriteTexture)
        {
            Rect result;
            result.x = 0;
            result.y = 0;
            sdl2utils::guards::abbr::z(SDL_QueryTexture(pspriteTexture, nullptr, nullptr, &(result.w), &(result.h)));
            return result;
        }
    }
}

#endif /* BLINK_GFX_UTILS_H */

