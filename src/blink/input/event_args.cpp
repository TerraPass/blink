#include "event_args.h"

#include <boost/format.hpp>
#include <SDL.h>

namespace blink
{
    namespace input
    {
        using boost::format;
        using boost::str;

        std::ostream& operator<<(std::ostream& out, const KeyboardEventArgs& args)
        {
            out << (format("\"%1%\" (scancode: %2%, keycode: %3%, %4%pressed)")
                % SDL_GetKeyName(args.keycode)
                % args.scancode
                % args.keycode
                % (args.pressed ? "" : "not "));
            return out;
        }
    }
}
