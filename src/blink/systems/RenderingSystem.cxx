#ifdef BLINK_COMPILE_TEMPLATES
#include "RenderingSystem.h"

#include "camera/TopDownCamera.h"
#include "gfx/SpriteRenderer.h"
#endif

#include <cmath>
#include <algorithm>
#include <cassert>
#include <functional>

#include "components/Sprite.h"
#include "components/Position.h"

namespace blink
{
    namespace systems
    {
        template <typename Renderer, typename RenderPolicy>
        RenderingSystem<Renderer, RenderPolicy>::RenderingSystem(Renderer renderer, const size_t initialCapacity)
        : spriteRenderer(renderer), renderPolicy(), renderables()
        {
            renderables.reserve(initialCapacity);
        }

        template <typename Renderer, typename RenderPolicy>
        void RenderingSystem<Renderer, RenderPolicy>::configure(entityx::EntityManager& /*entities*/, entityx::EventManager& events)
        {
            events.subscribe<entityx::ComponentAddedEvent<components::Position>>(*this);
            events.subscribe<entityx::ComponentAddedEvent<components::Sprite>>(*this);
        }

        template <typename Renderer, typename RenderPolicy>
        void RenderingSystem<Renderer, RenderPolicy>::receive(const entityx::ComponentAddedEvent<components::Position>& event)
        {
            auto entity = event.entity;
            if(entity.has_component<components::Sprite>())
            {
                renderables.emplace_back(event.entity, event.component, entity.component<components::Sprite>());
            }
        }

        template <typename Renderer, typename RenderPolicy>
        void RenderingSystem<Renderer, RenderPolicy>::receive(const entityx::ComponentAddedEvent<components::Sprite>& event)
        {
            auto entity = event.entity;
            if(event.entity.has_component<components::Position>())
            {
                renderables.emplace_back(event.entity, entity.component<components::Position>(), event.component);
            }
        }

        template <typename Cont, typename Pred>
        static inline void removeWhere(Cont& container, Pred predicate)
        {
            container.erase(std::remove_if(container.begin(), container.end(), predicate), container.end());
        }

        template <typename Renderer, typename RenderPolicy>
        void RenderingSystem<Renderer, RenderPolicy>::update(entityx::EntityManager& /*entities*/, entityx::EventManager& /*events*/, entityx::TimeDelta /*dt*/)
        {
            // Remove invalidated renderables from the collection
            removeWhere(renderables, shouldBeRemoved);

            // Perform rendering according to the policy
            renderPolicy(
                spriteRenderer,
                renderables,
                lessPriority
            );
        }

#ifdef BLINK_COMPILE_TEMPLATES
        // Explicit template instantiations
        template class RenderingSystem<gfx::SpriteRenderer<camera::TopDownCamera>, rendering::SortingRender>;
#endif
    }
}
