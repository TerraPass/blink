#ifndef BLINK_IO_RETRIEVAL_TEXTURERETRIEVER_H
#define BLINK_IO_RETRIEVAL_TEXTURERETRIEVER_H

#include <sdl2utils/pointers.h>

#include "io/types.h"

#include "SurfaceRetriever.h"

namespace blink
{
    namespace io
    {
        namespace retrieval
        {
            class TextureRetriever final
            {
                SDL_Renderer* const prenderer;
                const SurfaceRetriever surfaceRetriever;

            public:
                explicit TextureRetriever(SDL_Renderer* const prenderer);

                using TexturePtr = sdl2utils::SDL_TexturePtr;

                TexturePtr retrieve(const FilePath& filePath) const;
            };
        }
    }
}

#endif /* BLINK_IO_RETRIEVAL_TEXTURERETRIEVER_H */

