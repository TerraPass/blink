#ifndef BLINK_GFX_TILEMAP_H
#define BLINK_GFX_TILEMAP_H

#include <vector>
#include <boost/variant.hpp>

#include "MapTilesContainer.h"
#include "TileLayer.h"
#include "types.h"

namespace blink
{
    namespace tiling
    {
        //template <typename PropertiesContainer, typename PropertiesContainerFactory>
        class Tilemap final
        {
            // TODO:
        //public:
            //using Layer = boost::variant<TileLayer, ObjectLayer>;

        private:
            //MapTilesContainerPtr ptiles;    // Why even store this here? Layers already have shared ownership of tiles container.
            //std::vector<Layer> layers;
            std::vector<TileLayer> tileLayers;  // Use previous instead of this later?

            // TODO: Store properties (possibly in type-erased form).

        public:
            explicit Tilemap(/*MapTilesContainerPtr ptiles, */std::vector<TileLayer>&& tileLayers)
            : /*ptiles(ptiles), */tileLayers(std::move(tileLayers))
            {
                // TODO: Sanity check layers?
            }

            Tilemap(const Tilemap&) = delete;
            Tilemap(Tilemap&&) = default;   // sic

            Tilemap& operator=(const Tilemap&) = delete;
            Tilemap& operator=(Tilemap&&) = delete;

            inline size_t getTileLayersCount() const
            {
                return tileLayers.size();
            }

            inline const TileLayer& getTileLayer(const size_t index) const
            {
                return tileLayers.at(index);
            }

            inline TileLayer& getTileLayer(const size_t index)
            {
                return tileLayers.at(index);
            }
        };
    }
}

#endif /* BLINK_GFX_TILEMAP_H */

