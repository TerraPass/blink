#ifdef BLINK_COMPILE_TEMPLATES
#include "sortingRender.h"

// The following headers (up to #endif) are needed for explicit template instantiations
#include <vector>

#include "gfx/SpriteRenderer.h"
#include "systems/RenderingSystem.h"
#include "camera/TopDownCamera.h"
#endif

#include <algorithm>
#include <cassert>

namespace blink
{
    namespace systems
    {
        namespace rendering
        {
            template <typename Renderer,
                typename RenderablesContainer,
                typename PriorityComparer>
            void sortingRender(
                const Renderer& renderer,
                RenderablesContainer& renderables, 
                PriorityComparer lessPriority
            )
            {
                // Render valid renderables and simultaneously perform insertion sort by priority.
                // (This means that render priority changes will only take effect on the *next* frame.)
                // This is a trade off to avoid an extra sorting step before rendering.
                // Insertion sort has close to linear complexity on nearly sorted sequences and,
                // since we have to iterate over renderables anyway to render them,
                // if the sequence is already sorted, the sort introduces virtually no overhead.
                for(size_t i = 0; i < renderables.size(); i++)
                {
                    auto& renderable = renderables[i];

                    assert(renderable.isValid() && "All renderables must be valid at this point");

                    // TODO: if(getPriority(renderable) > MIN_RENDERING_PRIORITY)
                    // Render
                    renderer.renderSprite(
                        renderable.getSprite(),
                        renderable.getPosition()
                    );

                    // Sort, if necessary
                    if(i > 0 && !lessPriority(renderables[i - 1], renderable))
                    {
                        const auto it = renderables.begin() + i;
                        std::rotate(
                            std::upper_bound(renderables.begin(), it, renderable, lessPriority),
                            it,
                            std::next(it)
                        );
                    }
                }

                // The following assertions were replaced with unit-tests in test/blink/systems/rendering/sortingRender.cpp
//                assert(
//                    std::is_sorted(renderables.begin(), renderables.end(), lessPriority)
//                    && "renderables collection must be sorted at exit from update()"
//                );
//                assert(
//                    std::adjacent_find(
//                        renderables.begin(),
//                        renderables.end(),
//                        [](const auto& ra, const auto& rb) {
//                            return ra.entity == rb.entity;
//                        }
//                    ) == renderables.end()
//                    && "renderables collection must not contain duplicates"
//                );
            }

#ifdef BLINK_COMPILE_TEMPLATES
            // Explicit template instantiations
            template void sortingRender<gfx::SpriteRenderer<camera::TopDownCamera>, 
                std::vector<SortingRenderingSystem<gfx::SpriteRenderer<camera::TopDownCamera>>::Renderable>, 
                bool (*)(const SortingRenderingSystem<gfx::SpriteRenderer<camera::TopDownCamera>>::Renderable&, const SortingRenderingSystem<gfx::SpriteRenderer<camera::TopDownCamera>>::Renderable&)>
            (
                gfx::SpriteRenderer<camera::TopDownCamera> const&, 
                std::vector<systems::SortingRenderingSystem<gfx::SpriteRenderer<camera::TopDownCamera>>::Renderable>&, 
                bool (*)(const SortingRenderingSystem<gfx::SpriteRenderer<camera::TopDownCamera>>::Renderable&, const SortingRenderingSystem<gfx::SpriteRenderer<camera::TopDownCamera>>::Renderable&)
            );
#endif
        }
    }
}