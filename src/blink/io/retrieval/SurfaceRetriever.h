#ifndef BLINK_IO_RETRIEVAL_SURFACERETRIEVER_H
#define BLINK_IO_RETRIEVAL_SURFACERETRIEVER_H

#include <sdl2utils/pointers.h>

#include "io/types.h"

namespace blink
{
    namespace io
    {
        namespace retrieval
        {
            class SurfaceRetriever final
            {
            public:
                using SurfacePtr = sdl2utils::SDL_SurfacePtr;

                SurfacePtr retrieve(const FilePath& filePath) const;
            };
        }
    }
}

#endif /* BLINK_IO_RETRIEVAL_SURFACERETRIEVER_H */

