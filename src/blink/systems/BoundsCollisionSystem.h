#ifndef BLINK_SYSTEMS_BOUNDSCOLLISIONSYSTEM_H
#define BLINK_SYSTEMS_BOUNDSCOLLISIONSYSTEM_H

#include <entityx/entityx.h>

#include "gfx/types.h"

namespace blink
{
    namespace systems
    {
        class BoundsCollisionSystem final : public entityx::System<BoundsCollisionSystem>
        {
            const I16 minX;
            const I16 minY;
            const I16 maxX;
            const I16 maxY;

        public:
            static constexpr const I16 DEFAULT_MIN_COORD = 0;
            
            BoundsCollisionSystem(const I16 minX, const I16 minY, const I16 maxX, const I16 maxY)
            : minX(minX), minY(minY), maxX(maxX), maxY(maxY)
            {
                
            }
            
            BoundsCollisionSystem(const I16 maxX, const I16 maxY)
            : BoundsCollisionSystem(DEFAULT_MIN_COORD, DEFAULT_MIN_COORD, maxX, maxY)
            {
                
            }

            explicit BoundsCollisionSystem(const gfx::Rect rect)
            : BoundsCollisionSystem(rect.x, rect.y, rect.x + rect.w, rect.y + rect.h)
            {
                
            }

            BoundsCollisionSystem(const BoundsCollisionSystem&) = delete;
            BoundsCollisionSystem(BoundsCollisionSystem&&) = default;   // sic
            
            BoundsCollisionSystem& operator=(const BoundsCollisionSystem&) = delete;
            BoundsCollisionSystem& operator=(BoundsCollisionSystem&&) = delete;

            virtual ~BoundsCollisionSystem() = default;

            virtual void update(entityx::EntityManager& entities, entityx::EventManager& events, entityx::TimeDelta dt) override;

        };
    }
}

#endif /* BLINK_SYSTEMS_BOUNDSCOLLISIONSYSTEM_H */

