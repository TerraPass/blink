#ifndef BLINK_COMPONENTS_BASELINE_H
#define BLINK_COMPONENTS_BASELINE_H

namespace blink
{
    namespace components
    {
        struct BaseLine
        {
            WorldPosition source;
            WorldPosition target;

            bool ray;

            BaseLine()
            : source(), target(), ray(false)
            {

            }

            BaseLine(const WorldPosition source, const WorldPosition target, const bool ray = false)
            : source(source), target(target), ray(ray)
            {

            }
        };
    }
}

#endif /* BLINK_COMPONENTS_BASELINE_H */

