#ifndef BLINK_SYSTEMS_MOVEMENTSYSTEM_H
#define BLINK_SYSTEMS_MOVEMENTSYSTEM_H

#include <entityx/entityx.h>

namespace blink
{
    namespace systems
    {
        class MovementSystem final : public entityx::System<MovementSystem>
        {
        public:
            MovementSystem() = default;
            MovementSystem(const MovementSystem&) = delete;
            MovementSystem(MovementSystem&&) = default; // sic
            
            MovementSystem& operator=(const MovementSystem&) = delete;
            MovementSystem& operator=(MovementSystem&&) = delete;

            virtual ~MovementSystem() = default;
            
            virtual void update(entityx::EntityManager& entities, entityx::EventManager& events, entityx::TimeDelta dt) override;
        };
    }
}

#endif /* BLINK_SYSTEMS_MOVEMENTSYSTEM_H */

