#ifndef BLINK_CORE_MIXEDTIMESTEPGAMELOOP_H
#define BLINK_CORE_MIXEDTIMESTEPGAMELOOP_H

#include <memory>

#include "IGameLoop.h"

#include "common/numeric.h"

#include "time/ExecutionTimer.h"

#include "input/InputSystem.h"

#include "ITimeKeeper.h"
#include "IGameWorld.h"

namespace blink
{
    namespace core
    {
        // This class implements the game loop
        // with fixed update timestep but variable rendering,
        // i.e. catch-up game loop.
        class CatchUpGameLoop : public IGameLoop
        {
            static constexpr const U8 UPDATES_PER_SECOND = 75;
            static constexpr const real SECONDS_PER_UPDATE = 
                1.0 / static_cast<real>(CatchUpGameLoop::UPDATES_PER_SECOND);

            static constexpr const U8 MAX_UPDATES_BETWEEN_RENDERS = 100;

            const std::shared_ptr<ITimeKeeper> ptimeKeeper;
            const std::shared_ptr<input::InputSystem> pinputSystem;
            const std::shared_ptr<IGameWorld> pgameWorld;
            
            bool isExitRequested;
            
        public:
            inline CatchUpGameLoop(
                const std::shared_ptr<ITimeKeeper> ptimeKeeper,
                const std::shared_ptr<input::InputSystem> pinputSystem,
                const std::shared_ptr<IGameWorld> pgameWorld
            ) : ptimeKeeper(ptimeKeeper), pinputSystem(pinputSystem), pgameWorld(pgameWorld), isExitRequested(false)
            {

            }
            
        private:
            virtual void run() override;
            
            inline virtual void requestExitImpl() override
            {
                this->isExitRequested = true;
            }

            void processInput();
            
            inline void updateWorld(const real deltaTime)
            {
                this->pgameWorld->updateWorld(deltaTime);
            }

            inline void renderWorld(const real deltaTime)
            {
                this->pgameWorld->renderWorld(deltaTime);
            }
        };
    }
}

#endif /* BLINK_CORE_MIXEDTIMESTEPGAMELOOP_H */

