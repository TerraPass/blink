#ifndef BLINK_COMMON_EXCEPTIONS_H
#define BLINK_COMMON_EXCEPTIONS_H

#include <stdexcept>

#include "debug/typeinfo.h"

namespace blink
{
    namespace common
    {
        // Base class for all exceptions, produced by Blink engine itself
        class BlinkException : public std::runtime_error
        {
        public:
            inline explicit BlinkException(const std::string& message)
                : std::runtime_error(message)
            {
                
            }
        };
    }
}

#endif /* BLINK_COMMON_EXCEPTIONS_H */

