#include "ScopedSDLSystem.h"

#include <cassert>

namespace blink
{
    namespace sdl
    {
        constexpr const Uint32 ScopedSDLSystem::sdlInitFlags;
        constexpr const int ScopedSDLSystem::sdlImageInitFlags;
        constexpr const int ScopedSDLSystem::sdlMixerInitFlags;

#ifndef NDEBUG
        bool ScopedSDLSystem::instanceExists = false;
#endif
    }
}
