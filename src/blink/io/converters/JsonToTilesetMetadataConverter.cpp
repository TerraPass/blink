#include "JsonToTilesetMetadataConverter.h"

#include "debug/logging.h"

#include "io/exceptions.h"

namespace blink
{
    namespace io
    {
        namespace converters
        {
            data::TilesetMetadata JsonToTilesetMetadataConverter::convert(const nlohmann::json& metaJson) const try
            {
                data::TilesetMetadata metadata;
                metadata.name = metaJson.at("name");
                metadata.imageFilePath = metaJson.at("image");
                metadata.columns = metaJson.at("columns");
                metadata.imagewidth = metaJson.at("imagewidth");
                metadata.imageheight = metaJson.at("imageheight");
                metadata.margin = metaJson.at("margin");
                metadata.spacing = metaJson.at("spacing");
                metadata.tilecount = metaJson.at("tilecount");
                metadata.tilewidth = metaJson.at("tilewidth");
                metadata.tileheight = metaJson.at("tileheight");
                //metadata.type = metaJson.at("type");

                BLINK_LOGF_W_IF(metadata.tilecount == 0, "JSON for tileset \"%1%\" specifies tilecount 0", metadata.name);
                BLINK_LOGF_W_IF(
                    metadata.tilecount % metadata.columns != 0,
                    "In JSON for tileset \"%1%\": tilecount %2% is not divisible by the number of columns (%3%)",
                    metadata.name % metadata.tilecount % metadata.columns
                );
                BLINK_LOGF_E_IF(
                    metadata.tilecount < metadata.columns,
                    "In JSON for tileset \"%1%\": tilecount %2% is less than the number of columns (%3%)",
                    metadata.name % metadata.tilecount % metadata.columns
                );

                const auto firstgidIt = metaJson.find("firstgid");
                if((firstgidIt) != metaJson.end())
                {
                    metadata.firstgid = firstgidIt.value();
                }
                else
                {
                    metadata.firstgid = -1;
                }

                return metadata;
            }
            catch(const std::out_of_range& e)
            {
                throw detail::DataConversionExceptionImpl<nlohmann::json, data::TilesetMetadata>(e.what());
            }
            catch(const std::domain_error& e)
            {
                throw detail::DataConversionExceptionImpl<nlohmann::json, data::TilesetMetadata>(e.what());
            }
        }
    }
}
