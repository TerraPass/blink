#ifndef BLINK_SYSTEMS_ANIMATIONSYSTEM_H
#define BLINK_SYSTEMS_ANIMATIONSYSTEM_H

#include <entityx/entityx.h>

#include "core/ITimeKeeper.h"

namespace blink
{
    namespace systems
    {
        class AnimationSystem final : public entityx::System<AnimationSystem>
        {
            const core::ITimeKeeper& timeKeeper;

        public:
            explicit AnimationSystem(const core::ITimeKeeper& timeKeeper);
            
            AnimationSystem(const AnimationSystem&) = delete;
            AnimationSystem(AnimationSystem&&) = default;   // sic
            
            AnimationSystem& operator=(const AnimationSystem&) = delete;
            AnimationSystem& operator=(AnimationSystem&&) = delete;

            virtual ~AnimationSystem() = default;

            virtual void update(entityx::EntityManager& entities, entityx::EventManager& events, entityx::TimeDelta dt) override;

        };
    }
}

#endif /* BLINK_SYSTEMS_ANIMATIONSYSTEM_H */

