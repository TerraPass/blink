#include "InputSystem.h"

#include <cassert>

#include <SDL.h>
#include <SDL_events.h>

#include "debug/logging.h"

namespace blink
{
    namespace input
    {
        InputSystem::InputSystem()
            : appShutdownRequestedEvent(*this),
                mouseButtonDownEvent(*this),
                keyDownEvent(*this),
                keyUpEvent(*this)
        {

        }

        static inline MouseButton toMouseButton(const int sdlButton)
        {
            switch(sdlButton)
            {
                default: return MouseButton::UNKNOWN;
                case SDL_BUTTON_LEFT: return MouseButton::LEFT;
                case SDL_BUTTON_RIGHT: return MouseButton::RIGHT;
                case SDL_BUTTON_MIDDLE: return MouseButton::MIDDLE;
            }
        }

        void InputSystem::update()
        {
            SDL_Event event;
            while(SDL_PollEvent(&event) != 0)
            {
                // TODO: Replace with a pre-initialized handlers array/map lookup
                switch(event.type)
                {
                    default: break;

                    case SDL_QUIT:
                    {
                        BLINK_LOG_I("User has requested the application to quit (SDL_QUIT event)");
                        this->appShutdownRequestedEvent();
                    } break;

                    case SDL_MOUSEBUTTONDOWN:
                    {
                        const MouseButton mouseButton = toMouseButton(event.button.button);
                        MousePosition mousePosition;
                        SDL_GetMouseState(&(mousePosition.x), &(mousePosition.y));
                        BLINK_LOGF_D("Mouse button %1% down at pos. %2% (SDL_MOUSEBUTTONDOWN event)", static_cast<int>(mouseButton) % mousePosition);
                        this->mouseButtonDownEvent(MouseEventArgs(mouseButton, mousePosition));
                    } break;

                    case SDL_KEYDOWN:
                    case SDL_KEYUP:
                    {
                        // Do not report key repeat
                        if(event.key.repeat)
                        {
                            break;
                        }

                        const bool down = (event.type == SDL_KEYDOWN);
                        const KeyboardEventArgs eventArgs(
                            event.key.keysym.scancode,
                            event.key.keysym.sym,
                            (event.key.state == SDL_PRESSED)
                        );
                        assert((event.key.state == SDL_PRESSED) == (event.type == SDL_KEYDOWN));
                        assert((event.key.state == SDL_RELEASED) == (event.type == SDL_KEYUP));
                        BLINK_LOGF_D(
                            "Key %1%: %2% (%3% event)",
                            (down ? "down" : "up")
                            % eventArgs
                            % (down ? "SDL_KEYDOWN" : "SDL_KEYUP")
                        );
                        (down ? keyDownEvent : keyUpEvent)(eventArgs);
                    } break;
                }
            }
        }
    }
}
