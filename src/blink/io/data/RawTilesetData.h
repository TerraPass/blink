#ifndef BLINK_IO_DATA_RAWTILESETDATA_H
#define BLINK_IO_DATA_RAWTILESETDATA_H

#include <nlohmann_json.hpp>
#include <sdl2utils/pointers.h>

namespace blink
{
    namespace io
    {
        namespace data
        {
            struct RawTilesetData final
            {
                nlohmann::json metadataJson;
                sdl2utils::SDL_TexturePtr ptexture;

                RawTilesetData(nlohmann::json metadataJson, sdl2utils::SDL_TexturePtr ptexture)
                : metadataJson(std::move(metadataJson)), ptexture(std::move(ptexture))
                {
                    
                }
            };
        }
    }
}

#endif /* BLINK_IO_DATA_RAWTILESETDATA_H */

