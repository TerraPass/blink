#include "TilesetFactory.h"

#include <vector>

#include "types.h"

namespace blink
{
    namespace tiling
    {
        Tileset TilesetFactory::create(io::data::TilesetData data) const
        {
            const auto& metadata = data.metadata;

            if(metadata.tilecount < metadata.columns)
            {
                throw std::invalid_argument("Tileset's tilecount must not be less than its number of columns");
            }

            const auto margin = static_cast<pixelunit>(metadata.margin);
            const auto spacing = static_cast<pixelunit>(metadata.spacing);

            const auto tileWidth = static_cast<pixelunit>(metadata.tilewidth);
            const auto tileHeight = static_cast<pixelunit>(metadata.tileheight);

            std::vector<Rect> tileRects(metadata.tilecount, {-1, -1, tileWidth, tileHeight});

            for(size_t i = 0; i < metadata.tilecount; i++)
            {
                const auto row = static_cast<pixelunit>(i / metadata.columns);
                const auto col = static_cast<pixelunit>(i % metadata.columns);

                auto& rect = tileRects[i];

                rect.x = margin + (tileWidth + spacing)*col;
                rect.y = margin + (tileHeight + spacing)*row;
            }

            return Tileset(std::move(data.textureData.texturePtr), std::move(tileRects), tileWidth, tileHeight);
        }
    }
}
