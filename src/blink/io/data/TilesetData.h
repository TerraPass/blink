#ifndef BLINK_IO_DATA_TILESETDATA_H
#define BLINK_IO_DATA_TILESETDATA_H

#include <string>

#include "common/numeric.h"
#include "io/types.h"

#include "TextureData.h"

namespace blink
{
    namespace io
    {
        namespace data
        {
            struct TilesetMetadata final
            {
                static constexpr const auto DISPLAY_NAME = "tileset metadata";

                I64 firstgid;
                std::string name;
                FilePath imageFilePath;
                U16 columns;
                U32 imagewidth;
                U32 imageheight;
                U8 margin;
                U8 spacing;
                size_t tilecount;
                U8 tilewidth;
                U8 tileheight;
                //std::string type;
            };

            struct TilesetData final
            {
                static constexpr const auto DISPLAY_NAME = "tileset";

                TextureData textureData;
                TilesetMetadata metadata;

                TilesetData(TextureData textureData, const TilesetMetadata metadata)
                : textureData(std::move(textureData)), metadata(metadata)
                {
                    
                }
            };
        }
    }
}

#endif /* BLINK_IO_DATA_TILESETDATA_H */
