#ifndef BLINK_IO_RETRIEVAL_JSONRETRIEVER_H
#define BLINK_IO_RETRIEVAL_JSONRETRIEVER_H

#include <nlohmann_json.hpp>

#include "io/types.h"

namespace blink
{
    namespace io
    {
        namespace retrieval
        {
            class JsonRetriever final
            {
            public:
                nlohmann::json retrieve(const FilePath& filePath) const;
            };
        }
    }
}

#endif /* BLINK_IO_RETRIEVAL_JSONRETRIEVER_H */

