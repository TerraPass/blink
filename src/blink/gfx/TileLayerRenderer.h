#ifndef BLINK_GFX_TILELAYERRENDERER_H
#define BLINK_GFX_TILELAYERRENDERER_H

#include <unordered_map>

#include <SDL.h>
#include <sdl2utils/pointers.h>

#include "tiling/TileLayer.h"

#include "FullTileLayerRenderer.h"

namespace blink
{
    namespace gfx
    {
        template <typename Camera>
        class TileLayerRenderer final
        {
            // TODO: Separate caching logic into a decorator template
            struct CacheEntry final
            {
                sdl2utils::SDL_TexturePtr ptexture;
                U64 version;
                U8 cameraScale; // TODO: Store cache entries for different scales,
                                // i.e. make cameraScale a part of the key, not value,
                                // in order to allow for faster zooming.
            };

            using Cache = std::unordered_map<const tiling::TileLayer*, CacheEntry>;

            static constexpr const Uint32 CACHE_PIXEL_FORMAT = SDL_BYTEORDER == SDL_BIG_ENDIAN
                ? SDL_PIXELFORMAT_RGBA8888
                : SDL_PIXELFORMAT_ABGR8888;

            static constexpr const real MIN_RENDER_OPACITY = 0.01;

            SDL_Renderer* const prenderer;

            const FullTileLayerRenderer fullRenderer;
            const Camera& camera;

            Cache cache;

        public:
            TileLayerRenderer(SDL_Renderer* const prenderer, const Camera& camera)
            : prenderer(prenderer), fullRenderer(prenderer), camera(camera), cache()
            {
                
            }

            void renderLayer(const tiling::TileLayer& tileLayer, const typename Camera::InputPosition offset);

            inline void clearCache()
            {
                cache.clear();
            }

        private:
            void renderToCache(const tiling::TileLayer& tileLayer, const typename Cache::iterator cacheHit);
            void renderFromCache(const tiling::TileLayer& tileLayer, const typename Camera::InputPosition offset) const;
        };
    }
}

#ifndef BLINK_COMPILE_TEMPLATES
#include "TileLayerRenderer.cxx"
#endif

#endif /* BLINK_GFX_TILELAYERRENDERER_H */

