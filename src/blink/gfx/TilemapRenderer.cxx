#ifdef BLINK_COMPILE_TEMPLATES
#include "TilemapRenderer.h"

#include "camera/TopDownCamera.h"
#endif

namespace blink
{
    namespace gfx
    {
        template<typename Camera>
        void TilemapRenderer<Camera>::renderMap(const tiling::Tilemap& tilemap, const typename Camera::InputPosition offset) const
        {
            for(size_t i = 0; i < tilemap.getTileLayersCount(); i++)
            {
                tileLayerRenderer.renderLayer(tilemap.getTileLayer(i), offset);
            }
        }

#ifdef BLINK_COMPILE_TEMPLATES
        template class TilemapRenderer<camera::TopDownCamera>;
#endif
    }
}
