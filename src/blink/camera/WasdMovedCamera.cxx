#ifdef BLINK_COMPILE_TEMPLATES
#include "WasdMovedCamera.h"

#include "TopDownCamera.h"
#endif

namespace blink
{
    namespace camera
    {
        template <typename Camera>
        constexpr const real WasdMovedCamera<Camera>::SENSITIVITY;

        template<typename Camera>
        WasdMovedCamera<Camera>::WasdMovedCamera(WasdMovedCamera&& other)
        : Camera(std::move(other)),
            inputSystem(other.inputSystem),
            keyDownSub(
                inputSystem.getKeyDownEvent().subscribe(&WasdMovedCamera<Camera>::onKeyDown, this)
            ),
            keyUpSub(
                inputSystem.getKeyUpEvent().subscribe(&WasdMovedCamera<Camera>::onKeyUp, this)
            ),
            currentKeyMask(other.currentKeyMask)
        {
            other.keyDownSub.cancel();
            other.keyUpSub.cancel();
        }

        namespace
        {
            constexpr const U8 W_DOWN = 1;
            constexpr const U8 S_DOWN = 2;
            constexpr const U8 A_DOWN = 4;
            constexpr const U8 D_DOWN = 8;

            inline U8 scancodeToFlag(const input::Scancode scancode)
            {
                switch(scancode)
                {
                    default: return 0;
                    case SDL_SCANCODE_W: return W_DOWN;
                    case SDL_SCANCODE_S: return S_DOWN;
                    case SDL_SCANCODE_A: return A_DOWN;
                    case SDL_SCANCODE_D: return D_DOWN;
                }
            }

            /**
             * Returns 1 if the flag is set in the mask, 0 otherwise.
             * 
             * @param mask Mask to check for flag.
             * @param flag Flag.
             * @return 1 if the flag is set, 0 otherwise.
             */
            inline U8 flagSetUnit(const U8 mask, const U8 flag)
            {
                const auto andres = mask & flag;
                return andres ? 1 : 0;
            }
        }
        
        template<typename Camera>
        void WasdMovedCamera<Camera>::update(const real seconds)
        {
            if(currentKeyMask == 0)
            {
                return;
            }

            I8 dx = flagSetUnit(currentKeyMask, D_DOWN) - flagSetUnit(currentKeyMask, A_DOWN);
            I8 dy = flagSetUnit(currentKeyMask, S_DOWN) - flagSetUnit(currentKeyMask, W_DOWN);
            this->Camera::getTargetPosition() 
                += seconds*SENSITIVITY*(typename Camera::InputPosition(
                    static_cast<typename Camera::InputPosition::unit>(dx),
                    static_cast<typename Camera::InputPosition::unit>(dy)
                ));
        }

        template<typename Camera>
        void WasdMovedCamera<Camera>::onKeyDown(input::InputSystem& /*inputSystem*/, const input::KeyboardEventArgs& args)
        {
            currentKeyMask |= scancodeToFlag(args.scancode);
        }

        template<typename Camera>
        void WasdMovedCamera<Camera>::onKeyUp(input::InputSystem& /*inputSystem*/, const input::KeyboardEventArgs& args)
        {
            currentKeyMask &= ~scancodeToFlag(args.scancode);
        }

#ifdef BLINK_COMPILE_TEMPLATES
        // Explicit template instantiations
        template class WasdMovedCamera<TopDownCamera>;
#endif
    }
}
