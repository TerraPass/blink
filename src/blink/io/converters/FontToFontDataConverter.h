#ifndef BLINK_IO_CONVERTERS_FONTTOFONTDATACONVERTER_H
#define BLINK_IO_CONVERTERS_FONTTOFONTDATACONVERTER_H

#include "io/data/FontData.h"

namespace blink
{
    namespace io
    {
        namespace converters
        {
            class FontToFontDataConverter final
            {
            public:
                inline data::FontData convert(data::FontData::FontPtr pfont) const
                {
                    return data::FontData(std::move(pfont));
                }
            };
        }
    }
}

#endif /* BLINK_IO_CONVERTERS_FONTTOFONTDATACONVERTER_H */

