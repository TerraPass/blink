#ifndef BLINK_DEBUG_RAIILOG_H
#define BLINK_DEBUG_RAIILOG_H

#include <string>

#include <boost/any.hpp>

#include "logging.h"

namespace blink
{
    namespace debug
    {
        namespace detail
        {
            template <int LogLevel>
            inline void logImpl(const std::string& message) noexcept;

            template <>
            inline void logImpl<BLINK_LOG_NONE>(const std::string& /*message*/) noexcept
            {

            }

#ifdef __GNUG__
// Suppress GCC warning -Wunused-parameter.
// For logImpl<X>(), when X < BLINK_LOG_LEVEL, the message parameter will be unused,
// since the corresponding log macro will expand to (void)0. This is normal.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#endif

            template <>
            inline void logImpl<BLINK_LOG_FATAL>(const std::string& message) noexcept
            {
                BLINK_LOG_F(message);
            }

            template <>
            inline void logImpl<BLINK_LOG_ERROR>(const std::string& message) noexcept
            {
                BLINK_LOG_E(message);
            }

            template <>
            inline void logImpl<BLINK_LOG_WARNING>(const std::string& message) noexcept
            {
                BLINK_LOG_W(message);
            }

            template <>
            inline void logImpl<BLINK_LOG_INFO>(const std::string& message) noexcept
            {
                BLINK_LOG_I(message);
            }

            template <>
            inline void logImpl<BLINK_LOG_DEBUG>(const std::string& message) noexcept
            {
                BLINK_LOG_D(message);
            }

            template <>
            inline void logImpl<BLINK_LOG_TRACE>(const std::string& message) noexcept
            {
                BLINK_LOG_T(message);
            }

#ifdef __GNUG__
#pragma GCC diagnostic pop
#endif

        }

        template <typename T, int LogLevel = BLINK_LOG_INFO>
        class RaiiLogged : public T
        {
            const std::string destructionMessage;
            
        public:
            template <typename... Args>
            inline RaiiLogged(
                const std::string& constructionMessage,
                const std::string& destructionMessage,
                Args&&... args
            ) noexcept   
            : T(std::forward<Args>(args)...), destructionMessage(destructionMessage)
            {
                detail::logImpl<LogLevel>(constructionMessage);
            }

            virtual ~RaiiLogged()
            {
                detail::logImpl<LogLevel>(destructionMessage);
            }
        };
    }
}

#endif /* BLINK_DEBUG_RAIILOG_H */

