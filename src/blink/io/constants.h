#ifndef BLINK_IO_CONSTANTS_H
#define BLINK_IO_CONSTANTS_H

namespace blink
{
    namespace io
    {
        // FIXME: This needs to be relative to the application executable,
        // instead of to the directory, from which the application was started.
        const std::string ASSETS_BASE_PATH = "./assets/";    // TODO: Move to configuration
        
        const std::string FONTS_PATH        = ASSETS_BASE_PATH + "fonts";
        const std::string SPRITES_PATH      = ASSETS_BASE_PATH + "sprites";
        const std::string SPRITES_DATA_PATH = SPRITES_PATH;
        const std::string TILEMAPS_PATH     = ASSETS_BASE_PATH + "maps";
        const std::string TILESETS_PATH     = TILEMAPS_PATH + "tilesets";
        const std::string MUSIC_PATH        = ASSETS_BASE_PATH + "music";
        const std::string SFX_PATH          = ASSETS_BASE_PATH + "sfx";
        
        const std::string JSON_EXTENSION    = ".json";
        const std::string PNG_EXTENSION     = ".png";
        const std::string TTF_EXTENSION     = ".ttf";
        const std::string OTF_EXTENSION     = ".otf";
        const std::string MP3_EXTENSION     = ".mp3";
        const std::string OGG_EXTENSION     = ".ogg";
        const std::string WAV_EXTENSION     = ".wav";
    }
}

#endif /* BLINK_IO_CONSTANTS_H */

