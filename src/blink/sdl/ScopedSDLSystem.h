#ifndef BLINK_SDL_SCOPEDSDLSYSTEM_H
#define BLINK_SDL_SCOPEDSDLSYSTEM_H

#include <cassert>

#include <SDL.h>
#include <SDL_image.h>

#include <sdl2utils/raii.h>
#include <sdl2utils/exceptions.h>

#include "debug/logging/logging.h"
#include "debug/logging/raii_log.h"

namespace blink
{
    namespace sdl
    {
        using namespace sdl2utils;
        
        // TODO: Replace with a builder, which would allow to specify
        // the parameters for initialization of SDL and its extension libraries.
        // E.g. ScopedSDLSystem::getBuilder(SDL_INIT_EVERYTHING)->withSdlImage(INIT_PNG).withSdlTtf().withRaiiLogger(plogger)build().
        // RAII logger could also log SDL libraries compiled and linked versions.
        // Said builder can be placed into sdl2utils library.
        class ScopedSDLSystem final
        {
            static constexpr const Uint32 sdlInitFlags      = SDL_INIT_EVERYTHING;
            static constexpr const int sdlImageInitFlags    = IMG_INIT_PNG;
            static constexpr const int sdlMixerInitFlags    = 0; // TODO: Use other formats?

#ifndef NDEBUG
            static bool instanceExists;
#endif

            const raii::ScopedSDLCore sdlCore;
            const raii::ScopedSDLImageExt sdlImageExt;
            const raii::ScopedSDLTtfExt sdlTtfExt;
            const raii::ScopedSDLMixerExt sdlMixerExt;
        public:
            inline ScopedSDLSystem()
                : sdlCore(sdlInitFlags), sdlImageExt(sdlImageInitFlags), sdlTtfExt(), 
                    sdlMixerExt(44100, MIX_DEFAULT_FORMAT, 2, 4096, sdlMixerInitFlags)
            {
                assert(!instanceExists && "No more than one instance of ScopedSDLSystem may exist at the same time");
#ifndef NDEBUG
                instanceExists = true;
#endif
                BLINK_LOG_I("SDL core and extension libraries initialized");
            }

            inline ~ScopedSDLSystem()
            {
#ifndef NDEBUG
                instanceExists = false;
#endif
                BLINK_LOG_I("SDL core and extension libraries deinitialized");
            }

            ScopedSDLSystem(const ScopedSDLSystem&) = delete;
            ScopedSDLSystem(ScopedSDLSystem&&) = delete;
            
            ScopedSDLSystem& operator=(const ScopedSDLSystem&) = delete;
            ScopedSDLSystem& operator=(ScopedSDLSystem&&) = delete;
        };
    }
}

#endif /* BLINK_SDL_SCOPEDSDLSYSTEM_H */

