#include "FilePathResolver.h"

#include "debug/logging.h"

#include "utils.h"

namespace blink
{
    namespace io
    {
        namespace resolvers
        {
            namespace
            {
                inline std::string withTrailingSlash(const std::string& path)
                {
                    if(path.empty())
                    {
                        return "./";
                    }

                    if(path[path.length() - 1] != '/')
                    {
                        return path + '/';
                    }

                    return path;
                }

                inline std::string withLeadingDot(const std::string& fileExtension)
                {
                    if(!fileExtension.empty() && fileExtension[0] != '.')
                    {
                        return "." + fileExtension;
                    }

                    return fileExtension;
                }

            }

            FilePathResolver::FilePathResolver(const std::string baseRelativePath, const std::string fileExtension)
            : basePath(withTrailingSlash(baseRelativePath)),
                fileExtension(withLeadingDot(fileExtension))
            {

            }

            FilePath FilePathResolver::resolve(const BasicKey& key) const
            {
                if (key.empty())
                {
                    throw std::invalid_argument("key must not be empty");
                }

                static const std::string execDirPath = withTrailingSlash(getExecutableDirectory());

                const auto result = basePath + key + fileExtension;
                BLINK_LOGF_D("Resolved \"%1%\" to \"%2%\"", key % result);
                return basePath + key + fileExtension;
            }
        }
    }
}
