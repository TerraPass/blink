#include "CatchUpGameLoop.h"

#include "debug/logging/logging.h"

#include "time/ExecutionTimer.h"
#include "time/ScalableResettableExecTimer.h"

namespace blink
{
    namespace core
    {
        constexpr const U8 CatchUpGameLoop::UPDATES_PER_SECOND;
        constexpr const real CatchUpGameLoop::SECONDS_PER_UPDATE;
        
        constexpr const U8 CatchUpGameLoop::MAX_UPDATES_BETWEEN_RENDERS;

        void CatchUpGameLoop::run()
        {
            // NB: Realtime, not gameplay time, is currently used for the game loop.
            // This might  have to be reconsidered in the future.
            real previous = this->ptimeKeeper->getRealtimeElapsedSeconds();
            real lag = 0.0;

            while(!this->isExitRequested)
            {
                real current = this->ptimeKeeper->getRealtimeElapsedSeconds();
                real elapsed = current - previous;
                
                previous = current;
                lag += elapsed;
                
                this->processInput();

                U8 updates = 0;
                while(lag >= SECONDS_PER_UPDATE && updates < MAX_UPDATES_BETWEEN_RENDERS)
                {
                    this->updateWorld(SECONDS_PER_UPDATE);
                    lag -= SECONDS_PER_UPDATE;
                    updates++;
                }

                this->renderWorld(elapsed);
            }
        }

        void CatchUpGameLoop::processInput()
        {
            // TODO: Implement an input manager class (or classes),
            // which would poll SDL events and expose a convenient
            // interface to user input.
            // Additionally, implement Observer pattern, allowing various
            // components to subscribe to input events they are interested in.
            // Update object(s) of this/these class(es) from this method.
            this->pinputSystem->update();
        }
    }
}