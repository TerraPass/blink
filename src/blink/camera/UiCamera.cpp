#include "UiCamera.h"

#include "utils.h"

namespace blink
{
    namespace camera
    {
        ScreenPosition UiCamera::toScreenPosition(const InputPosition inputPosition) const
        {
            // TODO: Handle negative coordinates as offsets from the other side of the screen.
            return scale*inputPosition;
        }

        UiCamera::InputPosition UiCamera::fromScreenPosition(const ScreenPosition screenPosition) const
        {
            // TODO: Handle negative coordinates as offsets from the other side of the screen.
            return scale*screenPosition;
        }

        Rect UiCamera::scaleRect(const Rect rect) const
        {
            // TODO: Handle negative coordinates?
            return blink::camera::scaleRect(rect, scale);
        }
    }
}
