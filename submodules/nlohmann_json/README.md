This directory contains nlohmann/json library @ version 2.1.1, publlished at https://github.com/nlohmann/json.git under tag v2.1.1.
Due to the large size of nlohmann/json repository (mainly caused by it containing JSON data for benchmarks), only the files necessary for including this library in a CMake project and the header file itself were copied, instead of the entire repo being made a submodule.

json.hpp and json.hpp.re2c files were renamed to nlohmann_json.hpp and nlohmann_json.hpp.re2c respectively to avoid potential name conflicts, due to the overly generic name of the library's header file.

Aside from them, this README.md is the only copied file which was changed.

LICENSE.MIT contains the original license from nlohmann/json @ v2.1.1.
