#ifndef BLINK_DEBUG_TYPEINFO_H
#define BLINK_DEBUG_TYPEINFO_H

// Code in this file has been taken with minimal changes
// from the following answer by Ali on StackOverflow:
// http://stackoverflow.com/a/4541470/1051764

#include <string>
#include <typeinfo>

namespace blink
{
    namespace debug
    {
        namespace detail
        {
            std::string demangle(const char* name);
        }

        template <class T>
        std::string getTypename(const T& t)
        {
            return detail::demangle(typeid(t).name());
        }

        template <class T>
        std::string getTypename()
        {
            return detail::demangle(typeid(T).name());
        }
    }
}

#endif /* BLINK_DEBUG_TYPEINFO_H */

