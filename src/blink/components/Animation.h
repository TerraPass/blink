#ifndef BLINK_COMPONENTS_ANIMATION_H
#define BLINK_COMPONENTS_ANIMATION_H

#include <cassert>
#include <limits>
#include <cmath>
#include <boost/optional.hpp>

#include <SDL.h>

#include "gfx/Animation.h"

namespace blink
{
    namespace components
    {
        struct Animation final
        {
            static constexpr const real UNKNOWN_GAMEPLAY_TIME = std::numeric_limits<real>::min();

            boost::optional<gfx::Animation> animation;
            real lastUpdateGameplayTime;    // seconds
            real lastUpdateAnimTime;        // seconds

            Animation()
            : animation(), lastUpdateGameplayTime(UNKNOWN_GAMEPLAY_TIME), lastUpdateAnimTime(0)
            {
                
            }
            
            explicit Animation(const gfx::Animation& animation, const real initialGameplayTime, const real initialAnimTime = 0)
            : animation(animation), lastUpdateGameplayTime(initialGameplayTime), 
                lastUpdateAnimTime(std::fmod(initialAnimTime, static_cast<real>(animation.getTotalDuration())))
            {
                
            }

            inline gfx::Animation& getAnimation()
            {
                assert(animation && "animation must be present on call to getAnimation()");

                return *animation;
            }
        };
    }
}

#endif
