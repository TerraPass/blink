#include "SurfaceRetriever.h"
#include "io/exceptions.h"

#include <SDL.h>
#include <SDL_image.h>
#include <sdl2utils/exceptions.h>
#include <sdl2utils/guards.h>

namespace blink
{
    namespace io
    {
        namespace retrieval
        {
            using namespace sdl2utils;

            SurfaceRetriever::SurfacePtr SurfaceRetriever::retrieve(const FilePath& filePath) const try
            {                
                return SDL_SurfacePtr(
                    sdl2utils::guards::ensureNotNull<SDLImageExtException>(
                        IMG_Load(filePath.c_str()), 
                        "IMG_Load() return value"
                    )
                );
            }
            catch(const SDLImageExtException& e)
            {
                throw detail::RawDataRetrievalExceptionImpl<SurfaceRetriever::SurfacePtr>(
                    filePath,
                    e.what()
                );
            }
        }
    }
}
