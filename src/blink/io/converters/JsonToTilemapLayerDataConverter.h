#ifndef BLINK_IO_CONVERTERS_JSONTOTILEMAPLAYERDATACONVERTER_H
#define BLINK_IO_CONVERTERS_JSONTOTILEMAPLAYERDATACONVERTER_H

#include "io/data/TilemapData.h"

#include "JsonToTilemapObjectDataConverter.h"

namespace blink
{
    namespace io
    {
        namespace converters
        {
            class JsonToTilemapLayerDataConverter final
            {
                using LayerData = data::TilemapData::LayerData;

                static const std::string LAYER_TYPE_TILE;
                static const std::string LAYER_TYPE_OBJECT;

                const JsonToTilemapObjectDataConverter objectDataConverter;

            public:

                JsonToTilemapLayerDataConverter()
                : objectDataConverter()
                {

                }

                LayerData convert(const nlohmann::json& layerJson) const;
            };
        }
    }
}

#endif /* BLINK_IO_CONVERTERS_JSONTOTILEMAPLAYERDATACONVERTER_H */

