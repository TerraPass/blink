#ifndef BLINK_TIME_ISCALABLERESETTABLETIMER_H
#define BLINK_TIME_ISCALABLERESETTABLETIMER_H

#include "IScalableTimer.h"
#include "IResettableTimer.h"


namespace blink
{
    namespace time
    {
        class IScalableResettableTimer : public virtual IScalableTimer, public virtual IResettableTimer
        {
            
        };
    }
}

#endif /* BLINK_TIME_ISCALABLERESETTABLETIMER_H */

