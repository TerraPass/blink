#include "MousePosition.h"

#include <boost/format.hpp>

namespace blink
{
    namespace input
    {
        using boost::format;
        using boost::str;

        std::ostream& operator<<(std::ostream& out, const MousePosition& mousePosition)
        {
            out << (format("(%1%, %2%)") % mousePosition.x % mousePosition.y);
            return out;
        }
    }
}
