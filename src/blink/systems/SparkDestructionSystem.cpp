#include "SparkDestructionSystem.h"

#include <cmath>
#include <limits>

#include "common/utils/math.h"
#include "components/Position.h"
#include "components/Sprite.h"
#include "camera/utils.h"

namespace blink
{
    namespace systems
    {
        using namespace entityx;

        using namespace components;

        SparkDestructionSystem::SparkDestructionSystem(input::InputSystem& inputSystem, const camera::TopDownCamera& camera)
        : inputSystem(inputSystem), camera(camera),
            mouseButtonDownSub(
                inputSystem.getMouseButtonDownEvent().subscribe(&SparkDestructionSystem::onMouseButtonDown, this)
            )
        {
            
        }

        SparkDestructionSystem::SparkDestructionSystem(SparkDestructionSystem&& other)
            : inputSystem(other.inputSystem), camera(other.camera),
                mouseButtonDownSub(
                    inputSystem.getMouseButtonDownEvent().subscribe(&SparkDestructionSystem::onMouseButtonDown, this)
                )
        {
            other.mouseButtonDownSub.cancel();
        }

        SparkDestructionSystem::~SparkDestructionSystem() = default;

        void SparkDestructionSystem::onMouseButtonDown(input::InputSystem&, input::MouseEventArgs args)
        {
            if(args.button == input::MouseButton::LEFT)
            {
                maybeLmbClickPosition = args.position;
            }
        }

        void SparkDestructionSystem::update(EntityManager& entities, EventManager& /*events*/, TimeDelta dt)
        {
            // If there is no outstanding LMB press
            if(!maybeLmbClickPosition)
            {
                return;
            }

            const auto& lmbClickPosition = *maybeLmbClickPosition;

            // FIXME: This duplicates rendering priority implementation from RenderingSystem
            real maxY = std::numeric_limits<real>::min();
            Entity topEntity;
            entities.each<Position, Sprite>(
                [this, dt, lmbClickPosition, &maxY, &topEntity] (Entity entity, Position& position, Sprite& sprite) {
                    if(blink::camera::isInsideWorldRect(
                            camera, 
                            lmbClickPosition, 
                            {
                                static_cast<gfx::unit>(position.x),
                                static_cast<gfx::unit>(position.y),
                                sprite.getSprite().getWidth(),
                                sprite.getSprite().getHeight()
                            }
                        )
                        && position.y > maxY)
                    {
                        maxY = position.y;
                        topEntity = entity;
                    }
                }
            );

            if(topEntity)
            {
                topEntity.destroy();
            }

            maybeLmbClickPosition = boost::none;
        }
    }
}
