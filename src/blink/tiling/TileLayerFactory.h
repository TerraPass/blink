#ifndef BLINK_GFX_TILELAYERFACTORY_H
#define BLINK_GFX_TILELAYERFACTORY_H

#include "io/data/TilemapData.h"

#include "TileLayer.h"
#include "MapTilesContainer.h"

namespace blink
{
    namespace tiling
    {
        class TileLayerFactory final
        {
        public:
            TileLayer create(const io::data::TilemapData::LayerData& data, ConstMapTilesContainerPtr pmapTilesContainer) const;
        };
    }
}

#endif /* BLINK_GFX_TILELAYERFACTORY_H */

