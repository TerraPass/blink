#ifndef BLINK_CAMERA_TOPDOWNCAMERA_H
#define BLINK_CAMERA_TOPDOWNCAMERA_H

#include "common/types.h"

#include "types.h"

namespace blink
{
    namespace camera
    {
        class TopDownCamera
        {
        public:
            using InputPosition = WorldPosition;
            //using OutputPosition = gfx::ScreenPosition;

        private:
            gfx::unit outputWidth;
            gfx::unit outputHeight;
            InputPosition targetPosition;
            U8 scale;
            
        public:
            static const InputPosition DEFAULT_TARGET_POSITION;
            static constexpr const U8 DEFAULT_SCALE = 1;

            explicit TopDownCamera(
                const gfx::unit outputWidth,
                const gfx::unit outputHeight,
                const InputPosition targetPosition = DEFAULT_TARGET_POSITION,
                const U8 scale = DEFAULT_SCALE
            );

            TopDownCamera(const TopDownCamera&) = delete;
            TopDownCamera(TopDownCamera&&) = default;   // sic
            
            TopDownCamera& operator=(const TopDownCamera&) = delete;
            TopDownCamera& operator=(TopDownCamera&&) = delete;

            virtual ~TopDownCamera() = default;

            inline gfx::unit getOutputWidth() const
            {
                return this->outputWidth;
            }

            inline gfx::unit getOutputHeight() const
            {
                return this->outputHeight;
            }

            inline InputPosition& getTargetPosition()
            {
                return targetPosition;
            }

            inline const InputPosition& getTargetPosition() const
            {
                return targetPosition;
            }

            void lookAt(const InputPosition target);
            InputPosition getLookAtTarget() const;

            inline U8 getScale() const
            {
                return scale;
            }

            inline TopDownCamera& setScale(const U8 scale)
            {
                this->scale = scale;
                return *this;
            }

            ScreenPosition toScreenPosition(const InputPosition worldPosition) const;
            InputPosition fromScreenPosition(const ScreenPosition screenPosition) const;

            Rect scaleRect(const Rect rect) const;

            // TODO: Introduce WorldRect and use it as parameter of this method (maybe via InputRect alias)
            Rect toScreenRect(const Rect rect) const;
            Rect fromScreenRect(const Rect rect) const;

            inline InputPosition getInputTopLeft() const
            {
                return fromScreenPosition({0, 0});
            }

            inline InputPosition getInputBottomRight() const
            {
                return fromScreenPosition({outputWidth, outputHeight});
            }

            inline bool isInFrame(const InputPosition worldPosition) const
            {
                const auto topLeft = getInputTopLeft();
                const auto bottomRight = getInputBottomRight();
                return worldPosition.x >= topLeft.x && worldPosition.x < bottomRight.x
                        && worldPosition.y >= topLeft.y && worldPosition.y < bottomRight.y;
            }
        };
    }
}

#endif /* BLINK_CAMERA_TOPDOWNCAMERA_H */

