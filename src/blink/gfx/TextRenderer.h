#ifndef BLINK_GFX_TEXTRENDERER_H
#define BLINK_GFX_TEXTRENDERER_H

#include <string>
#include <boost/optional.hpp>

#include <SDL.h>

#include "Font.h"
#include "SpriteRenderer.h"

namespace blink
{
    namespace gfx
    {
        // TODO: Implement incremental rendering for monospace fonts
        template <typename Camera>
        class /*Incremental*/TextRenderer final
        {
            const SpriteRenderer<Camera> spriteRenderer;
            const Camera& camera;

        public:
            explicit TextRenderer(SDL_Renderer* const prenderer, const Camera& camera)
            : spriteRenderer(prenderer, camera), camera(camera)
            {
                
            }

            TextRenderer(const TextRenderer&) = default;
            TextRenderer(TextRenderer&&) = default;
            
            TextRenderer& operator=(const TextRenderer&) = delete;
            TextRenderer& operator=(TextRenderer&&) = delete;

            void renderText(
                const Font& font, 
                const typename Camera::InputPosition& cameraInputPosition,
                const std::string& text/*, 
                boost::optional<const std::string&> maybeOldText = {}*/
            );

            ~TextRenderer() = default;
        };
    }
}

#ifndef BLINK_COMPILE_TEMPLATES
#include "TextRenderer.cxx"
#endif

#endif /* BLINK_GFX_INCREMENTALFONTRENDERER_H */

