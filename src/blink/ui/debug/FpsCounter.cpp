#include "FpsCounter.h"

#include <string>
#include <algorithm>
#include <numeric>
#include <boost/format.hpp>

namespace blink
{
    namespace ui
    {
        namespace debug
        {
            using boost::str;
            using boost::format;

            constexpr const size_t FpsCounter::AVG_OVER_FRAMES;

            void FpsCounter::render(const real deltaTime)
            {
                const auto currentTime = timeKeeper.getRealtimeElapsedSeconds();
                frameTimes[(currentFrame++) % AVG_OVER_FRAMES] = deltaTime >= 0.0
                    ? deltaTime
                    : currentTime - lastFrameTime; //secondsSince(lastFrameTime);
                lastFrameTime = currentTime;

                // If it's time to refresh the displayed value, recalculate it.
                if(currentTime - lastRefreshTime > REFRESH_INTERVAL_SECONDS)
                {
                    // Calculate the average SPF over the last AVG_OVER_FRAMES frames,
                    // ignoring negative values (with which the array is filled initially).
                    const std::pair<size_t, double> sumFrameSecondsPair = std::accumulate(
                        frameTimes.cbegin() + 1,
                        frameTimes.cend(),
                        std::make_pair(static_cast<size_t>(1), frameTimes[0]),
                        [](std::pair<size_t, double> acc, const double cur)
                        {
                            if(cur >= 0)
                            {
                                acc.first++;
                                acc.second += cur;
                            }

                            return acc;
                        }
                    );

                    avgFrameSeconds = sumFrameSecondsPair.second / sumFrameSecondsPair.first;

                    lastRefreshTime = currentTime;
                }

                const std::string fpsCounterText(
                    str(
                        format("FPS: %1$.1f")
                            % (1.0 / avgFrameSeconds)
                    )
                );

                textRenderer.renderText(font, screenPosition, fpsCounterText);
            }
        }
    }
}
