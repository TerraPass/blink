#include "AnimatedSpriteFactory.h"

#include <cassert>

#include "utils.h"

namespace blink
{
    namespace gfx
    {
        // AnimatedSpriteFactory will identify trivial animations it creates
        // starting from this big number (as opposed to AnimationFactory, which starts from 1).
        // This is so there is no intersection between IDs of regular and trivial animations.
        U32 AnimatedSpriteFactory::nextTrivialAnimId = 2147483648 + 1;   // 2**31 + 1

        static inline Animation makeTrivialAnimation(SDL_Texture* const pstaticSprite, const U32 duration, const U32 id)
        {
            return Animation({Animation::Frame(getEntireTextureRect(pstaticSprite), duration)}, pstaticSprite, id);
        }

        AnimatedSprite AnimatedSpriteFactory::create(const io::data::AnimationData& data, sdl2utils::SDL_TexturePtr pspriteSheet) const
        {
            assert(pspriteSheet);
            const auto pspriteSheetRaw = pspriteSheet.get();
            return AnimatedSprite(std::move(pspriteSheet), animationsFactory.create(data, pspriteSheetRaw));
            // TODO: The following works on GCC, Clang and MSVC but does not work properly when compiled with Apple LLVM.
            // On Apple LLVM it appears that at the time of the call to pspriteSheet.get() the unique_ptr
            // has already been moved from. This doesn't make sense, since std::move() does not actually move
            // and function arguments must be evaluated before the call to the constructor (which would move from pspriteSheet) is made.
            //return AnimatedSprite(std::move(pspriteSheet), animationsFactory.create(data, pspriteSheet.get());
        }

        AnimatedSprite AnimatedSpriteFactory::createStatic(sdl2utils::SDL_TexturePtr pstaticSprite) const
        {
            AnimationContainer trivialAnimContainer;
            // Ugh... Taking default frame tag name from container factory.
            trivialAnimContainer.insert(std::make_pair(std::string(AnimationContainerFactory::DEFAULT_FRAME_TAG_NAME), makeTrivialAnimation(pstaticSprite.get(), STATIC_TRIVIAL_FRAME_DURATION, nextTrivialAnimId++)));

            return AnimatedSprite(std::move(pstaticSprite), std::move(trivialAnimContainer));
        }
    }
}
