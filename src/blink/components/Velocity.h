#ifndef BLINK_COMPONENTS_VELOCITY_H
#define BLINK_COMPONENTS_VELOCITY_H

#include "common/numeric.h"

namespace blink
{
    namespace components
    {
        struct Velocity final
        {
            real x;
            real y;
            
            explicit Velocity(const real x = 0, const real y = 0)
            : x(x), y(y)
            {
                
            }
        };
    }
}

#endif /* VELOCITY_H */

