#ifdef BLINK_COMPILE_TEMPLATES
#include "TextRenderer.h"

#include "camera/TopDownCamera.h"
#include "camera/UiCamera.h"
#endif

#include "debug/logging.h"

namespace blink
{
    namespace gfx
    {
        template <typename Camera>
        void /*Incremental*/TextRenderer<Camera>::renderText(
            const Font& font, 
            const typename Camera::InputPosition& position, 
            const std::string& text/*, 
            boost::optional<const std::string&> maybeOldText*/
        )
        {
            // TODO: Support and handle multiline and tabulated text!

            auto currentPosition = position;
            for(const char ch : text)
            {
                BLINK_LOGF_E_IF(!font.hasChar(ch), "Font has no glyph for character \'%1%\' (ord: %1$#x)", ch);

                const Sprite charSprite = font.getSpriteForChar(ch);
                spriteRenderer.renderSprite(charSprite, currentPosition);
                currentPosition.x += camera.getScale()*charSprite.getWidth();
            }
        }

#ifdef BLINK_COMPILE_TEMPLATES
        // Explicit template instantiations
        template class TextRenderer<camera::TopDownCamera>;
        template class TextRenderer<camera::UiCamera>;
#endif
    }
}
