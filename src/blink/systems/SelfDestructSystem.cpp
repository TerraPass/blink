#include "SelfDestructSystem.h"

#include "components/SelfDestruct.h"

namespace blink
{
    namespace systems
    {
        void SelfDestructSystem::update(entityx::EntityManager& entities, entityx::EventManager& /*events*/, entityx::TimeDelta /*dt*/)
        {
            using namespace components;
            
            using entityx::Entity;

            entities.each<SelfDestruct>(
                [this](Entity entity, SelfDestruct& selfDestruct) {
                    if(timeKeeper.getGameplayElapsedSeconds() > selfDestruct.destructionTime)
                    {
                        entity.destroy();
                    }
                }
            );
        }
    }
}
