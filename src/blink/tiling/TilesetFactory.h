#ifndef BLINK_GFX_TILESETFACTORY_H
#define BLINK_GFX_TILESETFACTORY_H

#include "io/data/TilesetData.h"

#include "Tileset.h"

namespace blink
{
    namespace tiling
    {
        class TilesetFactory final
        {
        public:
            Tileset create(io::data::TilesetData data) const;
        };
    }
}

#endif /* BLINK_GFX_TILESETFACTORY_H */

