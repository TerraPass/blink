#include "JsonRetriever.h"

#include <fstream>

#include "io/exceptions.h"

namespace blink
{
    namespace io
    {
        namespace retrieval
        {
            using nlohmann::json;

            json JsonRetriever::retrieve(const FilePath& filePath) const
            {
                std::ifstream fin(filePath);
                if(!fin.good())
                {
                    throw io::detail::RawDataOpeningFailedExceptionImpl<json>(filePath);
                }

                json dataJson;

                try
                {
                    fin >> dataJson;
                }
                catch(const std::invalid_argument& e)
                {
                    throw io::detail::RawDataFormatExceptionImpl<json>(filePath, e.what());
                }

                if(!dataJson.is_structured())
                {
                    throw io::detail::RawDataFormatExceptionImpl<json>(filePath);
                }

                return dataJson;
            }
        }
    }
}
