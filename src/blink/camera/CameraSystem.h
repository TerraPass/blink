#ifndef BLINK_CAMERA_CAMERASYSTEM_H
#define BLINK_CAMERA_CAMERASYSTEM_H

#include "common/types.h"
#include "input/InputSystem.h"

#include "ICameraSystem.h"
#include "TopDownCamera.h"
#include "UiCamera.h"
#include "WasdMovedCamera.h"

namespace blink
{
    namespace camera
    {
        class CameraSystem final : public ICameraSystem
        {
            static constexpr const WorldPosition INITIAL_TOPDOWN_CAMERA_TARGET{0,0};

            WasdMovedCamera<TopDownCamera> topDownCamera;
            //TopDownCamera topDownCamera;
            UiCamera uiCamera;

        public:
            CameraSystem(const gfx::unit outputWidth, const gfx::unit outputHeight, input::InputSystem& inputSystem, const U8 topDownCameraScale, const U8 uiCameraScale);

            CameraSystem(const CameraSystem&) = delete;
            CameraSystem(CameraSystem&&) = default; // sic            

            CameraSystem& operator=(const CameraSystem&) = delete;
            CameraSystem& operator=(CameraSystem&&) = delete;

            inline void update(const real seconds)
            {
                topDownCamera.update(seconds);
            }

            virtual ~CameraSystem() = default;

            virtual TopDownCamera& getTopDownCameraImpl() override;
            virtual const TopDownCamera& getTopDownCameraImpl() const override;

            virtual UiCamera& getUiCameraImpl() override;
            virtual const UiCamera& getUiCameraImpl() const override;
        };
    }
}

#endif /* BLINK_CAMERA_CAMERASYSTEM_H */

