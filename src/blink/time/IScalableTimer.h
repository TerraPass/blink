#ifndef BLINK_TIME_ISCALABLETIMER_H
#define BLINK_TIME_ISCALABLETIMER_H

#include "ITimer.h"

#include "common/numeric.h"

namespace blink
{
    namespace time
    {
        class IScalableTimer : public virtual ITimer
        {
        public:
            IScalableTimer() = default;
            virtual ~IScalableTimer() = default;

            real getScale() const
            {
                return this->getScaleImpl();
            }
            
            void setScale(const real scale)
            {
                this->setScaleImpl(scale);
            }

        private:
            virtual real getScaleImpl() const = 0;
            virtual void setScaleImpl(const real scale) = 0;
        };
    }
}

#endif /* BLINK_TIME_ISCALABLETIMER_H */

