#include "typeinfo.h"

// Code in this file has been taken with minimal changes
// from the following answer by Ali on StackOverflow:
// http://stackoverflow.com/a/4541470/1051764


#ifdef __GNUG__
#include <cstdlib>
#include <memory>
#include <cxxabi.h>
#endif

namespace blink
{
    namespace debug
    {
        namespace detail
        {
#ifdef __GNUG__
            std::string demangle(const char* name)
            {
                int status = -1;

                std::unique_ptr<char, void(*)(void*)> res {
                    abi::__cxa_demangle(name, NULL, NULL, &status),
                    std::free
                };

                return (status==0) ? res.get() : name ;
            }
#else
            // Do nothing, if we are not on GCC
            std::string demangle(const char* name)
            {
                return name;
            }
#endif
        }
    }
}
