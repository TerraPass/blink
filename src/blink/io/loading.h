#ifndef BLINK_IO_LOADING_H
#define BLINK_IO_LOADING_H

#include "types.h"
#include "constants.h"
#include "Loader.h"
#include "resolvers/FilePathResolver.h"
#include "resolvers/FontKeyResolver.h"
#include "resolvers/TilesetKeyResolver.h"
#include "retrieval/JsonRetriever.h"
#include "retrieval/TextureRetriever.h"
#include "retrieval/FontRetriever.h"
#include "retrieval/RawTilesetDataRetriever.h"
#include "converters/JsonToAnimationDataConverter.h"
#include "converters/TextureToTextureDataConverter.h"
#include "converters/FontToFontDataConverter.h"
#include "converters/RawTilesetDataConverter.h"
#include "converters/JsonToTilemapDataConverter.h"

namespace blink
{
    namespace io
    {
        template <typename Data, typename RawDataRetriever, typename DataConverter>
        using FileLoader = Loader<Data, resolvers::FilePathResolver, RawDataRetriever, DataConverter>;

        using AnimationDataLoader = FileLoader<data::AnimationData, 
            retrieval::JsonRetriever, 
            converters::JsonToAnimationDataCoverter>;
        using TextureDataLoader = FileLoader<data::TextureData,
            retrieval::TextureRetriever,
            converters::TextureToTextureDataConverter>;

        using FontDataLoader = Loader<data::FontData,
            resolvers::FontKeyResolver,
            retrieval::FontRetriever,
            converters::FontToFontDataConverter,
            data::FontKey>;

        using TilesetDataLoader = Loader<data::TilesetData,
            resolvers::TilesetKeyResolver,
            retrieval::RawTilesetDataRetriever,
            converters::RawTilesetDataConverter,
            data::TilesetKey>;

        using TilemapDataLoader = FileLoader<data::TilemapData,
            retrieval::JsonRetriever,
            converters::JsonToTilemapDataConverter>;

        inline AnimationDataLoader makeAnimationDataLoader()
        {
            return AnimationDataLoader(
                resolvers::FilePathResolver(SPRITES_DATA_PATH, JSON_EXTENSION)
            );
        }

        inline TextureDataLoader makeTextureDataLoader(SDL_Renderer* const prenderer)
        {
            return TextureDataLoader(
                resolvers::FilePathResolver(SPRITES_PATH, PNG_EXTENSION),
                retrieval::TextureRetriever(prenderer)
            );
        }

        inline FontDataLoader makeFontDataLoader()
        {
            return FontDataLoader(
                resolvers::FontKeyResolver(FONTS_PATH, OTF_EXTENSION)
            );
        }

        inline TilesetDataLoader makeTilesetDataLoader(SDL_Renderer* const prenderer)
        {
            return TilesetDataLoader(
                resolvers::TilesetKeyResolver(TILESETS_PATH, JSON_EXTENSION, PNG_EXTENSION),
                retrieval::RawTilesetDataRetriever(prenderer)
            );
        }

        inline TilemapDataLoader makeTilemapDataLoader(SDL_Renderer* const prenderer)
        {
            return TilemapDataLoader(
                resolvers::FilePathResolver(TILEMAPS_PATH, JSON_EXTENSION),
                retrieval::JsonRetriever(),
                converters::JsonToTilemapDataConverter(TILEMAPS_PATH, prenderer)
            );
        }
    }
}

#endif /* BLINK_IO_LOADING_H */

