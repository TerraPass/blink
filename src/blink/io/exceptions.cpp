#include "exceptions.h"

#include <boost/format.hpp>

namespace blink
{
    namespace io
    {
        using boost::str;
        using boost::format;

        IOException::IOException(const std::string& message, const std::string& details)
        : common::BlinkException(str(format(MESSAGE_TEMPLATE) % message % details))
        {

        }
        
        KeyResolutionException::KeyResolutionException(const BasicKey& basicKey, const std::string& details)
        : IOException(str(format(MESSAGE_TEMPLATE) % basicKey), details), basicKey(basicKey)
        {

        }

        RawDataRetrievalException::RawDataRetrievalException(
            const std::string& rawDataTypeName, 
            const FilePath& source, 
            const std::string& details
        )
        : IOException(str(format(MESSAGE_TEMPLATE) % rawDataTypeName % source), details),
            rawDataTypeName(rawDataTypeName), source(source)
        {

        }

        RawDataOpeningFailedException::RawDataOpeningFailedException(const std::string& rawDataTypeName, const FilePath& source)
        : RawDataRetrievalException(rawDataTypeName, source, str(format(DETAILS_TEMPLATE) % source))
        {

        }
        
        RawDataFormatException::RawDataFormatException(const std::string& rawDataTypeName, const FilePath& source, const std::string& innerMessage)
        : RawDataRetrievalException(
            rawDataTypeName,
            source,
            str(format(DETAILS_TEMPLATE)
                % source
                % (innerMessage.empty()
                    ? ""
                    : str(format(" (%1%)") % innerMessage)))
        )
        {

        }

        DataConversionException::DataConversionException(
            const std::string& sourceTypeName, 
            const std::string& destTypeName, 
            const std::string& details
        )
        : IOException(str(format(MESSAGE_TEMPLATE) % sourceTypeName % destTypeName), details),
            sourceTypeName(sourceTypeName), destTypeName(destTypeName)
        {

        }
    }
}
