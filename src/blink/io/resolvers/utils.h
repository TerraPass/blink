#ifndef BLINK_IO_RESOLVERS_UTILS_H
#define BLINK_IO_RESOLVERS_UTILS_H

#include <string>

namespace blink
{
    namespace io
    {
        namespace resolvers
        {
            /**
             * Returns the path to the directory, containing the game executable.
             * (The returned path does not include the trailing slash.)
             * 
             * @return String, containing the path to executable's directory w/o trailing slash.
             */
            std::string getExecutableDirectory();
        }
    }
}

#endif /* BLINK_IO_RESOLVERS_UTILS_H */

