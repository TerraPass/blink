#ifndef BLINK_GFX_TYPES_H
#define BLINK_GFX_TYPES_H

#include <SDL.h>
#include <sdl2utils/pointers.h>

#include "common/numeric.h"
#include "common/data/Vector2.h"

namespace blink
{
    namespace gfx
    {
        using Rect = SDL_Rect;
        using Color = SDL_Color;

        using unit = decltype(Rect::x);

        using ScreenPosition = Vector2<unit>;
    }
}

#endif /* BLINK_GFX_TYPES_H */

