#ifndef BLINK_IO_CONVERTERS_JSONTOTILEMAPDATACONVERTER_H
#define BLINK_IO_CONVERTERS_JSONTOTILEMAPDATACONVERTER_H

#include <nlohmann_json.hpp>

#include "io/data/TilemapData.h"
#include "io/resolvers/FilePathResolver.h"
#include "io/retrieval/TextureRetriever.h"
#include "io/converters/TextureToTextureDataConverter.h"
#include "io/Loader.h"

#include "JsonToTilesetMetadataConverter.h"
#include "JsonToTilemapLayerDataConverter.h"

namespace blink
{
    namespace io
    {
        namespace converters
        {
            class JsonToTilemapDataConverter final
            {
                class TextureLoadingTilesetDataJsonConverter final
                {
                    // This alias is a workaround to avoid including io/loading.h,
                    // where similar TextureDataLoader alias is declared,
                    // since including it would lead to #include recursion.
                    using TextureLoader = Loader<data::TextureData,
                        resolvers::FilePathResolver,
                        retrieval::TextureRetriever,
                        converters::TextureToTextureDataConverter>;

                    JsonToTilesetMetadataConverter metadataConverter;
                    TextureLoader textureDataLoader;

                public:
                    TextureLoadingTilesetDataJsonConverter(const std::string& tilemapsBasePath, SDL_Renderer* const prenderer);

                    data::TilesetData convert(const nlohmann::json& metadataJson) const;
                };

                static constexpr const auto DEFAULT_BACKGROUNDCOLOR = "#00000000";

                TextureLoadingTilesetDataJsonConverter tilesetConverter;
                JsonToTilemapLayerDataConverter layerConverter;

            public:
                static constexpr const int FORMAT_VERSION = 1;

                JsonToTilemapDataConverter(const std::string& tilemapsBasePath, SDL_Renderer* const prenderer);

                JsonToTilemapDataConverter(const JsonToTilemapDataConverter&) = delete;
                JsonToTilemapDataConverter(JsonToTilemapDataConverter&&) = default; // sic

                JsonToTilemapDataConverter& operator=(const JsonToTilemapDataConverter&) = delete;
                JsonToTilemapDataConverter& operator=(JsonToTilemapDataConverter&&) = delete;

                data::TilemapData convert(const nlohmann::json& dataJson) const;
            };
        }
    }
}

#endif /* BLINK_IO_CONVERTERS_JSONTOTILEMAPDATACONVERTER_H */

