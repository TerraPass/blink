#include "JsonToTilemapDataConverter.h"

#include <functional>
#include <boost/format.hpp>

#include "debug/logging.h"
#include "debug/typeinfo.h"
#include "io/exceptions.h"
#include "io/resolvers/FilePathResolver.h"
#include "io/retrieval/TextureRetriever.h"

#include "utils.h"

namespace blink
{
    namespace io
    {
        namespace converters
        {

// JsonToTilemapDataConverter implementation {
            constexpr const int JsonToTilemapDataConverter::FORMAT_VERSION;
            constexpr const decltype(JsonToTilemapDataConverter::DEFAULT_BACKGROUNDCOLOR) JsonToTilemapDataConverter::DEFAULT_BACKGROUNDCOLOR;

            JsonToTilemapDataConverter::JsonToTilemapDataConverter(
                const std::string& tilemapsBasePath,
                SDL_Renderer * const prenderer
            )
            : tilesetConverter(tilemapsBasePath, prenderer),
                layerConverter()
            {

            }

            data::TilemapData JsonToTilemapDataConverter::convert(const nlohmann::json& dataJson) const try
            {
                try
                {
                    const auto jsonFormatVersion = dataJson.at("version");
                    if(jsonFormatVersion != FORMAT_VERSION)
                    {
                        throw detail::DataConversionExceptionImpl<nlohmann::json, data::TilemapData>(
                            boost::str(boost::format("Unsupported Tiled JSON version %1%; %2% expected version %3%")
                                % jsonFormatVersion
                                % debug::getTypename(*this)
                                % FORMAT_VERSION)
                        );
                    }
                }
                catch(const std::out_of_range&)
                {
                    BLINK_LOGF_E("Tilemap JSON specifies no \"version\"; assuming %1%; may be processed incorrectly, if the actual version is not %1%", FORMAT_VERSION);
                    BLINK_LOGF_I("Note that %1% supports only version %2% of Tiled JSON format", debug::getTypename(*this) % FORMAT_VERSION);
                }

                data::TilemapData data;

                data.tiledversion = dataJson.at("tiledversion");
                data.width = dataJson.at("width");
                data.height = dataJson.at("height");
                data.tilewidth = dataJson.at("tilewidth");
                data.tileheight = dataJson.at("tileheight");
                data.orientation = dataJson.at("orientation");
                data.renderorder = dataJson.at("renderorder");
                data.nextobjectid = dataJson.at("nextobjectid");

                //data.backgroundcolor = dataJson.at("backgroundcolor");
                const auto bgColorIt = dataJson.find("backgroundcolor");
                if(bgColorIt == dataJson.end())
                {
                    BLINK_LOGF_I("No \"backgroundcolor\" provided in tilemap JSON; \"%1%\" will be used by default", DEFAULT_BACKGROUNDCOLOR);
                    data.backgroundcolor = DEFAULT_BACKGROUNDCOLOR;
                }
                else
                {
                    data.backgroundcolor = (*bgColorIt);
                }
                
                convertJsonArray(data.layers, dataJson.at("layers"), layerConverter);
                convertJsonArray(data.tilesets, dataJson.at("tilesets"), tilesetConverter);

                readTiledProperties(data.properties, dataJson, false);

                return data;
            }
            catch(const std::out_of_range& e)
            {
                throw detail::DataConversionExceptionImpl<nlohmann::json, data::TilemapData>(e.what());
            }
            catch(const std::domain_error& e)
            {
                throw detail::DataConversionExceptionImpl<nlohmann::json, data::TilemapData>(e.what());
            }
//}

//JsonToTilemapDataConverter::TextureLoadingTilesetDataJsonConverter implementation {

            JsonToTilemapDataConverter::TextureLoadingTilesetDataJsonConverter::TextureLoadingTilesetDataJsonConverter(
                const std::string& tilemapsBasePath, SDL_Renderer * const prenderer
            )
            : metadataConverter(),
                textureDataLoader(
                    resolvers::FilePathResolver(tilemapsBasePath, ""),
                    retrieval::TextureRetriever(prenderer)
                )
            {

            }

            data::TilesetData JsonToTilemapDataConverter::TextureLoadingTilesetDataJsonConverter::convert(
                const nlohmann::json& metadataJson
            ) const
            {
                const data::TilesetMetadata metadata = metadataConverter.convert(metadataJson);
                return data::TilesetData(
                    textureDataLoader.load(metadata.imageFilePath),
                    metadata
                );
            }
//}
        }
    }
}
