#ifdef BLINK_COMPILE_TEMPLATES
#include "SpriteRenderer.h"

#include "camera/TopDownCamera.h"
#include "camera/UiCamera.h"
#endif

#include <sdl2utils/guards.h>

namespace blink
{
    namespace gfx
    { 
        template <typename Camera>
        void SpriteRenderer<Camera>::renderSprite(const Sprite& sprite, const typename Camera::InputPosition cameraInputPosition) const
        {
            // TODO: Maybe sprite textures should be pre-scaled on loading, instead of being scaled each time
            // they are rendered.
            // (Need to implement the alternative and run some benchmarks...)
            
            const auto screenPosition = camera.toScreenPosition(cameraInputPosition);

            Rect srcRect(sprite.getRect());
            Rect dstRect(camera.scaleRect(srcRect));
            dstRect.x = screenPosition.x;
            dstRect.y = screenPosition.y;            
            sdl2utils::guards::abbr::z(
                SDL_RenderCopy(
                    prenderer,
                    sprite.getSpriteSheetPtr(),
                    &srcRect,
                    &dstRect
                ),
                "SDL_RenderCopy() failed"
            );
        }

#ifdef BLINK_COMPILE_TEMPLATES
        // Explicit template instantiations
        template class SpriteRenderer<camera::TopDownCamera>;
        template class SpriteRenderer<camera::UiCamera>;
#endif
    }
}
