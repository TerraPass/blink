#ifndef BLINK_GFX_ANIMATIONCONTAINER_H
#define BLINK_GFX_ANIMATIONCONTAINER_H

//#include <vector>
#include <unordered_map>
//#include <algorithm>

#include "Animation.h"

namespace blink
{
    namespace gfx
    {
        /**
         * Map from strings (animation tags) to Animation instances.
         */
        using AnimationContainer = std::unordered_map<std::string, Animation>;

        // TODO: The actual class for AnimationContainer, which, for faster access,
        // would support integer indexes
        // in addition to string keys.
//        class AnimationContainer final
//        {
//            // TODO: Make animations available not only by string key, but also by an integer index.
////            std::unordered_map<std::string, size_t> animationIndexes;
////            std::vector<Animation> animations;
//
//        public:
//            /**
//             * 
//             * @param animationsBegin Starting iterator for a range of std::string-Animation pairs
//             * (elements of the collection are instances of std::pair<std::string, gfx::Animation>).
//             * @param animationsEnd Past-the-end iterator of the range.
//             */
////            template <typename InputIt>
////            explicit AnimationContainer(InputIt animationsBegin, InputIt animationsEnd)
////            : animationIndexes(), animations()
////            {
////                size_t i = 0;
////                std::for_each(
////                    animationsBegin,
////                    animationsEnd,
////                    [this, &i](auto& pair) {
////                        animationIndexes(pair.first, i++);
////                        animations.push_back(pair.second);
////                    }
////                );
////            }
//        };
    }
}

#endif /* BLINK_GFX_ANIMATIONCONTAINER_H */

