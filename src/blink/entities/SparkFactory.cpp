#include "SparkFactory.h"

#include "gfx/Animation.h"
#include "gfx/Sprite.h"
#include "components/Animation.h"
#include "components/Position.h"
#include "components/Sprite.h"
#include "components/Velocity.h"
#include "core/ITimeKeeper.h"

namespace blink
{
    namespace entities
    {
        using namespace entityx;
        using namespace blink::components;

        SparkFactory::SparkFactory(
            const core::ContentManager& contentManager,
            EntityManager& entityManager,
            const core::ITimeKeeper& timeKeeper
        )
        : contentManager(contentManager), entityManager(entityManager), timeKeeper(timeKeeper)
        {

        }

        Entity SparkFactory::create(const real posX, const real posY, const real velX, const real velY, const real initialAnimTime)
        {
            Entity entity = entityManager.create();
            entity.assign<Position>(posX, posY);
            entity.assign<Velocity>(velX, velY);
            entity.assign<Sprite>(contentManager.getSparkAnimatedSprite().getFirstSprite());
            entity.assign<Animation>(contentManager.getSparkAnimatedSprite().getAnimation(ANIMATION_KEY), timeKeeper.getGameplayElapsedSeconds(), initialAnimTime);
            
            return entity;
        }
    }
}
