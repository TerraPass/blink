#ifndef BLINK_CAMERA_TYPES_H
#define BLINK_CAMERA_TYPES_H

#include "common/types.h"
#include "gfx/types.h"

namespace blink
{
    namespace camera
    {
        using ScreenPosition = gfx::ScreenPosition;
        using Rect = gfx::Rect;
    }
}

#endif /* BLINK_CAMERA_TYPES_H */

