#ifndef BLINK_INPUT_MOUSEPOSITION_H
#define BLINK_INPUT_MOUSEPOSITION_H

#include <ostream>

#include "common/data/Vector2.h"

#include "types.h"

namespace blink
{
    namespace input
    {   
        struct MousePosition final
        {
            screen_unit x;
            screen_unit y;

            MousePosition()
            : x(0), y(0)
            {
                
            }

            MousePosition(const screen_unit x, const screen_unit y)
            : x(x), y(y)
            {
                
            }

            explicit MousePosition(const Vector2<screen_unit>& vector)
            : x(vector.x), y(vector.y)
            {
                
            }

            /*implicit*/ operator Vector2<screen_unit>() const
            {
                return Vector2<screen_unit>(x, y);
            }
        };

        std::ostream& operator<<(std::ostream& out, const MousePosition& mousePosition);
    }
}

#endif /* BLINK_INPUT_MOUSEPOSITION_H */

