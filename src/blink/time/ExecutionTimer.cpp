#include "ExecutionTimer.h"

namespace blink
{
    namespace time
    {   
        bool ExecutionTimer::pauseImpl()
        {
            if(!this->isPaused)
            {
                this->accumulatedDuration += (getCurrentTime() - this->startTime);
                this->isPaused = true;
                return true;
            }
            else
            {
                return false;
            }
        }
        
        bool ExecutionTimer::resumeImpl()
        {
            if(this->isPaused)
            {
                this->startTime = getCurrentTime();
                this->isPaused = false;
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
