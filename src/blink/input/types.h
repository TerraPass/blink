#ifndef BLINK_INPUT_TYPES_H
#define BLINK_INPUT_TYPES_H

#include <SDL.h>

#include "gfx/types.h"

namespace blink
{
    namespace input
    {
        // TODO: Use custom enums, instead of SDL constants.
        // A header with their definition can probably be generated at built time via CMake.
        using Scancode  = SDL_Scancode;
        using Keycode   = SDL_Keycode;

        using screen_unit = gfx::unit;
    }
}

#endif /* BLINK_INPUT_TYPES_H */

