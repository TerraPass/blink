#include "TextureRetriever.h"

#include <cassert>

#include <SDL.h>
#include <SDL_image.h>
#include <sdl2utils/exceptions.h>
#include <sdl2utils/guards.h>

#include "io/exceptions.h"

namespace blink
{
    namespace io
    {
        namespace retrieval
        {
            TextureRetriever::TextureRetriever(SDL_Renderer * const prenderer)
            : prenderer(prenderer), surfaceRetriever()
            {
                assert(prenderer != nullptr && "prenderer must not be null");
            }

            template <typename Exception>
            [[noreturn]] inline void wrapAndRethrow(const Exception& e, const FilePath& filePath)
            {
                throw detail::RawDataRetrievalExceptionImpl<TextureRetriever::TexturePtr>(filePath, e.what());
            }

            TextureRetriever::TexturePtr TextureRetriever::retrieve(const FilePath& filePath) const try
            {
                return TexturePtr(sdl2utils::guards::ensureNotNull(
                    SDL_CreateTextureFromSurface(prenderer, surfaceRetriever.retrieve(filePath).get()),
                    "SDL_CreateTextureFromSurface() return value"
                ));
            }
            catch(const sdl2utils::SDLErrorException& e)
            {
                wrapAndRethrow(e, filePath);
            }
            catch(const RawDataRetrievalException& e)   // Catch possible surfaceRetriever exception and wrap it
            {
                wrapAndRethrow(e, filePath);
            }
        }
    }
}
