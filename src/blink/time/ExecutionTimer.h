#ifndef BLINK_TIME_EXECUTIONTIMER_H
#define BLINK_TIME_EXECUTIONTIMER_H

#include <chrono>

#include "ITimer.h"
#include "utils.h"

namespace blink
{
    namespace time
    {
        using namespace std::chrono;

        class ExecutionTimer : public virtual ITimer
        {
        protected:
            using ExecutionClock = high_resolution_clock;
            using TimePoint = ExecutionClock::time_point;
            using Duration = ExecutionClock::duration;
            
        private:
            TimePoint startTime;
            Duration accumulatedDuration;
            
            bool isPaused;
            
        protected:
            inline static TimePoint getCurrentTime()
            {
                return ExecutionClock::now();
            }

        public:
            inline explicit ExecutionTimer(bool startPaused = false)
                : startTime(getCurrentTime()), accumulatedDuration(0), isPaused(startPaused)
            {
                
            }

            virtual ~ExecutionTimer() = default;
            
        private:
            virtual real getElapsedSecondsImpl() const override
            {
                return toSeconds(
                    this->isPaused
                    ? this->accumulatedDuration
                    : this->accumulatedDuration + (getCurrentTime() - this->startTime)
                );
            }
            
            inline virtual bool isPausedImpl() const override
            {
                return this->isPaused;
            }

            virtual bool pauseImpl() override;
            virtual bool resumeImpl() override;
            
        protected:
            inline void setAccumulatedDuration(const Duration duration)
            {
                this->accumulatedDuration = duration;
            }

            inline Duration getAccumulatedDuration() const
            {
                return this->accumulatedDuration;
            }
            
            inline void setStartTime(const TimePoint timePoint)
            {
                this->startTime = timePoint;
            }
            
            inline TimePoint getStartTime() const
            {
                return this->startTime;
            }
            
            inline void setPaused(const bool paused)
            {
                this->isPaused = paused;
            }
            
            inline bool getPaused() const
            {
                return this->isPaused;
            }
        };
    }
}

#endif /* BLINK_TIME_EXECUTIONTIMER_H */

