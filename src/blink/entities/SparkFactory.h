#ifndef BLINK_ENTITIES_SPARKFACTORY_H
#define BLINK_ENTITIES_SPARKFACTORY_H

#include <entityx/entityx.h>

#include "core/ContentManager.h"
#include "core/ITimeKeeper.h"
#include "gfx/types.h"
#include "gfx/Sprite.h"
#include "gfx/Animation.h"

namespace blink
{
    namespace entities
    {
        class SparkFactory final
        {
            static constexpr const auto ANIMATION_KEY = "Loop";

            const core::ContentManager& contentManager;
            entityx::EntityManager& entityManager;
            const core::ITimeKeeper& timeKeeper;

        public:
            SparkFactory(
                const core::ContentManager& contentManager, 
                entityx::EntityManager& entityManager, 
                const core::ITimeKeeper& timeKeeper
            );

            entityx::Entity create(const real posX, const real posY, const real velX, const real velY, const real initialAnimTime = 0.0);
            
            inline entityx::Entity create()
            {
                return this->create(0.0, 0.0, 0.0, 0.0);
            }
        };
    }
}

#endif /* BLINK_ENTITIES_SPARKFACTORY_H */

