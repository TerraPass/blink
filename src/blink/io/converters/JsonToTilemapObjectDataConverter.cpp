#include "JsonToTilemapObjectDataConverter.h"

#include <stdexcept>
#include <cassert>

#include "debug/logging.h"
#include "io/exceptions.h"

namespace blink
{
    namespace io
    {
        namespace converters
        {
            JsonToTilemapObjectDataConverter::ObjectData JsonToTilemapObjectDataConverter::convert(const nlohmann::json& objectJson) const try
            {
                ObjectData data;
                data.kind = ObjectData::Kind::RECTANGLE;

                data.id = objectJson.at("id");
                data.name = objectJson.at("name");
                data.type = objectJson.at("type");
                data.visible = objectJson.at("visible");
                data.width = objectJson.at("width");
                data.height = objectJson.at("height");
                data.rotation = objectJson.at("rotation");
                data.x = objectJson.at("x");
                data.y = objectJson.at("y");

                auto it = objectJson.cend();
                if(it = objectJson.find("point"), it != objectJson.cend() && (*it))
                {
                    data.kind = ObjectData::Kind::POINT;
                    data.point = true;
                }
                else if(it = objectJson.find("ellipse"), it != objectJson.cend() && (*it))
                {
                    data.kind = ObjectData::Kind::ELLIPSE;
                    data.ellipse = true;
                }
                else if(it = objectJson.find("polygon"), it != objectJson.cend())                
                {
                    data.kind = ObjectData::Kind::POLYGON;
                    BLINK_LOG_W("Parsing object data of kind \"polygon\" is not yet supported");
                }
                else if(it = objectJson.find("polyline"), it != objectJson.cend())
                {
                    data.kind = ObjectData::Kind::POLYLINE;
                    BLINK_LOG_W("Parsing object data of kind \"polyline\" is not yet supported");
                }
                else if(it = objectJson.find("gid"), it != objectJson.cend())
                {
                    data.kind = ObjectData::Kind::TILE;
                    data.gid = (*it);
                }
                else if(it = objectJson.find("text"), it != objectJson.cend())
                {
                    data.kind = ObjectData::Kind::TEXT;
                    BLINK_LOG_W("Parsing object data of kind \"text\" is not yet supported");
                }

//                if(data.kind == ObjectData::Kind::UNKNOWN)
//                {
//                    BLINK_LOGF_E("Failed to determine the kind of the object with ID %1%", data.id);
//                }
                assert(data.kind != ObjectData::Kind::UNKNOWN);

                return data;
            }
            catch(const std::out_of_range& e)
            {
                throw detail::DataConversionExceptionImpl<nlohmann::json, ObjectData>(e.what());
            }
            catch(const std::domain_error& e)
            {
                throw detail::DataConversionExceptionImpl<nlohmann::json, ObjectData>(e.what());
            }
        }
    }
}
