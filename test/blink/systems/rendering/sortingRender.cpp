#ifndef BLINK_TESTING
#error "BLINK_TESTING must be defined when buidling tests"
#endif

#include "catch.hpp"

#include <limits>
#include <vector>

#include "random.h"

#include "systems/rendering/sortingRender.h"
#include "common/numeric.h"

using namespace blink;
using blink::systems::rendering::sortingRender;

namespace
{
    struct FakeSprite
    {
    private:
        static int nextId;

        int id;

    public:
        FakeSprite()
        : id(nextId++)
        {

        }

        inline int getId() const
        {
            return id;
        }
    };

    int FakeSprite::nextId = 0;

    struct FakeScreenPosition
    {
        real priority;
    };

    struct MockRenderer
    {
        using Sprite = FakeSprite;
        using ScreenPosition = FakeScreenPosition;

    private:
        mutable size_t renderCalls;
        mutable bool ascendingPriorityOnly;

        mutable real lastPriority;

        mutable std::vector<int> spriteIds;

    public:
        MockRenderer()
        : renderCalls(0), ascendingPriorityOnly(true), lastPriority(std::numeric_limits<real>::min())
        {

        }
        virtual ~MockRenderer()
        {

        }

        void renderSprite(const Sprite& sprite, FakeScreenPosition position) const
        {
            renderCalls++;
            ascendingPriorityOnly = ascendingPriorityOnly && position.priority > lastPriority;
            lastPriority = position.priority;

            spriteIds.emplace_back(sprite.getId());
        }

        inline size_t getRenderCalls() const
        {
            return renderCalls;
        }

        inline bool isAscendingPriorityOnly() const
        {
            return ascendingPriorityOnly;
        }

        inline const std::vector<int>& getSpriteIds() const
        {
            return spriteIds;
        }
    };

    struct FakeRenderable
    {
        FakeSprite sprite;

        bool valid;
        real priority;

        explicit FakeRenderable(const real priority)
        : sprite(), valid(true), priority(priority)
        {

        }

        inline FakeSprite getSprite() const
        {
            return sprite;
        }

        inline FakeScreenPosition getPosition() const
        {
            return {priority};
        }

        inline bool isValid() const
        {
            return valid;
        }
    };

    inline bool operator<(const FakeRenderable& a, const FakeRenderable& b)
    {
        return a.priority < b.priority;
    }
}

TEST_CASE("sortingRender calls renderer once per each renderable", "[systems][rendering][sortingRender]")
{
    const unsigned int seed = 712847;

    srand(seed);

    MockRenderer renderer;
    std::vector<FakeRenderable> renderables;
    

    SECTION("Renderer is called as many times as there are renderables")
    {
        SECTION("No calls to renderer when there are no renderables")
        {
            sortingRender(renderer, renderables);

            REQUIRE(renderer.getRenderCalls() == 0);
        }
        
        SECTION("Number of calls to renderer changes correctly with addition/removal of renderables")
        {
            const size_t renderablesToAdd = 500;
            for(size_t i = 0; i < renderablesToAdd; i++)
            {
                renderables.emplace_back(randreal<real>());
            }

            SECTION("Renderer is called a correct number of times after renderable entities are added")
            {
                sortingRender(renderer, renderables);

                REQUIRE(renderer.getRenderCalls() == renderablesToAdd);
            }

            const size_t renderablesToRemove = 249;
            for(size_t i = 0; i < renderablesToRemove; i++)
            {
                renderables.pop_back();
            }

            SECTION("Renderer is called a correct number of times after some renderables are removed")
            {
                sortingRender(renderer, renderables);

                REQUIRE(renderer.getRenderCalls() == renderablesToAdd - renderablesToRemove);
            }

            SECTION("Renderer is called a correct number of times after some renderables are added and some are removed")
            {
                const size_t newRenderablesToAdd = 22;
                for(size_t i = 0; i < newRenderablesToAdd; i++)
                {
                    renderables.emplace_back(randreal<real>());
                }

                sortingRender(renderer, renderables);

                REQUIRE(renderer.getRenderCalls() == renderablesToAdd - renderablesToRemove + newRenderablesToAdd);
            }
        }
    }
}

TEST_CASE("sortingRender forwards renderables in the order they are given", "[systems][rendering][sortingRender]")
{
    const size_t renderablesCount = 500;
    const unsigned int seed = 123365;

    srand(seed);

    MockRenderer renderer;
    std::vector<FakeRenderable> renderables;
    for(size_t i = 0; i < renderablesCount; i++)
    {
        renderables.emplace_back(randreal<real>());
    }

    std::vector<int> spriteIds;
    std::transform(renderables.cbegin(), renderables.cend(), std::back_inserter(spriteIds), [](const auto& r){return r.sprite.getId();});

    sortingRender(renderer, renderables);

    bool sameOrder = true;
    for(size_t i = 0; i < renderablesCount; i++)
    {
        if(renderer.getSpriteIds().at(i) != spriteIds[i])
        {
            sameOrder = false;
        }
    }

    REQUIRE(sameOrder);
}

TEST_CASE("sortingRender sorts renderables in asc. order of priority", "[systems][rendering][sortingRender]")
{
    const size_t renderablesCount = 500;
    const unsigned int seed = 776231;

    srand(seed);

    MockRenderer renderer;
    std::vector<FakeRenderable> renderables;
    for(size_t i = 0; i < renderablesCount; i++)
    {
        renderables.emplace_back(randreal<real>());
    }

    sortingRender(renderer, renderables);

    REQUIRE(std::is_sorted(renderables.cbegin(), renderables.cend()));
}

TEST_CASE("sortingRender does not change the number of renderables", "[systems][rendering][sortingRender]")
{
    const size_t renderablesCount = 20;
    const unsigned int seed = 88011;
    
    srand(seed);

    MockRenderer renderer;
    std::vector<FakeRenderable> renderables;
    for(size_t i = 0; i < renderablesCount; i++)
    {
        renderables.emplace_back(randreal<real>());
    }

    sortingRender(renderer, renderables);

    REQUIRE(renderables.size() == renderablesCount);
}

TEST_CASE("sortingRender does not introduce duplicates", "[systems][rendering][sortingRender]")
{
    const size_t renderablesCount = 52;
    const unsigned int seed = 98513;
    
    srand(seed);

    MockRenderer renderer;
    std::vector<FakeRenderable> renderables;
    for(size_t i = 0; i < renderablesCount; i++)
    {
        renderables.emplace_back(randreal<real>());
    }

    sortingRender(renderer, renderables);

    std::sort(renderables.begin(), renderables.end());

    REQUIRE(
        std::adjacent_find(
            renderables.cbegin(),
            renderables.cend(), 
            [](const auto& ra, const auto& rb) {return ra.sprite.getId() == rb.sprite.getId();}
        ) == renderables.end()
    );
}
