#include "AnimationContainerFactory.h"

#include <stdexcept>
#include <cassert>
#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>

#include "debug/logging.h"

namespace blink
{
    namespace gfx
    {
        constexpr const decltype(AnimationContainerFactory::DEFAULT_FRAME_TAG_NAME) AnimationContainerFactory::DEFAULT_FRAME_TAG_NAME;
        constexpr const Animation::Direction AnimationContainerFactory::DEFAULT_DIRECTION;

        AnimationContainer AnimationContainerFactory::create(
            const io::data::AnimationData& data, 
            SDL_Texture * const pspriteSheet
        ) const
        {
            static const std::unordered_map<std::string, Animation::Direction> directions = {
                {"forward", Animation::Direction::FORWARD},
                {"reverse", Animation::Direction::REVERSE},
                {"pingpong", Animation::Direction::PING_PONG}
            };

            assert(data.meta.format == std::string("RGBA8888") && "Expecting AnimationData to specify format RGBA8888 for the texture");

#if BLINK_LOG_LEVEL >= BLINK_LOG_ERROR
            {
                int w = -1;
                int h = -1;
                SDL_QueryTexture(pspriteSheet, nullptr, nullptr, &w, &h);
                if(static_cast<U32>(w) != data.meta.sizeW || static_cast<U32>(h) != data.meta.sizeH)
                {
                    BLINK_LOGF_E(
                        "Sprite sheet size does not match metadata for %1% : meta size (%2%;%3%), real size (%4%,%5%)",
                        data.meta.image % data.meta.sizeW % data.meta.sizeH % w % h
                    );
                }
            }
#endif

            const size_t framesCount = data.frames.size();

            AnimationContainer animations;

            BLINK_LOGF_D(
                "Animation metadata for image %1% specifies %2% frame tag(s)",
                data.meta.image % data.meta.frameTags.size()
            );

            if(!data.meta.frameTags.empty())
            {
                for(const auto& frameTag : data.meta.frameTags)
                {
                    if(frameTag.from >= framesCount || frameTag.to >= framesCount || frameTag.from > frameTag.to)
                    {
                        throw std::invalid_argument("Invalid frame tag bounds");
                    }

                    BLINK_LOGF_D(
                        "Tag \"%1%\" (frames %2% to %3%, %4%)",
                        frameTag.name % frameTag.from % frameTag.to % frameTag.direction
                    );

                    animations.insert(std::make_pair(
                        frameTag.name,
                        animationFactory.create(
                            pspriteSheet,
                            data.frames.cbegin() + frameTag.from,
                            data.frames.cbegin() + frameTag.to + 1,
                            directions.at(boost::to_lower_copy<std::string>(frameTag.direction))
                        )
                    ));
                }
            }
            else
            {
                BLINK_LOGF_I(
                    "No frame tags in animation metadata for image %1%; tag \"%2%\" with all frames in forward direction will be created",
                    data.meta.image % DEFAULT_FRAME_TAG_NAME
                );

                animations.insert(std::make_pair(
                    DEFAULT_FRAME_TAG_NAME,
                    animationFactory.create(
                        pspriteSheet,
                        data.frames.cbegin(),
                        data.frames.cend(),
                        DEFAULT_DIRECTION
                    )
                ));
            }

            return animations;
        }
    }
}
