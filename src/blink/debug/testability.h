#ifndef BLINK_DEBUG_TESTABILITY_H
#define BLINK_DEBUG_TESTABILITY_H

#ifdef BLINK_TESTING
#define BLINK_TESTABLE_PRIVATE public
#else
#define BLINK_TESTABLE_PRIVATE private
#endif

#ifdef BLINK_TESTING
#define BLINK_TESTABLE_PROTECTED public
#else
#define BLINK_TESTABLE_PROTECTED protected
#endif

#ifdef BLINK_TESTING
#define BLINK_TESTING_ONLY(code) code
#else
#define BLINK_TESTING_ONLY(code)
#endif

#endif /* BLINK_DEBUG_TESTABILITY_H */
