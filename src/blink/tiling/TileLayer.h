#ifndef BLINK_GFX_TILELAYER_H
#define BLINK_GFX_TILELAYER_H

#include <memory>

#include "Tile.h"
#include "TileLayerData.h"
#include "MapTilesContainer.h"

namespace blink
{
    namespace tiling
    {
        class TileLayer final
        {
            ConstMapTilesContainerPtr pmapTiles;

            //std::string name;
            TileLayerData data;
            //tileunit x;
            //tileunit y;
            real opacity;
            bool visible;
            // TODO: Store properties (possibly still in type-erased form).

        public:
            static constexpr const real DEFAULT_OPACITY = 1.0;
            static constexpr const bool DEFAULT_VISIBILITY = true;

            explicit TileLayer(
                ConstMapTilesContainerPtr mapTiles,
                const TileLayerData& data,
                const real opacity = DEFAULT_OPACITY,
                const bool visible = DEFAULT_VISIBILITY
            )
            : pmapTiles(mapTiles), data(data), opacity(opacity), visible(visible)
            {
                
            }

            explicit TileLayer(
                ConstMapTilesContainerPtr mapTiles,
                TileLayerData&& data,
                const real opacity = DEFAULT_OPACITY,
                const bool visible = DEFAULT_VISIBILITY
            )
            : pmapTiles(mapTiles), data(std::move(data)), opacity(opacity), visible(visible)
            {
                
            }

            inline Tile at(const tileunit x, const tileunit y) const
            {
                return pmapTiles->at(data.at(x, y));
            }

            inline Tile at(const TilePosition position) const
            {
                return pmapTiles->at(data.at(position));
            }

            inline Tile operator[](const TilePosition position) const
            {
                return (*pmapTiles)[data[position]];
            }

            inline MaybeTile atMaybe(const tileunit x, const tileunit y) const
            {
                const auto gid = data.at(x, y);
                return gid > 0
                    ? MaybeTile(pmapTiles->at(gid))
                    : MaybeTile();
            }

            inline MaybeTile atMaybe(const TilePosition position) const
            {
                return atMaybe(position.x, position.y);
            }

            inline tileunit getWidth() const
            {
                return data.getWidth();
            }

            inline tileunit getHeight() const
            {
                return data.getHeight();
            }

            inline U64 getDataVersion() const
            {
                return data.getVersion();
            }

            inline pixelunit getTileWidth() const
            {
                return pmapTiles->getTileWidth();
            }

            inline pixelunit getTileHeight() const
            {
                return pmapTiles->getTileHeight();
            }

            inline real getOpacity() const
            {
                return opacity;
            }

            inline U8 getOpacityByte() const
            {
//                static real lastOpacity = 1.0;
//                static U8 lastOpacityByte = 0xFF;
//
//                if(lastOpacity != opacity)
//                {
//                    lastOpacity = opacity;
//                    lastOpacityByte = static_cast<U8>(static_cast<real>(0xFF) * opacity);
//                }
//
//                return lastOpacityByte;
                return static_cast<U8>(static_cast<real>(0xFF) * opacity);
            }

            inline bool isVisible() const
            {
                return visible;
            }
        };
    }
}

#endif /* BLINK_GFX_TILELAYER_H */

