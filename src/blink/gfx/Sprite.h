#ifndef BLINK_GFX_SPRITE_H
#define BLINK_GFX_SPRITE_H

#include <cassert>

#include "common/numeric.h"

#include "types.h"

namespace blink
{
    namespace gfx
    {
        class Sprite final
        {
            // Rect within the spritesheet
            Rect rect;
            SDL_Texture* pspriteSheet;
            
        public:
            inline Sprite(const Rect rect, SDL_Texture* const pspriteSheet)
            : rect(rect), pspriteSheet(pspriteSheet)
            {
                assert(pspriteSheet != nullptr && "Sprite must be given a non-null pspriteSheet");
            }
            
            explicit Sprite(SDL_Texture* const pspriteTexture);
            
            Sprite(const Sprite&) = default;
            Sprite(Sprite&&) = default;
            
            Sprite& operator=(const Sprite&) = default;
            Sprite& operator=(Sprite&&) = default; 
            
            ~Sprite() = default;

            inline unit getWidth() const
            {
                return this->rect.w;
            }
            
            inline unit getHeight() const
            {
                return this->rect.h;
            }

            inline Rect getRect() const
            {
                return this->rect;
            }

            inline SDL_Texture* getSpriteSheetPtr() const
            {
                return this->pspriteSheet;
            }
        };
    }
}

#endif /* BLINK_GFX_SPRITE_H */

