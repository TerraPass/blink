#ifndef BLINK_IO_DATA_TILEMAPDATA_H
#define BLINK_IO_DATA_TILEMAPDATA_H

#include <string>
#include <vector>
#include <map>
#include <boost/any.hpp>

#include "TilesetData.h"

namespace blink
{
    namespace io
    {
        namespace data
        {
            struct TilemapData final
            {
                static constexpr const auto DISPLAY_NAME = "tilemap";

                using Properties = std::map<std::string, boost::any>;

                struct LayerData final
                {
                    static constexpr const auto DISPLAY_NAME = "tilemap layer data";

                    enum class Type
                    {
                        UNKNOWN = 0,
                        TILE    = 1,
                        OBJECT  = 2
                        // TODO: Add support for imagelayer
                    };

                    struct ObjectData final
                    {
                        static constexpr const auto DISPLAY_NAME = "tilemap object data";

                        enum class Kind
                        {
                            UNKNOWN     = 0,
                            RECTANGLE   = 1,
                            POINT       = 2,
                            ELLIPSE     = 3,
                            POLYGON     = 4,
                            POLYLINE    = 5,
                            TILE        = 6,
                            TEXT        = 7
                        };

                        Kind kind;
                        I64 id;
                        U32 width;
                        U32 height;
                        std::string name;
                        std::string type;
                        Properties properties;
                        bool visible;
                        I32 x;
                        I32 y;
                        real rotation;
                        I64 gid;
                        bool point;
                        bool ellipse;
                        // TODO
                        // ??? polygon;
                        // ??? polyline;
                        // ??? text;
                    };

                    U32 width;
                    U32 height;
                    std::string name;
                    Type type;
                    std::string rawType;
                    bool visible;
                    I32 x;
                    I32 y;
                    std::vector<I64> data;
                    std::vector<ObjectData> objects;
                    Properties properties;
                    real opacity;
                    std::string draworder;
                };

//                struct ExternalTilesetData final
//                {
//                    U8 firstgid;
//                    FilePath source;
//                };

                std::string tiledversion;
                U32 width;
                U32 height;
                U8 tilewidth;
                U8 tileheight;
                std::string orientation;
                std::vector<LayerData> layers;
                // TODO: Consider adding support for external tilesets in the future.
//                std::vector<ExternalTilesetData> tilesets;
                std::vector<TilesetData> tilesets;
                std::string backgroundcolor;
                std::string renderorder;
                Properties properties;
                U64 nextobjectid;
            };
        }
    }
}

#endif /* BLINK_IO_DATA_TILEMAPDATA_H */

