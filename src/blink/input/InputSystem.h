#ifndef BLINK_INPUT_INPUTSYSTEM_H
#define BLINK_INPUT_INPUTSYSTEM_H

#include "common/events.h"

#include "event_args.h"

namespace blink
{
    namespace input
    {
        class InputSystem
        {
        public:
            template <typename EventArgs>
            using Event = common::Event<InputSystem, EventArgs>;

        private:
            Event<void> appShutdownRequestedEvent;

            Event<MouseEventArgs> mouseButtonDownEvent;

            Event<KeyboardEventArgs> keyDownEvent;
            Event<KeyboardEventArgs> keyUpEvent;

        public:
            InputSystem();
            
            InputSystem(const InputSystem&) = delete;
            InputSystem& operator=(const InputSystem&) = delete;
            
            InputSystem(InputSystem&&) = default;   // sic!
            InputSystem& operator=(InputSystem&&) = delete;

            void update();

            inline Event<void>& getAppShutdownRequestedEvent()
            {
                return this->appShutdownRequestedEvent;
            }

            inline Event<MouseEventArgs>& getMouseButtonDownEvent()
            {
                return this->mouseButtonDownEvent;
            }

            inline Event<KeyboardEventArgs>& getKeyDownEvent()
            {
                return this->keyDownEvent;
            }

            inline Event<KeyboardEventArgs>& getKeyUpEvent()
            {
                return this->keyUpEvent;
            }
        };
    }
}

#endif /* BLINK_INPUT_INPUTSYSTEM_H */

