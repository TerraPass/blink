#ifndef BLINK_COMMON_RANDOM_H
#define BLINK_COMMON_RANDOM_H

#include <random>
#include <type_traits>
#include <ctime>

#include "numeric.h"

namespace blink
{
    using RandomEngine = std::mt19937;
    using Seed = RandomEngine::result_type;

    namespace detail
    {
        template <typename Result, typename Distribution>
        class BaseRandom
        {
            RandomEngine randomEngine;
            Distribution distribution;

        public:
            template <typename... DistributionArgs>
            explicit BaseRandom(const Seed seed, DistributionArgs&&... distributionArgs)
            : randomEngine(seed), distribution(std::forward<DistributionArgs>(distributionArgs)...)
            {

            }

            inline Result next()
            {
                return distribution(randomEngine);
            }
        };

        template <typename Result>
        struct RandomTraits
        {
            using UniformDistribution = std::uniform_int_distribution<Result>;
        };

        template <>
        struct RandomTraits<real>
        {
            using UniformDistribution = std::uniform_real_distribution<real>;
        };
    }

    template <typename Result, typename Distribution = typename detail::RandomTraits<Result>::UniformDistribution>
    class Random final : public detail::BaseRandom<Result, Distribution>
    {
        static_assert(std::is_arithmetic<Result>::value, "Result must be an arithmetic type in Random<Result>");

    public:
        template <typename... DistributionArgs>
        explicit Random(const Seed seed, DistributionArgs&&... distributionArgs)
        : detail::BaseRandom<Result, Distribution>(seed, std::forward<DistributionArgs>(distributionArgs)...)
        {

        }
    };

    template <typename Result, 
            typename Distribution = typename detail::RandomTraits<Result>::UniformDistribution, 
            typename... DistributionArgs>
    Random<Result> makeSeededRandom(const Seed seed, DistributionArgs&&... distributionArgs)
    {
        return Random<Result, Distribution>(seed, std::forward<DistributionArgs>(distributionArgs)...);
    }

    template <typename Result, 
            typename Distribution = typename detail::RandomTraits<Result>::UniformDistribution, 
            typename... DistributionArgs>
    Random<Result> makeAutoSeededRandom(DistributionArgs&&... distributionArgs)
    {
        static std::random_device rd;
        return Random<Result, Distribution>(rd(), std::forward<DistributionArgs>(distributionArgs)...);
    }

    template <typename Result>
    Result random()
    {
        static auto random = makeAutoSeededRandom<Result>();
        return random.next();
    }
}

#endif /* BLINK_COMMON_RANDOM_H */

