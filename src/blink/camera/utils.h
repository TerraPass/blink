#ifndef BLINK_CAMERA_UTILS_H
#define BLINK_CAMERA_UTILS_H

#include "common/utils/math.h"

#include "types.h"

namespace blink
{
    namespace camera
    {
        inline Rect scaleRect(const Rect rect, const U8 scale)
        {
            return {scale*rect.x, scale*rect.y, scale*rect.w, scale*rect.h};
        }

        template <typename Camera>
        inline bool isInsideScreenRect(const Camera& camera, const typename Camera::InputPosition& position, const Rect screenRect)
        {
            const auto worldRect = camera.fromScreenRect(screenRect);
            return isInsideRect(position, worldRect);
        }

        template <typename Camera>
        inline bool isInsideWorldRect(const Camera& camera, const ScreenPosition screenPosition, const Rect worldRect)
        {
            const auto screenRect = camera.toScreenRect(worldRect);
            return isInsideRect(screenPosition, screenRect);
        }
    }
}

#endif /* BLINK_CAMERA_UTILS_H */

