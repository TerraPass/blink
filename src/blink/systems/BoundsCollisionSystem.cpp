#include "BoundsCollisionSystem.h"

#include <cmath>

#include "components/Position.h"
#include "components/Velocity.h"
#include "components/Sprite.h"

namespace blink
{
    namespace systems
    {
        using namespace entityx;
        using namespace blink::components;
        
        using std::floor;
        
        void BoundsCollisionSystem::update(EntityManager& entities, EventManager& /*events*/, TimeDelta dt)
        {
            entities.each<Position, Sprite>(
                [this, dt](Entity entity, Position& position, Sprite& sprite) {
                    auto spriteWidth = sprite.getSprite().getWidth();
                    auto spriteHeight = sprite.getSprite().getHeight();
                    
                    auto screenPosX = floor(position.x);
                    auto screenPosY = floor(position.y);
                    
                    bool outsideX = screenPosX < minX || screenPosX + spriteWidth >= maxX;
                    bool outsideY = screenPosY < minY || screenPosY + spriteHeight >= maxY;

                    if(!outsideX && !outsideY)
                    {
                        return;
                    }
                    
                    // Not all bounded entities have velocity, so query for this component
                    auto velocity = entity.component<Velocity>();
                    // Reverse the velocity components, if they would lead the entity further out of bounds
                    if(velocity)
                    {
                        if(outsideX && ((screenPosX < minX && velocity->x < 0) || velocity->x > 0))
                        {
                            velocity->x *= -1.0;
                        }
                        if(outsideY && ((screenPosY < minY && velocity->y < 0) || velocity->y > 0))
                        {
                            velocity->y *= -1.0;
                        }
                    }
                    
                    // Put the entity back within bounds
                    if(outsideX)
                    {
                        position.x = static_cast<real>(screenPosX < minX ? minX : maxX - spriteWidth);
                    }
                    if(outsideY)
                    {
                        position.y = static_cast<real>(screenPosY < minY ? minY : maxY - spriteHeight);
                    }
                }
            );
        }
    }
}
