#include "SurfaceToTextureDataConverter.h"

#include <cassert>

#include <SDL.h>
#include <SDL_image.h>
#include <sdl2utils/exceptions.h>
#include <sdl2utils/guards.h>

#include "io/exceptions.h"

namespace blink
{
    namespace io
    {
        namespace converters
        {
            using namespace sdl2utils;
            using data::TextureData;

            SurfaceToTextureDataConverter::SurfaceToTextureDataConverter(SDL_Renderer * const prenderer)
            : prenderer(prenderer)
            {
                assert(prenderer != nullptr && "prenderer must not be null");
            }

            TextureData SurfaceToTextureDataConverter::convert(SDL_SurfacePtr psurface) const try
            {
                return TextureData(
                    sdl2utils::guards::ensureNotNull(
                        SDL_CreateTextureFromSurface(prenderer, psurface.get()),
                        "SDL_CreateTextureFromSurface() return value"
                    )
                );
            }
            catch(const SDLErrorException& e)
            {
                throw detail::DataConversionExceptionImpl<SDL_SurfacePtr, TextureData>(e.what());
            }
        }
    }
}
