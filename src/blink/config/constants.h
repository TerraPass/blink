#ifndef BLINK_CONFIG_CONSTANTS_H
#define BLINK_CONFIG_CONSTANTS_H

#include "common/numeric.h"

namespace blink
{
    namespace config
    {
#ifndef NDEBUG
#define BLINK_LOG_LEVEL 5   // TODO: Move to CMake configuration

        constexpr const bool DEBUG_BUILD    = true;
#else
        constexpr const bool DEBUG_BUILD    = false;
#endif
        constexpr const bool RELEASE_BUILD  = !DEBUG_BUILD;

        constexpr const auto WINDOW_TILE    = "Blink";  // TODO: Obviously, this needs to be customizable.

        constexpr const int WINDOW_WIDTH    = 800;
        constexpr const int WINDOW_HEIGHT   = 600;

        constexpr const bool USE_VSYNC      = false;

        constexpr const bool TRAP_MOUSE_CURSOR  = RELEASE_BUILD;

        constexpr const U8 WORLD_RENDER_SCALE   = 2;
        constexpr const U8 UI_RENDER_SCALE      = 1;
    }
}

#endif /* BLINK_CONFIG_CONSTANTS_H */

