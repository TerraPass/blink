#ifndef BLINK_CORE_IGAMEWORLD_H
#define BLINK_CORE_IGAMEWORLD_H

#include "common/numeric.h"

namespace blink
{
    namespace core
    {
        class IGameWorld
        {
        public:
            IGameWorld() = default;
            virtual ~IGameWorld() = default;

            // TODO: Replace deltaTime in the following two signatures with proper UpdateMetadata

            inline void updateWorld(const real deltaTime)
            {
                this->updateWorldImpl(deltaTime);
            }

            inline void renderWorld(const real deltaTime)
            {
                this->renderWorldImpl(deltaTime);
            }

        private:
            virtual void updateWorldImpl(const real deltaTime) = 0;
            virtual void renderWorldImpl(const real deltaTime) = 0;
        };
    }
}

#endif /* BLINK_CORE_IGAMEWORLD_H */

