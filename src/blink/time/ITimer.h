#ifndef BLINK_TIME_ITIMER_H
#define BLINK_TIME_ITIMER_H

#include "common/numeric.h"

namespace blink
{
    namespace time
    {
        class ITimer
        {
        public:
            ITimer() = default;
            virtual ~ITimer() = default;

            inline real getElapsedSeconds() const
            {
                return this->getElapsedSecondsImpl();
            }

            inline bool isPaused() const
            {
                return this->isPausedImpl();
            }

            inline bool resume()
            {
                return this->resumeImpl();
            }

            inline bool pause()
            {
                return this->pauseImpl();
            }

        private:
            virtual real getElapsedSecondsImpl() const = 0;
            virtual bool isPausedImpl() const = 0;

            virtual bool resumeImpl() = 0;
            virtual bool pauseImpl() = 0;
        };
    }
}

#endif /* BLINK_TIME_ITIMER_H */

