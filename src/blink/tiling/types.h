#ifndef BLINK_TILING_TYPES_H
#define BLINK_TILING_TYPES_H

#include "gfx/types.h"

namespace blink
{
    namespace tiling
    {
        /**
         * Tile GID, identifying tile in a particular tilemap across all its tilesets.
         */
        using tilegid = I64;

        using tileunit = I32;
        using TilePosition = Vector2<tileunit>;

        using pixelunit = gfx::unit;
        using PixelPosition = Vector2<pixelunit>;

        // TODO: Should create a custom Rect<Unit> template
        // with Rect<int> specialization implicitly convertible to/from SDL_Rect
        using Rect = gfx::Rect;
    }
}

#endif /* BLINK_TILING_TYPES_H */

