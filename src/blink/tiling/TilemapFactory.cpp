#include "TilemapFactory.h"

#include <memory>
#include <vector>
#include <algorithm>

namespace blink
{
    namespace tiling
    {
        Tilemap TilemapFactory::create(io::data::TilemapData&& data) const
        {
            // Assume ownership over provided data
            auto tilemapData = std::move(data);

            MapTilesContainerPtr pmapTilesContainer{
                mapTilesContainerFactory.create(std::move(tilemapData.tilesets))
            };

            std::vector<TileLayer> tileLayers;
            tileLayers.reserve(data.layers.size());
            std::for_each(
                tilemapData.layers.cbegin(),
                tilemapData.layers.cend(),
                [this, &tileLayers, pmapTilesContainer](const io::data::TilemapData::LayerData& layerData) {
                    if(layerData.type == io::data::TilemapData::LayerData::Type::TILE)
                    {
                        tileLayers.emplace_back(this->tileLayerFactory.create(layerData, pmapTilesContainer));
                    }
                }
            );

            return Tilemap(std::move(tileLayers));
        }
    }
}
