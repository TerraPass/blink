#ifndef BLINK_GFX_SPRITERENDERER_H
#define BLINK_GFX_SPRITERENDERER_H

#include <cassert>

#include "common/numeric.h"
#include "sdl/SDLWindow.h"

#include "types.h"
#include "Sprite.h"

namespace blink
{
    namespace gfx
    {
        template <typename Camera>
        class SpriteRenderer final
        {
            SDL_Renderer* const prenderer;
            const Camera& camera;

        public:
            using Sprite = gfx::Sprite;
            using ScreenPosition = gfx::ScreenPosition;
            using unit = ScreenPosition::unit;

            explicit SpriteRenderer(SDL_Renderer* const prenderer, const Camera& camera)
            : prenderer(prenderer), camera(camera)
            {
                
            }

            SpriteRenderer(const SpriteRenderer&) = default;
            SpriteRenderer(SpriteRenderer&&) = default;
            
            SpriteRenderer& operator=(const SpriteRenderer&) = delete;
            SpriteRenderer& operator=(SpriteRenderer&&) = delete;
            
            virtual ~SpriteRenderer() = default;

            void renderSprite(const Sprite& sprite, const typename Camera::InputPosition cameraInputPosition) const;
        };
    }
}

#ifndef BLINK_COMPILE_TEMPLATES
#include "SpriteRenderer.cxx"
#endif

#endif /* BLINK_GFX_SPRITERENDERER_H */

