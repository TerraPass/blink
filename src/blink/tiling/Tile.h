#ifndef BLINK_GFX_TILE_H
#define BLINK_GFX_TILE_H

#include <boost/optional.hpp>

#include <SDL.h>

#include "gfx/Sprite.h"

#include "types.h"

namespace blink
{
    namespace tiling
    {
        class Tile final
        {
            gfx::Sprite sprite;

            // TODO: Additional tile data, if needed.

        public:
            explicit Tile(const gfx::Sprite& sprite)
            : sprite(sprite)
            {
                
            }

            explicit Tile(gfx::Sprite&& sprite)
            : sprite(std::move(sprite))
            {
                
            }

            inline const gfx::Sprite& getSprite() const
            {
                return sprite;
            }

            inline pixelunit getWidth() const
            {
                return sprite.getWidth();
            }

            inline pixelunit getHeight() const
            {
                return sprite.getHeight();
            }
        };

        using MaybeTile = boost::optional<Tile>;

        inline Tile makeTile(SDL_Texture* const ptilesetTexture, const Rect tileRect)
        {
            return Tile(gfx::Sprite(tileRect, ptilesetTexture));
        }
    }
}

#endif /* BLINK_GFX_TILE_H */
