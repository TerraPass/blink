#ifndef BLINK_SDL_SDLWINDOW_H
#define BLINK_SDL_SDLWINDOW_H

#include <string>

#include <SDL.h>

#include <sdl2utils/pointers.h>

namespace blink
{
    namespace sdl
    {
        using namespace sdl2utils;
        
        class SDLWindow final
        {
            static constexpr const Uint32 DEFAULT_WINDOW_FLAGS    = 0;
            static constexpr const Uint32 DEFAULT_RENDERER_FLAGS  = 0;

            static constexpr const int WINDOW_X = SDL_WINDOWPOS_UNDEFINED;
            static constexpr const int WINDOW_Y = SDL_WINDOWPOS_UNDEFINED;

            const SDL_WindowPtr pwindow;
            const SDL_RendererPtr prenderer;

        public:
            explicit SDLWindow(
                const std::string& title,
                const int width,
                const int height,
                const Uint32 sdlWindowFlags = DEFAULT_WINDOW_FLAGS,
                const Uint32 sdlRendererFlags = DEFAULT_RENDERER_FLAGS
            );

            inline SDL_Window* getWindowPtr() const
            {
                return this->pwindow.get();
            }

            inline SDL_Renderer* getRendererPtr() const
            {
                return this->prenderer.get();
            }

            void trapMouseCursor(const bool value);

            int getWidth() const;
            int getHeight() const;
            
            SDLWindow(const SDLWindow&)     = delete;
            SDLWindow(SDLWindow&&)          = delete;
            
            SDLWindow& operator=(const SDLWindow&)  = delete;
            SDLWindow& operator=(SDLWindow&&)       = delete;
        };
    }
}

#endif /* BLINK_SDL_SDLWINDOW_H */

