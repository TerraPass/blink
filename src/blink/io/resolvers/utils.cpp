#include "utils.h"

#include "common/platform.h"
#include "debug/logging.h"

namespace blink
{
    namespace io
    {
        namespace resolvers
        {
            namespace
            {
#ifdef BLINK_MACOS
                std::string getExecutablePath()
                {
                    static const size_t bufferSize = 1024;

                    std::string buffer(bufferSize, '\0');

                    const auto pid = getpid();
                    const auto ret = proc_pidpath(pid, &buffer[0], bufferSize);
                    if(ret <= 0)
                    {
                        buffer.resize(0);
                        BLINK_LOGF_E("Failed to determine executable path for PID %1%: %2%", pid % strerror(errno));
                    }
                    else
                    {
                        buffer.resize(ret);
                        BLINK_LOGF_I("Determined executable path for PID %1% as %2%", pid % buffer);
                    }

                    return buffer;
                }

                std::string getExecutableDirectoryImpl()
                {
                    std::string execDir = getExecutablePath();
                    if(!execDir.empty())
                    {
                        execDir.erase(std::find(execDir.rbegin(), execDir.rend(), '/').base(), execDir.end());
                        BLINK_LOGF_I("Executable directory: %1%", execDir);
                    }
                    else
                    {
                        BLINK_LOG_I("Will assume current working directory to be the executable directory");
                    }
                    return execDir;
                }
#else
                std::string getExecutableDirectoryImpl()
                {
                    return ".";
                }
#endif
            }

            std::string getExecutableDirectory()
            {
                static const std::string result = getExecutableDirectoryImpl();
                return result;
            }
        }
    }
}
