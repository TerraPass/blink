#ifndef BLINK_IO_CONVERTERS_JSONTOTILEMAPOBJECTDATACONVERTER_H
#define BLINK_IO_CONVERTERS_JSONTOTILEMAPOBJECTDATACONVERTER_H

#include <stdexcept>

#include <nlohmann_json.hpp>

#include "io/data/TilemapData.h"

namespace blink
{
    namespace io
    {
        namespace converters
        {
            class JsonToTilemapObjectDataConverter final
            {
            public:
                using ObjectData = data::TilemapData::LayerData::ObjectData;

                ObjectData convert(const nlohmann::json& objectJson) const;
            };
        }
    }
}

#endif /* BLINK_IO_CONVERTERS_JSONTOTILEMAPOBJECTDATACONVERTER_H */

