#include "debug/logging.h"
#include "common/exceptions.h"
#include "Engine.h"

using namespace blink;

enum MainError
{
    NONE        = 0,
    UNKNOWN     = -1,
    INIT_FAILED = -2
};

void logReturnCode(MainError returnCode) noexcept;

int main(int /*argc*/, char** /*argv*/)
{
    MainError returnCode = MainError::NONE;

    // TODO: Implement engine bootstrap, which would encapsulate
    // reading engine config files and constructing the engine instance.

    try
    {
        Engine engine;

        engine.runGameLoop();
    }
    catch(const Engine::InitializationFailedException& e)
    {
        BLINK_LOG_F(e.what());
        returnCode = MainError::INIT_FAILED;
    }
    catch(const common::BlinkException& e)
    {
        BLINK_LOG_F("Unhandled engine exception: " << e.what());
        returnCode = MainError::UNKNOWN;
    }
    catch(const std::exception& e)
    {
        BLINK_LOG_F("Unexpected exception: " << e.what());
        returnCode = MainError::UNKNOWN;
    }
    catch(...)
    {
        BLINK_LOG_F("Unknown fatal error");
        returnCode = MainError::UNKNOWN;
    }

    logReturnCode(returnCode);
    
    return returnCode;
}

void logReturnCode(MainError returnCode) noexcept
{
    if(returnCode == MainError::NONE)
    {
        BLINK_LOG_I("Exiting normally (return code " << returnCode << ")");
    }
    else
    {
        BLINK_LOG_F("Abnormal termination (return code " << returnCode << ")");
    }
}
