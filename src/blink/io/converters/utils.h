#ifndef BLINK_IO_CONVERTERS_UTILS_H
#define BLINK_IO_CONVERTERS_UTILS_H

#include <algorithm>
#include <functional>
#include <boost/any.hpp>

#include <nlohmann_json.hpp>

#include "io/data/TilemapData.h"

namespace blink
{
    namespace io
    {
        namespace converters
        {
            /**
             * Fills targetContainer with values, converted from the elements
             * of the JSON array, provided as sourceJson parameter,
             * converted using the given converter.
             * 
             * @param targetContainer Target container, into which to insert the converted values.
             * @param sourceJson JSON array, containing objects or values to be converted.
             * @param converter Converter to use; must provide convert().
             */
            template <typename TargetContainer, typename Converter>
            inline void convertJsonArray(
                TargetContainer& targetContainer,
                const nlohmann::json& sourceJson,
                const Converter& converter
            )
            {
                std::transform(
                    sourceJson.cbegin(),
                    sourceJson.cend(),
                    std::back_inserter(targetContainer),
                    std::bind(&Converter::convert, &converter, std::placeholders::_1)
                );
            }

            /**
             * Fills the provided container with properties, obtained from a Tiled JSON object,
             * providing "properties" and "propertytypes" keys.
             * 
             * @param targetContainer Container, into which to insert the properties.
             * @param outerJson JSON object, containing "properties" and "propertytypes" objects.
             * @param required If true, the absence of "properties" in outerJson will trigger
             * an std::invalid_argument exception, otherwise the call will have no effect.
             * 
             * @throw std::invalid_argument if required is true but outerJson contains no "properties".
             * @throw std::out_of_range if "properties" is present but "propertytypes" is absent in outerJson.
             */
            void readTiledProperties(
                data::TilemapData::Properties& targetContainer,
                const nlohmann::json& outerJson,
                const bool required
            );
        }
    }
}

#endif /* BLINK_IO_CONVERTERS_UTILS_H */

