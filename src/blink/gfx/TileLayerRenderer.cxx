#ifdef BLINK_COMPILE_TEMPLATES
#include "TileLayerRenderer.h"

#include "camera/TopDownCamera.h"
#endif

#include <cassert>

#include <sdl2utils/raii.h>
#include <sdl2utils/guards.h>

namespace blink
{
    namespace gfx
    {
        template<typename Camera>
        void TileLayerRenderer<Camera>::renderToCache(const tiling::TileLayer& tileLayer, const typename Cache::iterator cacheHit)
        {
            using namespace sdl2utils::guards::abbr;
            using sdl2utils::guards::ensureNotNull;

            auto entryIt = cacheHit;

            // If there is no cache entry for this layer yet, create one
            if(cacheHit == cache.end())
            {
                const auto emplacement = cache.emplace(
                    &tileLayer,
                    CacheEntry{
                        sdl2utils::SDL_TexturePtr(nullptr),
                        static_cast<U64>(-1),   // Placeholder invalid version
                        static_cast<U8>(0)      // Placeholder invalid camera scale
                    }
                );
                assert(emplacement.second && "emplacement must have succeeded (we ensured that the key does not yet exist)");
                entryIt = emplacement.first;
            }

            // FIXME: Also handle tilelayer size changes (see comment in condition)
            if(camera.getScale() != entryIt->second.cameraScale /*|| tileLayer.getDimensions() != entryIt->second.dimensions*/)
            {
                const int textureWidth = static_cast<int>(tileLayer.getTileWidth() * tileLayer.getWidth() * camera.getScale());
                const int textureHeight = static_cast<int>(tileLayer.getTileHeight() * tileLayer.getHeight() * camera.getScale());
                entryIt->second.ptexture.reset(
                    ensureNotNull(SDL_CreateTexture(
                        prenderer,
                        CACHE_PIXEL_FORMAT,
                        SDL_TEXTUREACCESS_TARGET,
                        textureWidth,
                        textureHeight
                    ), "SDL_CreateTexture() result when allocating a cache texture in TileLayerRenderer<?>::renderToCache()")
                );
                entryIt->second.cameraScale = camera.getScale();

                // Set appropriate blending mode for the texture
                z(SDL_SetTextureBlendMode(entryIt->second.ptexture.get(), SDL_BLENDMODE_BLEND));
            }

            // Setup renderer to render the tilelayer onto the cache texture
            sdl2utils::raii::ScopedRenderTarget srt(prenderer, entryIt->second.ptexture.get());

            // Render the entire tilelayer into cache
            fullRenderer.renderLayer(tileLayer, camera.getScale());
            // Update the version of cache entry accordingly
            entryIt->second.version = tileLayer.getDataVersion();
        }

        template<typename Camera>
        void TileLayerRenderer<Camera>::renderFromCache(const tiling::TileLayer& tileLayer, const typename Camera::InputPosition offset) const
        {
            using namespace sdl2utils::guards::abbr;

            // No point in rendering, if the layer is not visible or completely transparent
            if(!tileLayer.isVisible() || tileLayer.getOpacity() < MIN_RENDER_OPACITY)
            {
                return;
            }

            const typename Cache::const_iterator entryIt = cache.find(&tileLayer);
            assert(entryIt != cache.cend() && "renderFromCache() assumes that tileLayer rendering is cached");

            // TODO: Take layer offset (not just passed in (tilemap) offset) into account
            const ScreenPosition screenPosition = camera.toScreenPosition(offset /*+ tileLayer.offset*/);

            Rect dstRect;
            dstRect.x = screenPosition.x;
            dstRect.y = screenPosition.y;
            z(SDL_QueryTexture(entryIt->second.ptexture.get(), nullptr, nullptr, &(dstRect.w), &(dstRect.h)));

            // Take opacity into account
            z(SDL_SetTextureAlphaMod(entryIt->second.ptexture.get(), tileLayer.getOpacityByte()));

            z(SDL_RenderCopy(
                prenderer,
                entryIt->second.ptexture.get(),
                nullptr,
                &dstRect
            ));
        }

        template<typename Camera>
        void TileLayerRenderer<Camera>::renderLayer(const tiling::TileLayer& tileLayer, const typename Camera::InputPosition offset)
        {
            const typename Cache::iterator cacheHit = cache.find(&tileLayer);
            if(cacheHit == cache.end() || cacheHit->second.version != tileLayer.getDataVersion()
                || cacheHit->second.cameraScale != camera.getScale())
            {
                renderToCache(tileLayer, cacheHit);
            }

            renderFromCache(tileLayer, offset);
        }

#ifdef BLINK_COMPILE_TEMPLATES
        template class TileLayerRenderer<camera::TopDownCamera>;
#endif
    }
}
