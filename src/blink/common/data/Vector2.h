#ifndef BLINK_COMMON_DATA_VECTOR2_H
#define BLINK_COMMON_DATA_VECTOR2_H

#include <type_traits>
#include <limits>
#include <cmath>

#include "common/numeric.h"
#include "common/utils/math.h"

namespace blink
{
    template <typename Unit>
    struct Vector2 final
    {
        static_assert(std::is_arithmetic<Unit>::value, "Unit must be an arithmetic type in Vector2<Unit>");

        using unit = Unit;
        
        Unit x;
        Unit y;

        constexpr Vector2(const Unit x, const Unit y) noexcept
        : x(x), y(y)
        {
            
        }

        constexpr Vector2() noexcept
        : x(0), y(0)
        {
            
        }

        Vector2(const Vector2&) = default;
        Vector2(Vector2&&) = default;

        Vector2& operator=(const Vector2&) = default;
        Vector2& operator=(Vector2&&) = default;

        ~Vector2() = default;

        inline Vector2& operator*=(const Unit scalar)
        {
            this->x *= scalar;
            this->y *= scalar;
            return *this;
        }

        inline Vector2& operator/=(const Unit scalar)
        {
            this->x /= scalar;
            this->y /= scalar;
            return *this;
        }

        inline Vector2& operator+=(const Vector2& rhs)
        {
            this->x += rhs.x;
            this->y += rhs.y;
            return *this;
        }

        inline Vector2& operator-=(const Vector2& rhs)
        {
            this->x -= rhs.x;
            this->y -= rhs.y;
            return *this;
        }
    };

    template <typename Unit>
    inline bool operator==(const Vector2<Unit>& lhs, const Vector2<Unit>& rhs)
    {
        return lhs.x == rhs.x && lhs.y == rhs.y;
    }

    template <typename Unit>
    inline bool operator!=(const Vector2<Unit>& lhs, const Vector2<Unit>& rhs)
    {
        return !(lhs == rhs);
    }

    template <typename Unit>
    inline Vector2<Unit> operator*(const Unit scalar, Vector2<Unit> vector)
    {
        return (vector *= scalar);
    }

    template <typename Unit, typename Scalar>
    inline std::enable_if_t<!std::is_same<Unit, Scalar>::value, Vector2<Unit>> operator*(const Scalar scalar, Vector2<Unit> vector)
    {
        return (static_cast<Unit>(scalar) * vector);
    }

    template <typename Unit>
    inline Vector2<Unit> operator*(const Vector2<Unit>& vector, const Unit scalar)
    {
        return scalar*vector;
    }

    template <typename Unit, typename Scalar>
    inline std::enable_if_t<!std::is_same<Unit, Scalar>::value, Vector2<Unit>> operator*(Vector2<Unit> vector, const Scalar scalar)
    {
        return (vector * static_cast<Unit>(scalar));
    }

    template <typename Unit>
    inline Vector2<Unit> operator/(Vector2<Unit> vector, const Unit scalar)
    {
        return (vector /= scalar);
    }

    template <typename Unit, typename Scalar>
    inline std::enable_if_t<!std::is_same<Unit, Scalar>::value, Vector2<Unit>> operator/(Vector2<Unit> vector, const Scalar scalar)
    {
        return (vector / static_cast<Unit>(scalar));
    }

    template <typename Unit>
    inline Vector2<Unit> operator+(Vector2<Unit> lhs, const Vector2<Unit>& rhs)
    {
        return (lhs += rhs);
    }

    template <typename Unit>
    inline Vector2<Unit> operator-(Vector2<Unit> lhs, const Vector2<Unit>& rhs)
    {
        return (lhs -= rhs);
    }

    template <typename DstUnit, typename SrcUnit>
    inline Vector2<DstUnit> staticVector2Cast(const Vector2<SrcUnit>& src)
    {
        return Vector2<DstUnit>(
            static_cast<DstUnit>(src.x),
            static_cast<DstUnit>(src.y)
        );
    }

    using RealVector2 = Vector2<real>;

    inline bool almostEqual(const RealVector2& lhs, const RealVector2& rhs, const real epsilon)
    {
        return almostEqual(lhs.x, rhs.x, epsilon) && almostEqual(lhs.y, rhs.y, epsilon);
    }

    inline bool almostEqual(const RealVector2& lhs, const RealVector2& rhs)
    {
        return almostEqual(lhs.x, rhs.x) && almostEqual(lhs.y, rhs.y);
    }

    inline real sqrMagnitude(const RealVector2& v)
    {
        return v.x*v.x + v.y*v.y;
    }

    inline real magnitude(const RealVector2& v)
    {
        return std::sqrt(sqrMagnitude(v));
    }

    inline real sqrDistance(const RealVector2& v0, const RealVector2& v1)
    {
        return sqrMagnitude(v1 - v0);
    }

    inline real distance(const RealVector2& v0, const RealVector2& v1)
    {
        return std::sqrt(sqrDistance(v0, v1));
    }

    inline real dotProduct(const RealVector2& v0, const RealVector2& v1)
    {
        return v0.x*v1.x + v0.y*v1.y;
    }

    /**
     * Returns the z coordinate of the 3d vector, obtained via cross product of v0 and v1
     * with 0 used as z coordinate for both of them.
     * 
     * @param v0 First vector.
     * @param v1 Second vector.
     * @return The z of the cross product between v0 and v1.
     */
    inline real crossProductZ(const RealVector2& v0, const RealVector2& v1)
    {
        return (v0.x*v1.y) - (v0.y*v1.x);
    }
}

#endif /* BLINK_COMMON_DATA_VECTOR2_H */
