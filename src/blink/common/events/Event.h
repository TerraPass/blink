#ifndef BLINK_COMMON_EVENT_H
#define BLINK_COMMON_EVENT_H

#include <list>
#include <functional>

namespace blink
{
    namespace common
    {
        // A functor, which may be called to unsubscribe the subscribed listener.
        // May be invoked multiple times but only the first invokation will have any effect.
        using EventUnsubscriptionCommand = std::function<bool()>;

        /**
         * A RAII wrapper for EventUnsubscriptionCommand.
         * Automatically calls the unsubscription command when destroyed.
         */
        class ScopedSubscription final
        {
            EventUnsubscriptionCommand unsubCommand;
            bool cancelled;

        public:
            explicit ScopedSubscription(EventUnsubscriptionCommand unsubCommand)
            : unsubCommand(unsubCommand), cancelled(false)
            {

            }

            ScopedSubscription(const ScopedSubscription&) = delete;

            ScopedSubscription(ScopedSubscription&& other)
            : unsubCommand(other.unsubCommand), cancelled(other.cancelled)
            {
                other.cancelled = true;
            }

            ScopedSubscription& operator=(const ScopedSubscription&) = delete;
            ScopedSubscription& operator=(ScopedSubscription&&) = delete;

            inline bool isCancelled() const
            {
                return cancelled;
            }

            inline bool cancel()
            {
                cancelled = true;
                return unsubCommand();
            }

            ~ScopedSubscription()
            {
                if(!cancelled)
                {
                    unsubCommand();
                }
            }
        };

        namespace detail
        {
            template <typename Subject, typename EventArgs>
            struct EventTraits
            {
                using Listener = std::function<void(Subject&, const EventArgs&)>;

                template <typename MemberFunc, typename This>
                static inline auto bindMemFn(MemberFunc memFn, This _this)
                {
                    using namespace std::placeholders;
                    return std::bind(memFn, _this, _1, _2);
                }
            };
            
            template <typename Subject>
            struct EventTraits<Subject, void>
            {
                using Listener = std::function<void(Subject&)>;

                template <typename MemberFunc, typename This>
                static inline auto bindMemFn(MemberFunc memFn, This _this)
                {
                    using namespace std::placeholders;
                    return std::bind(memFn, _this, _1);
                }
            };

            template <typename Subject, typename EventArgs, typename Derived>
            struct BaseEvent
            {
                //static constexpr const size_t LISTENER_CONTAINER_INITIAL_CAPACITY = 10;

                using Listener = typename EventTraits<Subject, EventArgs>::Listener;
                // list is used because unsubscription mechanism relies on iterators,
                // so it's necessary for them to stay valid when collection gets modified.
                // TODO: Optimize for data cache performance by using a vector of wrapped listeners instead.
                // (Listener wrapper would contain a bool, indicating whether the subscription
                // is still valid. (This might have an adverse effect on instruction cache performance, though.))
                using ListenerContainer = std::list<Listener>;

                Subject& subject;
                ListenerContainer listeners;

                inline explicit BaseEvent(Subject& subject)
                    : subject(subject), listeners()
                {
    //                if(LISTENER_CONTAINER_INITIAL_CAPACITY > 0)
    //                {
    //                    listeners.reserve(LISTENER_CONTAINER_INITIAL_CAPACITY);
    //                }
                }

                // Move-from-derived constructor
                inline explicit BaseEvent(Derived&& otherDerived)
                    : subject(otherDerived.subject), listeners(std::move(otherDerived.listeners))
                {

                }

                inline EventUnsubscriptionCommand subscribe(Listener listener)
                {
                    this->listeners.push_back(listener);

                    // Iterator, pointing to the newly inserted listener
                    auto lit = --(this->listeners.end());

                    // Return lambda, allowing to cancel the subscription
                    return [this, lit]()
                    {
                        static bool alreadyInvoked = false;

                        if(!alreadyInvoked)
                        {
                            this->listeners.erase(lit);
                            alreadyInvoked = true;
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    };
                }

                template <typename MemberFunc, typename This>
                inline EventUnsubscriptionCommand subscribe(MemberFunc memFn, This _this)
                {
                    return subscribe(EventTraits<Subject, EventArgs>::bindMemFn(memFn, _this));
                }
            };
        }
        
        template <typename Subject, typename EventArgs>
        class Event final : private detail::BaseEvent<Subject, EventArgs, Event<Subject, EventArgs>>
        {   
        public:
            using detail::BaseEvent<Subject, EventArgs, Event>::subscribe;
            
        private:    
            inline explicit Event(Subject& subject)
                : detail::BaseEvent<Subject, EventArgs, Event>(subject)
            {

            }
            
            Event(const Event&) = delete;
            Event& operator=(const Event&) = delete;

            Event(Event&& other)
                : detail::BaseEvent<Subject, EventArgs, Event>(other)
            {
                
            }

            Event& operator=(Event&&) = delete;

            inline void operator()(const EventArgs& args)
            {
                auto lit = this->listeners.begin();
                while(lit != this->listeners.end())
                {
                    auto& currentListener = (*lit);
                    
                    // Move iterator to next item, so that it doesn't
                    // get invalidated if the invoked listener decides to unsubscribe.
                    lit++;
                    
                    currentListener(this->subject, args);
                }
            }

            friend Subject;
        };

        template <typename Subject>
        class Event<Subject, void> final : private detail::BaseEvent<Subject, void, Event<Subject, void>>
        {
        public:
            using detail::BaseEvent<Subject, void, Event>::subscribe;
            
        private:    
            inline explicit Event(Subject& subject)
                : detail::BaseEvent<Subject, void, Event>(subject)
            {

            }
            
            Event(const Event&) = delete;
            Event& operator=(const Event&) = delete;

            Event(Event&& other)
                : detail::BaseEvent<Subject, void, Event>(other)
            {
                
            }
            
            Event& operator=(Event&&) = delete;

            inline void operator()()
            {
                auto lit = this->listeners.begin();
                while(lit != this->listeners.end())
                {
                    auto& currentListener = (*lit);
                    
                    // Move iterator to next item, so that it doesn't
                    // get invalidated if the invoked listener decides to unsubscribe.
                    lit++;
                    
                    currentListener(this->subject);
                }
            }

            friend Subject;
        };
    }
}

#endif /* BLINK_COMMON_EVENT_H */

