#ifndef BLINK_SYSTEMS_RENDERING_SORTINGRENDER_H
#define BLINK_SYSTEMS_RENDERING_SORTINGRENDER_H

#include <functional>

namespace blink
{
    namespace systems
    {
        namespace rendering
        {
            /**
             * Passes given renderables to the renderer in the order they are given,
             * while sorting them in ascending order of priority.
             * (I.e. renderer does not necessarily get renderables in the correct order,
             * but it will on next call to sortingRender(), if none of the renderables
             * have changed priorities.)
             * 
             * @param renderer Renderer to pass renderables to for rendering.
             * @param renderables Rendered and sorted container of renderables.
             * @param lessPriority A callable, implementing less-than relationship between two renderables.
             */
            template <typename Renderer,
                typename RenderablesContainer,
                typename PriorityComparer = std::less<typename RenderablesContainer::value_type>>
            void sortingRender(
                const Renderer& renderer,
                RenderablesContainer& renderables, 
                PriorityComparer lessPriority = std::less<typename RenderablesContainer::value_type>()
            );

            // Policy struct for use by RenderingSystem
            struct SortingRender final
            {
                template <typename Renderer,
                    typename RenderablesContainer,
                    typename PriorityComparer = std::less<typename RenderablesContainer::value_type>>
                inline void operator()(
                    const Renderer& renderer,
                    RenderablesContainer& renderables, 
                    PriorityComparer lessPriority = std::less<typename RenderablesContainer::value_type>()
                ) const
                {
                    sortingRender(renderer, renderables, lessPriority);
                }
            };
        }
    }
}

#ifndef BLINK_COMPILE_TEMPLATES
#include "sortingRender.cxx"
#endif

#endif /* BLINK_SYSTEMS_RENDERING_SORTINGRENDER_H */
