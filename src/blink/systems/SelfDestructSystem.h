#ifndef BLINK_SYSTEMS_SELFDESTRUCTSYSTEM_H
#define BLINK_SYSTEMS_SELFDESTRUCTSYSTEM_H

#include <entityx/entityx.h>

#include "core/ITimeKeeper.h"

namespace blink
{
    namespace systems
    {
        class SelfDestructSystem final : public entityx::System<SelfDestructSystem>
        {
            const core::ITimeKeeper& timeKeeper;

        public:
            explicit SelfDestructSystem(const core::ITimeKeeper& timeKeeper)
            : timeKeeper(timeKeeper)
            {
                
            }

            virtual void update(entityx::EntityManager& entities, entityx::EventManager& events, entityx::TimeDelta dt) override;
        };
    }
}

#endif /* BLINK_SYSTEMS_SELFDESTRUCTSYSTEM_H */

