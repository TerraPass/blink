#ifndef BLINK_SYSTEMS_LINERENDERINGSYSTEM_H
#define BLINK_SYSTEMS_LINERENDERINGSYSTEM_H

#include <SDL.h>

#include <entityx/entityx.h>

#include "camera/TopDownCamera.h"
#include "gfx/LineRenderer.h"

namespace blink
{
    namespace systems
    {
        class LineRenderingSystem final : public entityx::System<LineRenderingSystem>
        {
            const gfx::TopDownLineRenderer lineRenderer;

        public:
            LineRenderingSystem(SDL_Renderer* const prenderer, const camera::TopDownCamera& camera)
            : lineRenderer(prenderer, camera)
            {
                
            }

            virtual void update(entityx::EntityManager& entities, entityx::EventManager& events, entityx::TimeDelta dt) override;
        };
    }
}

#endif /* BLINK_SYSTEMS_LINERENDERINGSYSTEM_H */

