#include "JsonToAnimationDataConverter.h"

#include <boost/algorithm/string.hpp>

#include "debug/logging.h"
#include "io/exceptions.h"

namespace blink
{
    namespace io
    {
        namespace converters
        {
            using nlohmann::json;
            using data::AnimationData;

            const std::string JsonToAnimationDataCoverter::EXPECTED_FORMAT    = "RGBA8888";
            const std::string JsonToAnimationDataCoverter::EXPECTED_SCALE     = "1";

            AnimationData JsonToAnimationDataCoverter::convert(const json& dataJson) const try
            {
                AnimationData data;

                // Load frames
                for(const auto& frameJson : dataJson.at("frames"))
                {
                    const auto& frameFrameJson = frameJson.at("frame");
                    data.frames.emplace_back(
                        AnimationData::FrameData::Rect{
                            frameFrameJson.at("x"),
                            frameFrameJson.at("y"),
                            frameFrameJson.at("w"),
                            frameFrameJson.at("h")
                        },
                        frameJson.at("duration")
                    );
                }
                
                // Load metadata
                const auto& metaJson = dataJson.at("meta");
                data.meta.image = metaJson.at("image");
                data.meta.format = boost::to_upper_copy<std::string>(metaJson.at("format"));

                // Log an error in case of unexpected format
                BLINK_LOG_E_IF(data.meta.format != EXPECTED_FORMAT, "Unexpected meta.format \"" << data.meta.format << '\"');
                // Log an error in case of unexpected scale
#if BLINK_LOG_LEVEL >= BLINK_LOG_ERROR
                const auto sit = dataJson.find("scale");
                if(sit != dataJson.end() && (*sit) != EXPECTED_SCALE)
                {
                    BLINK_LOG_E("Unexpected meta.scale \"" << *sit << '\"');
                }
#endif
                
                const auto& metaSizeJson = metaJson.at("size");
                data.meta.sizeW = metaSizeJson.at("w");
                data.meta.sizeH = metaSizeJson.at("h");
                
                // Load frame tags
                for(const auto& frameTagJson : metaJson.at("frameTags"))
                {
                    data.meta.frameTags.emplace_back(
                        frameTagJson.at("name"),
                        frameTagJson.at("from"),
                        frameTagJson.at("to"),
                        frameTagJson.at("direction")
                    );
                }

                return data;
            }
            catch(const std::out_of_range& e)
            {
                throw detail::DataConversionExceptionImpl<json, AnimationData>(e.what());
            }
            catch(const std::domain_error& e)
            {
                throw detail::DataConversionExceptionImpl<json, AnimationData>(e.what());
            }
        }
    }
}
