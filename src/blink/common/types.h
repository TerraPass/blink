#ifndef BLINK_COMMON_TYPES_H
#define BLINK_COMMON_TYPES_H

#include "numeric.h"
#include "data/Vector2.h"
#include "data/Rect.h"

namespace blink
{
    using WorldPosition = RealVector2;  // TODO: Move to a more specialized namespace
    using WorldRect = Rect<real>;       // ditto
}

#endif /* BLINK_COMMON_TYPES_H */

