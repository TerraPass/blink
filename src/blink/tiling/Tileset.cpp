#include "Tileset.h"

#include <algorithm>
#include <stdexcept>

namespace blink
{
    namespace tiling
    {
        Tileset::Tileset(sdl2utils::SDL_TexturePtr ptexture, std::vector<Rect>&& tiles, const pixelunit tileWidth, const pixelunit tileHeight)
        : ptexture(std::move(ptexture)), tileRects(std::move(tiles)),
            tileWidth(tileWidth), tileHeight(tileHeight)
        {
            if(!std::all_of(
                tileRects.cbegin(),
                tileRects.cend(),
                [this](const Rect& rect) {
                    return rect.w == this->tileWidth && rect.h == this->tileHeight;
                }
            ))
            {
                throw std::invalid_argument("All tiles in the tileset must have the specified dimensions");
            }
        }
    }
}
