#ifndef BLINK_COMMON_DATA_RECT_H
#define BLINK_COMMON_DATA_RECT_H

#include <type_traits>

#include <SDL.h>

#include "Vector2.h"

namespace blink
{
    template <typename Unit>
    struct Rect final
    {
        static_assert(std::is_arithmetic<Unit>::value, "Unit must be an arithmetic type in Rect<Unit>");

        Unit x;
        Unit y;
        Unit w;
        Unit h;

        Rect()
        : x(0), y(0), w(0), h(0)
        {

        }

        Rect(const Unit x, const Unit y, const Unit w, const Unit h)
        : x(x), y(y), w(w), h(h)
        {

        }

        explicit Rect(const Vector2<Unit> dimensions, const Vector2<Unit> offset = {})
        : x(offset.x), y(offset.y), w(dimensions.x), h(dimensions.y)
        {

        }

        /**
         * This implicit constructor is only available for Rect<int> (int is the type of SDL_Rect's fields).
         * 
         * @param sdlRect SDL_Rect.
         * @param int Dummy parameter for SFINAE.
         */
        template <typename U = Unit>
        /*implicit*/ Rect(const SDL_Rect sdlRect, const std::enable_if_t<std::is_same<U, decltype(SDL_Rect::x)>::value, int>* const = nullptr)
        : x(sdlRect.x), y(sdlRect.y), w(sdlRect.w), h(sdlRect.h)
        {

        }

        /**
         * This implicit user defined conversion operator is only available for Rect<int> (int is the type of SDL_Rect's fields).
         * 
         * @return SDL_Rect.
         */
        template <typename U = Unit>
        /*implicit*/ operator std::enable_if_t<std::is_same<U, decltype(SDL_Rect::x)>::value, SDL_Rect>() const
        {
            return {x, y, w, h};
        }

        inline Vector2<Unit> getDimensions() const
        {
            return Vector2<Unit>(w, h);
        }

        inline void setDimensions(const Vector2<Unit> dimensions)
        {
            w = dimensions.x;
            h = dimensions.y;
        }

        inline Vector2<Unit> getOffset() const
        {
            return Vector2<Unit>(x, y);
        }

        inline void setOffset(const Vector2<Unit> offset)
        {
            x = offset.x;
            y = offset.y;
        }
    };
}

#endif /* BLINK_COMMON_DATA_RECT_H */
