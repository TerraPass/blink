#include "GameWorld.h"

#include <SDL.h>
#include <sdl2utils/raii.h>

#include "systems/MovementSystem.h"
#include "systems/BoundsCollisionSystem.h"
#include "systems/SelfDestructSystem.h"
#include "systems/RenderingSystem.h"
#include "systems/LineRenderingSystem.h"
#include "systems/AnimationSystem.h"
#include "systems/SparkDestructionSystem.h"
#include "components/Position.h"
#include "components/Sprite.h"
#include "components/Velocity.h"
#include "gfx/Sprite.h"
#include "gfx/Animation.h"
#include "gfx/SpriteRenderer.h"
#include "gfx/TilemapRenderer.h"
#include "common/random.h"
#include "common/types.h"

namespace blink
{
    namespace core
    {
        using namespace sdl2utils;
        using namespace sdl2utils::raii;
        using namespace blink::systems;
        using namespace blink::components;
        
        using std::rand;
        using std::srand;
        
        using RenderingSystem = systems::SortingRenderingSystem<gfx::SpriteRenderer<camera::TopDownCamera>>;

        static inline void generateWorld(
            const int worldWidth,
            const int worldHeight,
            entities::SparkFactory sparkFactory,
            const size_t numberOfEntities,
            const int spriteWidth,
            const int spriteHeight,
            const real maxVelocityCoord
        )
        {
            static auto randWidth = makeAutoSeededRandom<int>(0, worldWidth - spriteWidth - 1);
            static auto randHeight = makeAutoSeededRandom<int>(0, worldHeight - spriteHeight - 1);

            for(size_t i = 0; i < numberOfEntities; i++)
            {
                // Create a spark with random position and velocity
                sparkFactory.create(
                    static_cast<real>(randWidth.next()),            // posX
                    static_cast<real>(randHeight.next()),           // posY
                    (-1.0 + 2*random<real>()) * maxVelocityCoord,                   // velX
                    (-1.0 + 2*random<real>()) * maxVelocityCoord,                   // velY
                    random<real>()*RAND_MAX                                         // initialAnimTime
                );
            }
        }
        
        GameWorld::GameWorld(const sdl::SDLWindow& window, const ContentManager& contentManager, core::ITimeKeeper& timeKeeper, input::InputSystem& inputSystem)
        : window(window), contentManager(contentManager), timeKeeper(timeKeeper),
            cameraSystem(window.getWidth(), window.getHeight(), inputSystem, config::WORLD_RENDER_SCALE, config::UI_RENDER_SCALE),
            ex(), sparkFactory(contentManager, ex.entities, timeKeeper),
            fpsCounter(timeKeeper, window.getRendererPtr(), cameraSystem.getUiCamera(), {5,5}, contentManager.getFpsCounterFont())
        {
            const int worldWidth = window.getWidth()/config::WORLD_RENDER_SCALE;
            const int worldHeight = window.getHeight()/config::WORLD_RENDER_SCALE;

            ex.systems.add<MovementSystem>();
            ex.systems.add<BoundsCollisionSystem>(worldWidth, worldHeight);
            ex.systems.add<SelfDestructSystem>(timeKeeper);
            ex.systems.add<SparkDestructionSystem>(inputSystem, cameraSystem.getTopDownCamera());
            ex.systems.add<AnimationSystem>(timeKeeper);
            ex.systems.add<RenderingSystem>(gfx::SpriteRenderer<camera::TopDownCamera>(window.getRendererPtr(), cameraSystem.getTopDownCamera()));
            ex.systems.add<LineRenderingSystem>(window.getRendererPtr(), cameraSystem.getTopDownCamera());
            ex.systems.configure();

            generateWorld(
                worldWidth,
                worldHeight,
                this->sparkFactory,
                NUMBER_OF_ENTITIES,
                contentManager.getSparkAnimatedSprite().getFirstSprite().getWidth(),
                contentManager.getSparkAnimatedSprite().getFirstSprite().getHeight(),
                MAX_VELOCITY_COORD
            );
        }

        void GameWorld::updateWorldImpl(const real deltaTime)
        {
            // TODO: This may be better placed in renderWorldImpl().
            cameraSystem.update(deltaTime);

            ex.systems.update<MovementSystem>(deltaTime);
            ex.systems.update<BoundsCollisionSystem>(deltaTime);
            ex.systems.update<SelfDestructSystem>(deltaTime);
            ex.systems.update<SparkDestructionSystem>(deltaTime);
        }
        
        void GameWorld::renderWorldImpl(const real deltaTime)
        {
            static gfx::TilemapRenderer<camera::TopDownCamera> tilemapRenderer(
                window.getRendererPtr(),
                cameraSystem.getTopDownCamera()
            );

            ex.systems.update<AnimationSystem>(deltaTime);

            const SDL_Color clearColor = {0, 0, 0, 0xFF};

            auto prenderer = window.getRendererPtr();
            
            ScopedRenderDrawColor srdc0(prenderer, clearColor);
            SDL_RenderClear(prenderer);
            
            // Render test tilemap
            tilemapRenderer.renderMap(this->contentManager.getTestTilemap(), {0,0});

            ex.systems.update<RenderingSystem>(deltaTime);
            ex.systems.update<LineRenderingSystem>(deltaTime);

            fpsCounter.render(/*deltaTime*/);

            SDL_RenderPresent(prenderer);
        }
    }
}
