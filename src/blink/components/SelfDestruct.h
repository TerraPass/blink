#ifndef BLINK_COMPONENTS_SELFDESTRUCT_H
#define BLINK_COMPONENTS_SELFDESTRUCT_H

#include <limits>

#include "common/types.h"

namespace blink
{
    namespace components
    {
        struct SelfDestruct final
        {
            static constexpr const real DEFAULT_DESTRUCTION_TIME = std::numeric_limits<real>::infinity();

            real destructionTime;

            explicit SelfDestruct(const real destructionTime = DEFAULT_DESTRUCTION_TIME)
            : destructionTime(destructionTime)
            {
                
            }

            SelfDestruct(const real currentTime, const real delay)
            : destructionTime(currentTime + delay)
            {
                
            }
        };
    }
}

#endif /* BLINK_COMPONENTS_SELFDESTRUCT_H */

