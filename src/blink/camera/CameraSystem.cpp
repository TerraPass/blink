#include "CameraSystem.h"

namespace blink
{
    namespace camera
    {
        constexpr const WorldPosition CameraSystem::INITIAL_TOPDOWN_CAMERA_TARGET;

        CameraSystem::CameraSystem(const gfx::unit outputWidth, const gfx::unit outputHeight, input::InputSystem& inputSystem, const U8 topDownCameraScale, const U8 uiCameraScale)
        : topDownCamera(inputSystem, outputWidth, outputHeight, INITIAL_TOPDOWN_CAMERA_TARGET, topDownCameraScale),
            uiCamera(outputWidth, outputHeight, uiCameraScale)
        {

        }

        TopDownCamera& CameraSystem::getTopDownCameraImpl()
        {
            return topDownCamera;
        }
        
        const TopDownCamera& CameraSystem::getTopDownCameraImpl() const
        {
            return topDownCamera;
        }

        UiCamera& CameraSystem::getUiCameraImpl()
        {
            return uiCamera;
        }

        const UiCamera& CameraSystem::getUiCameraImpl() const
        {
            return uiCamera;
        }
    }
}
