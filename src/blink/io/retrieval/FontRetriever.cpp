#include "FontRetriever.h"

#include <SDL_ttf.h>
#include <sdl2utils/exceptions.h>
#include <sdl2utils/guards.h>

//#include "sdl/pointers.h"
#include "io/exceptions.h"

namespace blink
{
    namespace io
    {
        namespace retrieval
        {
            using namespace sdl2utils;
            using namespace io::data;
            //using sdl::makeSharedTTFFont;

            FontRetriever::FontPtr FontRetriever::retrieve(const FontResolvedKey& resolvedFontKey) const try
            {
                return FontPtr(
                    sdl2utils::guards::ensureNotNull(
                        TTF_OpenFont(resolvedFontKey.getFilePath().c_str(), resolvedFontKey.size),
                        "TTF_OpenFont() return value"
                    ),
                    TTF_CloseFont
                );
//                return makeSharedTTFFont(
//                    io::detail::throwSDLErrorExceptionIfNull(
//                        TTF_OpenFont(resolvedFontKey.getFilePath().c_str(), resolvedFontKey.size),
//                        "TTF_OpenFont() return value"
//                    )
//                );
            }
            catch(const SDLErrorException& e)
            {
                throw io::detail::RawDataRetrievalExceptionImpl<FontRetriever::FontPtr>(
                    resolvedFontKey.getFilePath(),
                    e.what()
                );
            }
        }
    }
}
