#include "utils.h"

#include <unordered_map>
#include <stdexcept>
#include <boost/format.hpp>

#include "common/numeric.h"
#include "io/types.h"

namespace blink
{
    namespace io
    {
        namespace converters
        {
            namespace
            {
                using boost::format;
                using boost::str;

                using AnyPropWrapperFunc = std::function<boost::any(const nlohmann::json&)>;

                const std::unordered_map<std::string, AnyPropWrapperFunc> anyPropWrappers = {
                    {"bool",    [](const nlohmann::json& value) { return value.get<bool>(); }},
                    {"file",    [](const nlohmann::json& value) { return value.get<FilePath>(); }},
                    {"string",  [](const nlohmann::json& value) { return value.get<std::string>(); }},
                    {"color",   [](const nlohmann::json& value) { return value.get<std::string>(); }},
                    {"float",   [](const nlohmann::json& value) { return value.get<real>(); }},
                    {"int",     [](const nlohmann::json& value) { return value.get<I64>(); }}
                };

                inline boost::any anyWrapProperty(const std::string& type, const nlohmann::json& value, const std::string& valueName) try
                {
                    return anyPropWrappers.at(type)(value);
                }
                catch(const std::out_of_range& e)
                {
                    throw std::invalid_argument(str(format("Unrecognized property type \"%1%\" specified for property \"%2%\" (%3%)") % type % valueName % e.what()));
                }
                catch(const std::domain_error& e)
                {
                    throw std::domain_error(str(format("Type mismatch for property \"%1%\" (%2%)") % valueName % e.what()));
                }
            }

            void readTiledProperties(
                data::TilemapData::Properties& targetContainer,
                const nlohmann::json& outerJson,
                const bool required
            )
            {
                const auto propertiesIt = outerJson.find("properties");

                if(propertiesIt == outerJson.cend())
                {
                    if(required)
                    {
                        throw std::invalid_argument("outerJson contains no \"properties\" key");
                    }
                    else
                    {
                        return;
                    }
                }

                const auto propertiesJson = *propertiesIt;
                if(!propertiesJson.empty())
                {
                    const auto propertyTypes = outerJson.at("propertytypes");
                    for(auto it = propertiesJson.cbegin(); it != propertiesJson.cend(); it++)
                    {
                        targetContainer.emplace(
                            it.key(),
                            anyWrapProperty(
                                propertyTypes.at(it.key()),
                                it.value(),
                                it.key()
                            )
                        );
                    }
                }
            }
        }
    }
}
