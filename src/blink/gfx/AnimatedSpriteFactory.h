#ifndef BLINK_GFX_ANIMATEDSPRITEFACTORY_H
#define BLINK_GFX_ANIMATEDSPRITEFACTORY_H

#include <sdl2utils/pointers.h>

#include "io/data/AnimationData.h"

#include "AnimationContainerFactory.h"
#include "AnimatedSprite.h"

namespace blink
{
    namespace gfx
    {
        class AnimatedSpriteFactory final
        {
            static U32 nextTrivialAnimId;

            const AnimationContainerFactory animationsFactory;

        public:
            static constexpr const U32 STATIC_TRIVIAL_FRAME_DURATION = 100;

            AnimatedSpriteFactory()
            : animationsFactory()
            {

            }

            AnimatedSprite create(const io::data::AnimationData& data, sdl2utils::SDL_TexturePtr pspriteSheet) const;
            AnimatedSprite createStatic(sdl2utils::SDL_TexturePtr pstaticSprite) const;
        };
    }
}

#endif /* BLINK_GFX_ANIMATEDSPRITEFACTORY_H */

