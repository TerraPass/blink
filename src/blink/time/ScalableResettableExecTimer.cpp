#include "ScalableResettableExecTimer.h"

namespace blink
{
    namespace time
    {
        constexpr const real ScalableResettableExecTimer::DEFAULT_SCALE;
        
        void ScalableResettableExecTimer::setScaleImpl(const real scale)
        {
            if(!this->getPaused())
            {
                const auto currentTime = getCurrentTime();

                this->setAccumulatedDuration(
                    // FIXME: Assertion check needed
                    this->getAccumulatedDuration() + static_cast<Duration>(
                        static_cast<Duration::rep>(this->scale) * (currentTime - this->getStartTime())
                    )
                );
                this->setStartTime(currentTime);
            }
            
            this->scale = scale;
        }

        bool ScalableResettableExecTimer::pauseImpl()
        {
            if(!this->getPaused())
            {
                this->setAccumulatedDuration(
                    // FIXME: Assertion check needed
                    this->getAccumulatedDuration() + static_cast<Duration>(
                        static_cast<Duration::rep>(this->scale) * (getCurrentTime() - this->getStartTime())
                    )
                );
                this->setPaused(true);
                
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
