#ifndef BLINK_TIME_IRESETTABLETIMER_H
#define BLINK_TIME_IRESETTABLETIMER_H

#include "ITimer.h"

namespace blink
{
    namespace time
    {
        class IResettableTimer : public virtual ITimer
        {
        public:
            IResettableTimer() = default;
            virtual ~IResettableTimer() = default;

            void reset(const bool startPaused = false)
            {
                this->resetImpl(startPaused);
            }

        private:
            virtual void resetImpl(const bool startPaused) = 0;
        };
    }
}

#endif /* BLINK_TIME_IRESETTABLETIMER_H */

