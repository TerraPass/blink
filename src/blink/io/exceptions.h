#ifndef BLINK_IO_EXCEPTIONS_H
#define BLINK_IO_EXCEPTIONS_H

#include "common/exceptions.h"

#include "data/getDisplayName.h"
#include "types.h"

namespace blink
{
    namespace io
    {
        class IOException : public common::BlinkException
        {
            static constexpr const auto MESSAGE_TEMPLATE = "%1% (%2%)";
            static constexpr const auto DEFAULT_MESSAGE = "Unknown IO error";
            
        protected:
            static constexpr const auto DEFAULT_DETAILS = "no details available";

        public:
            explicit IOException(const std::string& message = DEFAULT_MESSAGE, const std::string& details = DEFAULT_DETAILS);
        };

        class KeyResolutionException : public IOException
        {
            static constexpr const auto MESSAGE_TEMPLATE = "Failed to resolve key %1%";

            const BasicKey basicKey;

        public:
            explicit KeyResolutionException(const BasicKey& basicKey, const std::string& details = DEFAULT_DETAILS);

            inline const BasicKey& getBasicKey() const
            {
                return basicKey;
            }
        };

        class RawDataRetrievalException : public IOException
        {
            static constexpr const auto MESSAGE_TEMPLATE = "Couldn't retrieve %1% from %2%";

            const std::string rawDataTypeName;
            const FilePath source;

        public:
            RawDataRetrievalException(const std::string& rawDataTypeName, const FilePath& source, const std::string& details = DEFAULT_DETAILS);

            inline const std::string& getRawDataTypeName() const
            {
                return rawDataTypeName;
            }

            inline const FilePath& getSource() const
            {
                return source;
            }
        };
        
        class RawDataOpeningFailedException : public RawDataRetrievalException
        {
            static constexpr const auto DETAILS_TEMPLATE = "failed to open %1% for reading";

        public:
            RawDataOpeningFailedException(const std::string& rawDataTypeName, const FilePath& source);
        };
        
        class RawDataFormatException : public RawDataRetrievalException
        {
            static constexpr const auto DETAILS_TEMPLATE = "%1% has invalid format%2%";

        protected:
            static constexpr const auto DEFAULT_INNER_MESSAGE = "";

        public:
            RawDataFormatException(const std::string& rawDataTypeName, const FilePath& source, const std::string& innerMessage = DEFAULT_INNER_MESSAGE);
        };
        
        class DataConversionException : public IOException
        {
            static constexpr const auto MESSAGE_TEMPLATE = "Failed to convert %1% to %2%";

            const std::string sourceTypeName;
            const std::string destTypeName;

        public:
            DataConversionException(const std::string& sourceTypeName, const std::string& destTypeName, const std::string& details = DEFAULT_DETAILS);

            inline const std::string& getSourceTypeName() const
            {
                return sourceTypeName;
            }

            inline const std::string& getDestTypeName() const
            {
                return destTypeName;
            }
        };

        namespace detail
        {
            template <typename RawData>
            class RawDataRetrievalExceptionImpl : public RawDataRetrievalException
            {
            public:
                explicit RawDataRetrievalExceptionImpl(const FilePath& source, const std::string& details = DEFAULT_DETAILS)
                : RawDataRetrievalException(data::getDisplayName<RawData>(), source, details)
                {
                    
                }
            };

            template <typename RawData>
            class RawDataOpeningFailedExceptionImpl : public RawDataOpeningFailedException
            {
            public:
                explicit RawDataOpeningFailedExceptionImpl(const FilePath& source)
                : RawDataOpeningFailedException(data::getDisplayName<RawData>(), source)
                {
                    
                }
            };

            template <typename RawData>
            class RawDataFormatExceptionImpl : public RawDataFormatException
            {
            public:
                explicit RawDataFormatExceptionImpl(const FilePath& source, const std::string& innerMessage = RawDataFormatException::DEFAULT_INNER_MESSAGE)
                : RawDataFormatException(data::getDisplayName<RawData>(), source, innerMessage)
                {
                    
                }
            };

            template <typename SourceType, typename DestType>
            class DataConversionExceptionImpl : public DataConversionException
            {
            public:
                explicit DataConversionExceptionImpl(const std::string& details = DEFAULT_DETAILS)
                : DataConversionException(data::getDisplayName<SourceType>(), data::getDisplayName<DestType>(), details)
                {
                    
                }
            };
        }
    }
}

#endif /* BLINK_IO_EXCEPTIONS_H */

