#ifndef BLINK_CAMERA_WASDMOVEDCAMERA_H
#define BLINK_CAMERA_WASDMOVEDCAMERA_H

#include "common/events.h"
#include "input/InputSystem.h"

namespace blink
{
    namespace camera
    {
        template <typename Camera>
        class WasdMovedCamera : public Camera
        {
            input::InputSystem& inputSystem;
            common::ScopedSubscription keyDownSub;
            common::ScopedSubscription keyUpSub;

            // TODO: Create an input::Keyboard class, which would provide
            // convenient methods, such as isKeyPressed(Keycode),
            // which would free the clients of InputSystem from maintaining
            // such masks themselves.
            U8 currentKeyMask;

        public:
            static constexpr const real SENSITIVITY = 100.0; // WorldPosition units per second

            template <typename... Args>
            explicit WasdMovedCamera(input::InputSystem& inputSystem, Args&&... args);

            WasdMovedCamera(WasdMovedCamera&& other);

            void update(const real seconds);

        private:
            void onKeyDown(input::InputSystem& inputSystem, const input::KeyboardEventArgs& args);
            void onKeyUp(input::InputSystem& inputSystem, const input::KeyboardEventArgs& args);
        };

        template<typename Camera>
        template<typename... Args>
        WasdMovedCamera<Camera>::WasdMovedCamera(input::InputSystem& inputSystem, Args&&... args)
        : Camera(std::forward<Args>(args)...),
            inputSystem(inputSystem),
            keyDownSub(
                inputSystem.getKeyDownEvent().subscribe(&WasdMovedCamera<Camera>::onKeyDown, this)
            ),
            keyUpSub(
                inputSystem.getKeyUpEvent().subscribe(&WasdMovedCamera<Camera>::onKeyUp, this)
            ),
            currentKeyMask(0)
        {

        }
    }
}

#ifndef BLINK_COMPILE_TEMPLATES
#include "WasdMovedCamera.cxx"
#endif

#endif /* BLINK_CAMERA_WASDMOVEDCAMERA_H */

