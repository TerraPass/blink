#include "MapTilesContainer.h"

#include <stdexcept>
#include <algorithm>
#include <boost/format.hpp>

#include "debug/typeinfo.h"

namespace blink
{
    namespace tiling
    {
        using boost::str;
        using boost::format;

        using TilesetContainerElem = MapTilesContainer::TilesetContainer::value_type;

        template <typename Exception = std::invalid_argument>
        [[noreturn]] static inline void throwWithTypename(const std::string& messageFormat)
        {
            const auto typeName = debug::getTypename<MapTilesContainer>();
            throw Exception(str(format(messageFormat) % typeName));
        }

        static inline bool firstgidLess(const TilesetContainerElem& elem0, const TilesetContainerElem& elem1)
        {
            return elem0.first < elem1.first;
        }

        void MapTilesContainer::sanityCheckTilesets(const TilesetContainer& tilesets, const pixelunit tileWidth, const pixelunit tileHeight)
        {
            if(tilesets.empty())
            {
                throwWithTypename("%1% expected a non-empty tilesets container");
            }
            if(!std::is_sorted(tilesets.cbegin(), tilesets.cend(), firstgidLess))
            {
                throwWithTypename("%1% got an unsorted tilesets container");
            }
            if(tilesets[0].first != 1)
            {
                throwWithTypename("%1% expected the first tileset in the container to have firstgid 1");
            }
            if(!std::all_of(
                tilesets.cbegin(),
                tilesets.cend(),
                [tileWidth, tileHeight](const std::pair<tilegid, Tileset>& pair) {
                    return pair.second.getTileWidth() == tileWidth && pair.second.getTileHeight() == tileHeight;
                }
            ))
            {
                throwWithTypename("%1% expected all of the tilesets in the container to have the same dimensions");
            }
        }

        MapTilesContainer::TilesetContainerKey MapTilesContainer::toTilesetContainerKey(const tilegid gid) const
        {
            if(gid <= 0)
            {
                throw std::invalid_argument(
                    str(format("%1% expected a positive tile GID, got %2%")
                        % debug::getTypename(*this)
                        % gid)
                );
            }

            assert(std::is_sorted(tilesets.cbegin(), tilesets.cend(), firstgidLess));

            int tilesetIndex = static_cast<int>(tilesets.size()) - 1;
            while(tilesetIndex > 0 && tilesets[tilesetIndex].first > gid)
            {
                tilesetIndex--;
            }
            assert(tilesetIndex >= 0);

            const auto firstTilesetGid = tilesets[tilesetIndex].first;
            assert(gid >= firstTilesetGid);

            const size_t tileIndex = static_cast<size_t>(gid - firstTilesetGid);

            return std::make_pair(tilesetIndex, tileIndex);
        }
    }
}
