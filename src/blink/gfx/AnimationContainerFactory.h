#ifndef BLINK_GFX_ANIMATIONCONTAINERFACTORY_H
#define BLINK_GFX_ANIMATIONCONTAINERFACTORY_H

#include <SDL.h>

#include "io/data/AnimationData.h"

#include "AnimationContainer.h"

namespace blink
{
    namespace gfx
    {
        class AnimationContainerFactory final
        {
            const AnimationFactory animationFactory;

        public:
            static constexpr const auto DEFAULT_FRAME_TAG_NAME = "idle";
            static constexpr const Animation::Direction DEFAULT_DIRECTION = Animation::Direction::FORWARD;

            AnimationContainerFactory()
            : animationFactory()
            {

            }

            AnimationContainer create(const io::data::AnimationData& data, SDL_Texture* const pspriteSheet) const;
        };
    }
}

#endif /* BLINK_GFX_ANIMATIONCONTAINERFACTORY_H */

