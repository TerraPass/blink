#ifndef BLINK_SYSTEMS_SPARKDESTRUCTIONSYSTEM_H
#define BLINK_SYSTEMS_SPARKDESTRUCTIONSYSTEM_H

#include <boost/optional.hpp>

#include <entityx/entityx.h>

#include "input/InputSystem.h"
#include "input/MousePosition.h"
#include "camera/TopDownCamera.h"

namespace blink
{
    namespace systems
    {
        class SparkDestructionSystem final : public entityx::System<SparkDestructionSystem>
        {
            input::InputSystem& inputSystem;
            const camera::TopDownCamera& camera;

            common::ScopedSubscription mouseButtonDownSub;

            boost::optional<input::MousePosition> maybeLmbClickPosition;

        public:
            explicit SparkDestructionSystem(input::InputSystem& inputSystem, const camera::TopDownCamera& camera);
            
            SparkDestructionSystem(const SparkDestructionSystem&) = delete;
            SparkDestructionSystem(SparkDestructionSystem&&);   // defined
            
            SparkDestructionSystem& operator=(const SparkDestructionSystem&) = delete;
            SparkDestructionSystem& operator=(SparkDestructionSystem&&) = delete;
            
            ~SparkDestructionSystem();  // defined

            void update(entityx::EntityManager& entities, entityx::EventManager& events, entityx::TimeDelta dt) override;

        private:
            void onMouseButtonDown(input::InputSystem&, input::MouseEventArgs args);
        };
    }
}

#endif /* BLINK_SYSTEMS_SPARKDESTRUCTIONSYSTEM_H */

