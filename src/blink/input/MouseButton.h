#ifndef BLINK_INPUT_MOUSEBUTTON_H
#define BLINK_INPUT_MOUSEBUTTON_H

namespace blink
{
    namespace input
    {
        enum class MouseButton
        {
            LEFT    = 0,
            RIGHT   = 1,
            MIDDLE  = 2,

            UNKNOWN = 3
        };
    }
}

#endif /* MOUSEBUTTON_H */

