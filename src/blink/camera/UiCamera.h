#ifndef BLINK_CAMERA_UICAMERA_H
#define BLINK_CAMERA_UICAMERA_H

#include "types.h"

namespace blink
{
    namespace camera
    {
        class UiCamera final
        {
        public:
            using InputPosition = ScreenPosition;
            //using OutputPosition = gfx::ScreenPosition;

        private:
            gfx::unit outputWidth;
            gfx::unit outputHeight;

            U8 scale;
            
        public:
            static const InputPosition DEFAULT_TARGET_POSITION;
            static constexpr const U8 DEFAULT_SCALE = 1;

            explicit UiCamera(
                const gfx::unit outputWidth,
                const gfx::unit outputHeight,
                const U8 scale = DEFAULT_SCALE
            )
            : outputWidth(outputWidth), outputHeight(outputHeight), scale(scale)
            {
                
            }

            inline gfx::unit getOutputWidth() const
            {
                return this->outputWidth;
            }

            inline gfx::unit getOutputHeight() const
            {
                return this->outputHeight;
            }

            inline InputPosition getInputTopLeft() const
            {
                return fromScreenPosition({0, 0});
            }

            inline InputPosition getInputBottomRight() const
            {
                return fromScreenPosition({outputWidth, outputHeight});
            }

            inline gfx::unit getInputWidth() const
            {
                return getInputBottomRight().x - getInputTopLeft().x;
            }

            inline gfx::unit getInputHeight() const
            {
                return getInputBottomRight().y - getInputTopLeft().y;
            }

            inline U8 getScale() const
            {
                return scale;
            }

            inline UiCamera& setScale(const U8 scale)
            {
                this->scale = scale;
                return *this;
            }

            ScreenPosition toScreenPosition(const InputPosition inputPosition) const;
            InputPosition fromScreenPosition(const ScreenPosition screenPosition) const;

            Rect scaleRect(const Rect rect) const;
        };
    }
}

#endif /* BLINK_CAMERA_UICAMERA_H */

