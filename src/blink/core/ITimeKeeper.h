#ifndef BLINK_CORE_ITIMEKEEPER_H
#define BLINK_CORE_ITIMEKEEPER_H

#include "common/numeric.h"

namespace blink
{
    namespace core
    {
        class ITimeKeeper
        {
        public:
            ITimeKeeper() = default;
            virtual ~ITimeKeeper() = default;

            real getRealtimeElapsedSeconds() const
            {
                return this->getRealtimeElapsedSecondsImpl();
            }

            real getGameplayElapsedSeconds() const
            {
                return this->getGameplayElapsedSecondsImpl();
            }
            
        private:
            virtual real getRealtimeElapsedSecondsImpl() const = 0;
            virtual real getGameplayElapsedSecondsImpl() const = 0;
        };
    }
}

#endif /* BLINK_CORE_ITIMEKEEPER_H */

