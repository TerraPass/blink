#ifndef BLINK_IO_CONVERTERS_JSONTOANIMATIONDATACONVERTER_H
#define BLINK_IO_CONVERTERS_JSONTOANIMATIONDATACONVERTER_H

#include <string>

#include <nlohmann_json.hpp>

#include "io/data/AnimationData.h"

namespace blink
{
    namespace io
    {
        namespace converters
        {
            class JsonToAnimationDataCoverter final
            {
                // TODO: Make configurable
                static const std::string EXPECTED_FORMAT;
                static const std::string EXPECTED_SCALE;

            public:
                data::AnimationData convert(const nlohmann::json& dataJson) const;
            };
        }
    }
}

#endif /* BLINK_IO_CONVERTERS_JSONTOANIMATIONDATACONVERTER_H */

