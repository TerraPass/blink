#ifndef BLINK_GFX_TILESETCONTAINER_H
#define BLINK_GFX_TILESETCONTAINER_H

#include <vector>
#include <cassert>

#include "types.h"
#include "Tileset.h"
#include "Tile.h"

namespace blink
{
    namespace tiling
    {
        /**
         * Contains tiles from all of the tilesets for a particular Tiled tilemap
         * and provides operator[] and method at(), which allow to retrieve the tile
         * by the map-specific GID.
         */
        class MapTilesContainer final
        {
        public:
            /**
             * Contains tilesets, paired with their firstgid values.
             */
            using TilesetContainer = std::vector<std::pair<tilegid, Tileset>>;

        private:
            /**
             * Contains tilesets, paired with their firstgid values.
             */
            TilesetContainer tilesets; 

            const pixelunit tileWidth;
            const pixelunit tileHeight;

            /**
             * @throw std::invalid_argument if tilesets is empty, unsorted
             * or does not begin with a tileset having 1 as its firstgid.
             */
            static void sanityCheckTilesets(const TilesetContainer& tilesets, const pixelunit tileWidth, const pixelunit tileHeight);

        public:
            explicit MapTilesContainer(TilesetContainer&& tilesets, const pixelunit tileWidth, const pixelunit tileHeight)
            : tilesets(std::move(tilesets)), tileWidth(tileWidth), tileHeight(tileHeight)
            {
                sanityCheckTilesets(this->tilesets, tileWidth, tileHeight);
            }

            template <typename InputIt>
            MapTilesContainer(InputIt tilesetsBegin, InputIt tilesetsEnd, const pixelunit tileWidth, const pixelunit tileHeight)
            : tilesets(tilesetsBegin, tilesetsEnd), tileWidth(tileWidth), tileHeight(tileHeight)
            {
                sanityCheckTilesets(this->tilesets, tileWidth, tileHeight);
            }

            // Copy constructor would be implicitly deleted anyway,
            // since Tileset is non-copyable.
            MapTilesContainer(const MapTilesContainer&) = delete;
            MapTilesContainer(MapTilesContainer&&) = default;   // sic

            MapTilesContainer& operator=(const MapTilesContainer&) = delete;
            MapTilesContainer& operator=(MapTilesContainer&&) = delete;

        private:
            /**
             * The first element of the pair contains the index of the tileset,
             * the second - the 0-based index of the tile within said tileset.
             */
            using TilesetContainerKey = std::pair<size_t, size_t>;

            TilesetContainerKey toTilesetContainerKey(const tilegid gid) const;

        public:
            inline Tile operator[](const tilegid gid) const
            {
                const auto key = toTilesetContainerKey(gid);
                assert(key.first < tilesets.size() && key.second < tilesets[key.second].second.getTileCount());
                return tilesets[key.first].second[key.second];
            }

            inline Tile at(const tilegid gid) const
            {
                const auto key = toTilesetContainerKey(gid);
                return tilesets[key.first].second.at(key.second);
            }

            inline pixelunit getTileWidth() const
            {
                return tileWidth;
            }

            inline pixelunit getTileHeight() const
            {
                return tileHeight;
            }
        };

        using MapTilesContainerPtr = std::shared_ptr<MapTilesContainer>;
        using ConstMapTilesContainerPtr = std::shared_ptr<const MapTilesContainer>;
    }
}

#endif /* BLINK_GFX_TILESETCONTAINER_H */

