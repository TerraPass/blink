#include "LineRenderingSystem.h"

#include "components/Line.h"

namespace blink
{
    namespace systems
    {
        void LineRenderingSystem::update(entityx::EntityManager& entities, entityx::EventManager& /*events*/, entityx::TimeDelta /*dt*/)
        {
            using namespace components;

            using entityx::Entity;

            entities.each<Line>(
                [this](Entity /*entity*/, Line& line) {
                    lineRenderer.renderLine(line.source, line.target, line.ray, line.color);
                }
            );
        }
    }
}
