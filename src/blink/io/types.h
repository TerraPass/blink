#ifndef BLINK_IO_TYPES_H
#define BLINK_IO_TYPES_H

#include <string>
#include <istream>

namespace blink
{
    namespace io
    {
        using BasicKey = std::string;
        using FilePath = std::string;
    }
}

#endif /* BLINK_IO_TYPES_H */

