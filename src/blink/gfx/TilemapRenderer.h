#ifndef BLINK_GFX_TILEMAPRENDERER_H
#define BLINK_GFX_TILEMAPRENDERER_H

#include <SDL.h>

#include "tiling/Tilemap.h"

#include "TileLayerRenderer.h"

namespace blink
{
    namespace gfx
    {
        template <typename Camera>
        class TilemapRenderer final
        {
            // mutable due to caching;
            // maybe tileLayerRenderer should instead present a const renderLayer()
            // function and have its own cache-related fields be marked mutable?
            mutable TileLayerRenderer<Camera> tileLayerRenderer;

        public:
            TilemapRenderer(SDL_Renderer* const prenderer, const Camera& camera)
            : tileLayerRenderer(prenderer, camera)
            {
                
            }

            void renderMap(const tiling::Tilemap& tilemap, const typename Camera::InputPosition offset) const;
        };
    }
}

#ifndef BLINK_COMPILE_TEMPLATES
#include "TilemapRenderer.cxx"
#endif

#endif /* BLINK_GFX_TILEMAPRENDERER_H */

