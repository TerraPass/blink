#include "TopDownCamera.h"

#include <cmath>

#include "common/utils/math.h"

#include "utils.h"

namespace blink
{
    namespace camera
    {
        TopDownCamera::TopDownCamera(const gfx::unit outputWidth, const gfx::unit outputHeight, const TopDownCamera::InputPosition targetPosition, const U8 scale)
        : outputWidth(outputWidth), outputHeight(outputHeight), targetPosition(targetPosition), scale(scale)
        {

        }

        inline auto toScreenPosUnit(const real value)
        {
            return floorTo<ScreenPosition::unit>(value);
        }
        
        ScreenPosition TopDownCamera::toScreenPosition(const WorldPosition worldPosition) const
        {
            // TODO: Take into account the actual screen origin (top left corner)
            // and treat targetPosition as the center of the screen (maybe...)
            const auto relativeToCamPosition = worldPosition - targetPosition;
            const auto scaledPosition = scale*relativeToCamPosition;
            return ScreenPosition(
                toScreenPosUnit(scaledPosition.x),
                toScreenPosUnit(scaledPosition.y)
            );
        }

        TopDownCamera::InputPosition TopDownCamera::fromScreenPosition(const ScreenPosition screenPosition) const
        {
            // TODO: Take into account the actual screen origin (top left corner)
            // and treat targetPosition as the center of the screen (maybe...)
            const auto descaledScreenPosition = screenPosition / scale;
            const auto worldPosition = staticVector2Cast<real>(descaledScreenPosition) + targetPosition;
            return worldPosition;
        }

        Rect TopDownCamera::scaleRect(const Rect rect) const
        {
            return blink::camera::scaleRect(rect, scale);
        }

        Rect TopDownCamera::toScreenRect(const Rect rect) const
        {
            const InputPosition rectPosition(static_cast<real>(rect.x), static_cast<real>(rect.y));
            const auto screenRectPosition = toScreenPosition(rectPosition);
            return {screenRectPosition.x, screenRectPosition.y, scale*rect.w, scale*rect.h};
        }

        Rect TopDownCamera::fromScreenRect(const Rect rect) const
        {
            const auto worldRectPosition = fromScreenPosition({rect.x, rect.y});
            return {
                floorTo<decltype(Rect::x)>(worldRectPosition.x),
                floorTo<decltype(Rect::y)>(worldRectPosition.y),
                rect.w / scale,
                rect.h / scale
            };
        }

        void TopDownCamera::lookAt(const InputPosition target)
        {
            targetPosition = fromScreenPosition(toScreenPosition(target) - ScreenPosition{outputWidth/2, outputHeight/2});
        }

        TopDownCamera::InputPosition TopDownCamera::getLookAtTarget() const
        {
            return fromScreenPosition(toScreenPosition(targetPosition) + ScreenPosition{outputWidth/2, outputHeight/2});
        }
    }
}
