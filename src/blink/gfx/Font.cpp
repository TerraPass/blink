#include "Font.h"

namespace blink
{
    namespace gfx
    {
        Font::Font(const RenderMode renderMode, const U16 height, AtlasPtr&& patlas, CharMap&& charMap)
        : renderMode(renderMode), height(height), patlas(std::move(patlas)), charMap(std::move(charMap))
        {

        }

        Sprite Font::getSpriteForChar(const char ch) const
        {
            static const int unknownCharMapIndex = getCharMapIndex(UNKNOWN_CHAR_REPLACEMENT);
            const int charMapIndex = getCharMapIndex(ch);

            const Rect rect = hasChar(ch)
                ? charMap[charMapIndex]
                : charMap[unknownCharMapIndex];

            return Sprite(rect, patlas.get());
        }
    }
}
