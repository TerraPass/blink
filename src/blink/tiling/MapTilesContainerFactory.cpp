#include "MapTilesContainerFactory.h"

#include <algorithm>
#include <functional>
#include <cassert>

namespace blink
{
    namespace tiling
    {
        std::unique_ptr<MapTilesContainer> MapTilesContainerFactory::create(std::vector<io::data::TilesetData>&& mapTilesetsData) const
        {
            // Ensure that tilesets collection is non-empty
            if(mapTilesetsData.empty())
            {
                throw std::invalid_argument("mapTilesetsData collection must not be empty");
            }

            // Retrieve first tileset's dimensions
            const auto tileWidth = static_cast<pixelunit>(mapTilesetsData[0].metadata.tilewidth);
            const auto tileHeight = static_cast<pixelunit>(mapTilesetsData[0].metadata.tileheight);

            // Ensure that all tilesets have the same dimensions
            if(!std::all_of(
                mapTilesetsData.cbegin() + 1,
                mapTilesetsData.cend(),
                [tileWidth, tileHeight](const io::data::TilesetData& tilesetDatum) {
                    return tilesetDatum.metadata.tilewidth == tileWidth && tilesetDatum.metadata.tileheight == tileHeight;
                }
            ))
            {
                throw std::invalid_argument("mapTilesetsData must contain tilesets of the same dimensions");
            }

            // Assume ownership over the TilesetData collection
            auto tilesetsData = std::move(mapTilesetsData);

            // Sort the collection in ascending order of tilesets' firstgids
            std::sort(
                tilesetsData.begin(),
                tilesetsData.end(),
                [](const io::data::TilesetData& tilesetData0, const io::data::TilesetData& tilesetData1) {
                    return tilesetData0.metadata.firstgid < tilesetData1.metadata.firstgid;
                }
            );

            // Convert the collection into MapTilesContainer
            MapTilesContainer::TilesetContainer tilesets;

            std::transform(
                tilesetsData.begin(),
                tilesetsData.end(),
                std::back_inserter(tilesets),
                [this](io::data::TilesetData& tilesetData) {
                    const auto firstgid = tilesetData.metadata.firstgid;
                    return std::make_pair(
                        firstgid,
                        tilesetFactory.create(std::move(tilesetData))
                    );
                }
            );

            assert(std::is_sorted(
                tilesets.cbegin(),
                tilesets.cend(),
                [](const auto& pair0, const auto& pair1) {
                    return pair0.first < pair1.first;
                }
            ));

            return std::make_unique<MapTilesContainer>(std::move(tilesets), tileWidth, tileHeight);
        }
    }
}
