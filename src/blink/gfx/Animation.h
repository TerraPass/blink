#ifndef BLINK_GFX_ANIMATION_H
#define BLINK_GFX_ANIMATION_H

#include <vector>
#include <cassert>
#include <numeric>
#include <algorithm>

#include "common/numeric.h"
#include "io/data/AnimationData.h"

#include "types.h"
#include "Sprite.h"

namespace blink
{
    namespace gfx
    {
        class Animation final
        {   
        public:
            // TODO:
            // It's not ideal that animation direction is only considered
            // at creation stage to arrage the frames in a particular way,
            // when the user might realistically decide to change
            // the animation direction at runtime, which currently is impossible,
            // since the relevant information is used and then discarded at creation stage.
            enum class Direction
            {
                FORWARD     = 0,
                REVERSE     = 1,
                PING_PONG   = 2
            };

            struct Frame final
            {
                // Rect within the spritesheet
                Rect rect;
                // Frame duration in milliseconds
                U32 duration;

                Frame(const Rect rect, const U32 duration)
                : rect(rect), duration(duration)
                {
                    
                }
            };
            
        private:
            // TODO: Reconsider the type of this field.
            // As it stands, Animation instances are designed to be copied/copy-assigned,
            // however the frame vector should probably be shared between Animations that use it,
            // instead of being copied. (This might need to be refactored into a flyweight.
            std::vector<Frame> frames;
            SDL_Texture* pspriteSheet;

            // TODO: Consider replacing factory-provided ID with data-based hash.
            // (This way identical separately loaded animations will be considered equal.)
            U32 id;

            // Total animation duration in milliseconds
            U32 totalDuration;
            
            template<typename FramesInputIt>
            static inline U32 sumDuration(FramesInputIt framesBegin, FramesInputIt framesEnd)
            {
                return std::accumulate(
                    framesBegin, framesEnd, 
                    static_cast<U32>(0), 
                    [](U32 soFar, const Frame& cur) {return soFar + cur.duration;}
                );
            }
            
            inline void ensureNotEmpty() const
            {
                if(frames.empty())
                {
                    throw std::invalid_argument("Animation must have at least 1 frame");
                }
            }

        public:
            Animation(std::vector<Frame>&& frames, SDL_Texture* const pspriteSheet, const U32 id)
            : frames(frames), pspriteSheet(pspriteSheet), id(id),
                totalDuration(sumDuration(this->frames.cbegin(), this->frames.cend()))
            {
                ensureNotEmpty();
            }

            template<typename FramesInputIt>
            Animation(FramesInputIt framesBegin, FramesInputIt framesEnd, SDL_Texture* const pspriteSheet, const U32 id)
            :frames(framesBegin, framesEnd), pspriteSheet(pspriteSheet), id(id),
                totalDuration(sumDuration(framesBegin, framesEnd))
            {
                ensureNotEmpty();
            }

            Animation(const Animation&) = default;
            Animation(Animation&&) = default;
            
            Animation& operator=(const Animation&) = default;
            Animation& operator=(Animation&&) = default;

            inline U32 getId() const
            {
                return id;
            }

        private:
            inline Sprite frameToSprite(const Frame& frame) const
            {
                return Sprite(frame.rect, pspriteSheet);
            }

            inline size_t getFramesCount() const
            {
                return this->frames.size();
            }
            
            inline Frame getFrame(const size_t index) const
            {
                assert(/*index >= 0* &&*/ index < this->getFramesCount() && "Frame index out of bounds");

                return this->frames[index];
            }
            
        public:
            inline Sprite getSpriteAtFrame(const size_t index) const
            {
                return frameToSprite(getFrame(index));
            }
            
            
            inline U32 getTotalDuration() const
            {
                return this->totalDuration;
            }
            
            /**
             * Retrieve the frame at a particular point in time.
             * 
             * @param milliseconds Point in time (in milliseconds from the beginning of the animation).
             * This value gets wrapped around, if it's bigger than the total duration of the animation.
             * @return Animation frame at the given point in time.
             */
            Frame getFrameAtMs(const U32 milliseconds) const;
            
            inline Sprite getSpriteAtMs(const U32 milliseconds) const
            {
                return frameToSprite(getFrameAtMs(milliseconds));
            }
            
            ~Animation() = default;
        };

        class AnimationFactory final
        {
            static U32 nextId;

        public:

            template <typename FrameDataInputIt>
            Animation create(
                SDL_Texture* const pspriteSheet,
                FrameDataInputIt framesBegin, FrameDataInputIt framesEnd,
                const Animation::Direction direction
            ) const
            {
                std::vector<Animation::Frame> frames;

                // Put frames in the forward order (common for forward and pingpong directions)
                std::transform(
                    framesBegin, 
                    framesEnd,
                    std::back_inserter(frames),
                    [](const io::data::AnimationData::FrameData& frameData) {
                        return Animation::Frame(frameData.frame, frameData.duration);
                    }
                );

                // If ping-pong, append the middle frames in reverse
                if(direction == Animation::Direction::PING_PONG)
                {
                    for(auto i = frames.size() - 2; i > 0; i--)
                    {
                        frames.push_back(frames[i]);
                    }
                }
                else if(direction == Animation::Direction::REVERSE) // If reverse, then...
                {
                    std::reverse(frames.begin(), frames.end());
                }

                return Animation(std::move(frames), pspriteSheet, nextId++);
            }
        };
    }
}

#endif /* BLINK_GFX_ANIMATION_H */
