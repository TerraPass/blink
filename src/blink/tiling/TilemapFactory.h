#ifndef BLINK_GFX_TILEMAPFACTORY_H
#define BLINK_GFX_TILEMAPFACTORY_H

#include "io/data/TilemapData.h"

#include "Tilemap.h"
#include "MapTilesContainerFactory.h"
#include "TileLayerFactory.h"

namespace blink
{
    namespace tiling
    {
        class TilemapFactory final
        {
            const MapTilesContainerFactory mapTilesContainerFactory;
            const TileLayerFactory tileLayerFactory;

        public:
            explicit TilemapFactory()
            : mapTilesContainerFactory(), tileLayerFactory()
            {
                
            }

            // TODO: Accept an rvalue reference?
            Tilemap create(io::data::TilemapData&& data) const;
        };
    }
}

#endif /* BLINK_GFX_TILEMAPFACTORY_H */

