#ifndef BLINK_COMPONENTS_SPRITE_H
#define BLINK_COMPONENTS_SPRITE_H

#include <cassert>
#include <boost/optional.hpp>

#include <SDL.h>

#include "gfx/Sprite.h"

namespace blink
{
    namespace components
    {
        struct Sprite final
        {
            boost::optional<gfx::Sprite> sprite;

            Sprite()
            : sprite()
            {
                
            }
            
            explicit Sprite(const gfx::Sprite sprite)
            : sprite(sprite)
            {
                
            }

            inline gfx::Sprite& getSprite()
            {
                assert(sprite && "sprite must be present on call to getSprite()");

                return *sprite;
            }
        };
    }
}

#endif /* BLINK_COMPONENTS_SPRITE */
