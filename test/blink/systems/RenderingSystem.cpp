#ifndef BLINK_TESTING
#error "BLINK_TESTING must be defined when buidling tests"
#endif

#include <memory>

#include "catch.hpp"

#include "random.h"

// Preprocessor seams
#define BLINK_GFX_SPRITE_H

namespace blink
{    
    namespace gfx
    {
        struct Sprite {};
    }
}

#include "systems/RenderingSystem.h"

#include "components/Position.h"
#include "components/Sprite.h"
#include "gfx/Sprite.h"
#include "gfx/types.h"

using namespace blink;
using namespace blink::systems;
using namespace blink::components;

namespace
{
    struct MockRenderer
    {
        struct Sprite {};

    private:
        mutable size_t renderCalls;

    public:

        MockRenderer()
        :  renderCalls(0)
        {

        }
        virtual ~MockRenderer()
        {

        }

        void renderSprite(const gfx::Sprite& /*sprite*/, gfx::unit /*x*/, gfx::unit /*y*/) const
        {
            renderCalls++;
        }

        inline size_t getRenderCalls() const
        {
            return renderCalls;
        }
    };

    struct MockRenderPolicy final
    {
    private:
        mutable int numberOfCalls;
        mutable size_t lastRenderablesSize;

    public:
        MockRenderPolicy()
        : numberOfCalls(0), lastRenderablesSize(static_cast<size_t>(-1))
        {

        }

        template <typename Renderer,
            typename RenderablesContainer,
            typename PriorityComparer = std::less<typename RenderablesContainer::value_type>>
        inline void operator()(
            const Renderer& /*renderer*/,
            RenderablesContainer& renderables, 
            PriorityComparer /*lessPriority*/ = std::less<typename RenderablesContainer::value_type>()
        ) const
        {
            numberOfCalls++;
            lastRenderablesSize = renderables.size();
        }

        inline int getNumberOfCalls() const
        {
            return numberOfCalls;
        }

        inline size_t getLastRenderablesSize() const
        {
            return lastRenderablesSize;
        }
    };

    void addRenderableEntity(entityx::EntityManager& entities)
    {
        static const real minCoord = -10000.0;
        static const real maxCoord = 10000.0;

        auto entity = entities.create();
        entity.assign<Position>(randreal(minCoord, maxCoord), randreal(minCoord, maxCoord));
        entity.assign<Sprite>(gfx::Sprite());
    }
}

TEST_CASE("RenderingSystem calls RenderPolicy on update", "[systems][RenderingSystem]")
{
    const unsigned int seed = 712847;
    srand(seed);

    entityx::EntityX ex;

    using TestedRenderingSystem = RenderingSystem<MockRenderer, MockRenderPolicy>;
    auto prenderingSystem = std::make_shared<TestedRenderingSystem>();
    
    ex.systems.add(prenderingSystem);
    ex.systems.configure();

    const MockRenderer& mockRenderer = prenderingSystem->getRenderer();
    const MockRenderPolicy& mockRenderPolicy = prenderingSystem->getRenderPolicy();

    SECTION("RenderPolicy is called exactly once on update")
    {
        ex.systems.update<TestedRenderingSystem>(1.0);

        REQUIRE(mockRenderPolicy.getNumberOfCalls() == 1);
    }
    
    SECTION("RenderPolicy is called with empty renderables collection if there were no renderable entities")
    {
        ex.systems.update<TestedRenderingSystem>(1.0);

        REQUIRE(mockRenderPolicy.getLastRenderablesSize() == 0);
    }

    SECTION("Size of collection passed to RenderPolicy changes correctly with removal/addition of renderable entities")
    {
        const size_t renderablesToAdd = 500;
        for(size_t i = 0; i < renderablesToAdd; i++)
        {
            addRenderableEntity(ex.entities);
        }

        SECTION("RenderPolicy is passed a correct number of renderables after some renderable entities are added between updates")
        {
            ex.systems.update<TestedRenderingSystem>(1.0);

            REQUIRE(mockRenderPolicy.getLastRenderablesSize() == renderablesToAdd);
        }

        const size_t renderablesToDestroy = 249;
        const size_t renderablesToRemovePosition = 51;
        const size_t renderablesToRemoveSprite = 14;
        const size_t totalRemovedRenderables = renderablesToDestroy + renderablesToRemovePosition + renderablesToRemoveSprite;

        {
            size_t leftToDestroy = renderablesToDestroy;
            for(auto entity : ex.entities.entities_with_components<Position, Sprite>())
            {
                if(leftToDestroy == 0)
                {
                    break;
                }
                entity.destroy();
                leftToDestroy--;
            }

            size_t leftToRemovePosition = renderablesToRemovePosition;
            for(auto entity : ex.entities.entities_with_components<Position, Sprite>())
            {
                if(leftToRemovePosition == 0)
                {
                    break;
                }
                entity.component<Position>().remove();
                leftToRemovePosition--;
            }

            size_t leftToRemoveSprite = renderablesToRemoveSprite;
            for(auto entity : ex.entities.entities_with_components<Position, Sprite>())
            {
                if(leftToRemoveSprite == 0)
                {
                    break;
                }
                entity.component<Sprite>().remove();
                leftToRemoveSprite--;
            }
        }

        SECTION("RenderPolicy is passed a correct number of renderables after some renderable entities are removed between updates")
        {
            ex.systems.update<TestedRenderingSystem>(1.0);

            REQUIRE(mockRenderPolicy.getLastRenderablesSize() == renderablesToAdd - totalRemovedRenderables);
        }

        SECTION("RenderPolicy is passed a correct number of renderables after some renderable entities are removed and some are added")
        {
            const size_t newRenderablesToAdd = 22;
            for(size_t i = 0; i < newRenderablesToAdd; i++)
            {
                addRenderableEntity(ex.entities);
            }

            ex.systems.update<TestedRenderingSystem>(1.0);

            REQUIRE(mockRenderPolicy.getLastRenderablesSize() == renderablesToAdd - totalRemovedRenderables + newRenderablesToAdd);
        }
    }
}
