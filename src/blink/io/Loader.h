#ifndef BLINK_IO_LOADER_H
#define BLINK_IO_LOADER_H

#include <memory>
#include <string>

#include "debug/logging.h"
#include "debug/typeinfo.h"

#include "data/getDisplayName.h"

namespace blink
{
    namespace io
    {
        namespace detail
        {
            template <typename Key>
            struct OutputKeyResult
            {
                using type = const Key&;
            };

            template <>
            struct OutputKeyResult<BasicKey>
            {
                using type = std::string;
            };

            // outputKey() simply adds double quotes around std::string,
            // if it's used as Key.
            // Has no effect on other Key types.
            
            template <typename Key>
            inline typename OutputKeyResult<Key>::type outputKey(const Key& key)
            {
                return key;
            }

            template <>
            inline typename OutputKeyResult<BasicKey>::type outputKey<BasicKey>(const BasicKey& key)
            {
                return '\"' + key + '\"';
            }
        }
        
        template <typename Data, typename KeyResolver, typename RawDataRetriever, typename DataConverter, typename Key = BasicKey>
        class Loader final
        {
        public:
            //using KeyResolverPtr        = std::unique_ptr<KeyResolver>;
            //using RawDataProviderPtr    = std::shared_ptr<RawDataRetriever>;   // sic
            //using DataConverterPtr      = std::unique_ptr<DataConverter>;

        private:
            // Non-const to allow for move-construction of Loader
            KeyResolver keyResolver;
            RawDataRetriever rawDataRetriever;
            DataConverter dataConverter;

        public:
            Loader(
                KeyResolver keyResolver = KeyResolver(), 
                RawDataRetriever rawDataRetriever = RawDataRetriever(), 
                DataConverter dataConverter = DataConverter()
            )
            : keyResolver(std::move(keyResolver)), 
                rawDataRetriever(std::move(rawDataRetriever)), 
                dataConverter(std::move(dataConverter))
            {
                
            }

            Loader(const Loader&) = delete;
            Loader(Loader&&) = default; // sic, allow move-construction

            Loader& operator=(const Loader&) = delete;
            Loader& operator=(Loader&&) = delete;

            ~Loader() = default;

            Data load(const Key& key) const
            {
                BLINK_LOGF_I("Loading %1% for key %2%", data::getDisplayName<Data>() % detail::outputKey(key));
                return dataConverter.convert(rawDataRetriever.retrieve(keyResolver.resolve(key)));
            }
        };
    }
}

#endif /* BLINK_IO_LOADER_H */

