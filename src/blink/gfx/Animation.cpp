#include "Animation.h"

#include <unordered_map>

#include "debug/logging.h"

namespace blink
{
    namespace gfx
    {
        Animation::Frame Animation::getFrameAtMs(const U32 milliseconds) const
        {
            // TODO: Optimize to achieve 0(log(n)) complexity, instead of O(n).
            I32 ms = static_cast<I32>(milliseconds % getTotalDuration());   // ms can become negative in the loop, so it's signed
            for(size_t i = 0; i < getFramesCount() - 1; i++)
            {
                auto curFrame = getFrame(i);
                if(ms < static_cast<I32>(curFrame.duration))
                {
                    return curFrame;
                }

                ms -= curFrame.duration;
            }

            return getFrame(getFramesCount() - 1);
        }

        U32 AnimationFactory::nextId = 1;
    }
}
