#include "TilesetKeyResolver.h"

namespace blink
{
    namespace io
    {
        namespace resolvers
        {
            data::TilesetResolvedKey TilesetKeyResolver::resolve(const data::TilesetKey& key) const
            {
                return data::TilesetResolvedKey(
                    metadataPathResolver.resolve(key),
                    imagePathResolver.resolve(key)
                );
            }
        }
    }
}
