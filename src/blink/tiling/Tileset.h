#ifndef BLINK_GFX_TILESET_H
#define BLINK_GFX_TILESET_H

#include <vector>

#include <sdl2utils/pointers.h>

#include "Tile.h"
#include "types.h"

namespace blink
{
    namespace tiling
    {
        class Tileset final
        {
            sdl2utils::SDL_TexturePtr ptexture;
            const std::vector<Rect> tileRects;

            const pixelunit tileWidth;
            const pixelunit tileHeight;

        public:
            Tileset(
                sdl2utils::SDL_TexturePtr ptexture,
                std::vector<Rect>&& tiles,
                const pixelunit tileWidth,
                const pixelunit tileHeight
            );

            Tileset(const Tileset&) = delete;
            Tileset(Tileset&&) = default;   // sic

            Tileset& operator=(const Tileset&) = delete;
            Tileset& operator=(Tileset&&) = delete;

            inline Tile operator[](const size_t index) const
            {
                return makeTile(ptexture.get(), tileRects[index]);
            }

            inline Tile at(const size_t index) const
            {
                return makeTile(ptexture.get(), tileRects.at(index));
            }

            inline size_t getTileCount() const
            {
                return tileRects.size();
            }

            inline pixelunit getTileWidth() const
            {
                return tileWidth;
            }

            inline pixelunit getTileHeight() const
            {
                return tileHeight;
            }
        };
    }
}

#endif /* BLINK_GFX_TILESET_H */

