#ifndef BLINK_TESTING
#error "BLINK_TESTING must be defined when buidling tests"
#endif

#include "catch.hpp"

#include <vector>
#include <array>
#include <chrono>
#include <thread>

#include "common/random.h"

using namespace blink;

TEST_CASE("Random produces the same sequence when seeded with the same value", "[common][random]")
{
    const Seed seed = 76214UL;

    const size_t sequenceLength = 100;

    SECTION("Two Random<int> instances with the same seed produce the same sequence")
    {
        Random<int> rand0(seed);
        Random<int> rand1(seed);

        bool allSame = true;
        for(size_t i = 0; i < sequenceLength; i++)
        {
            if(rand0.next() != rand1.next())
            {
                allSame = false;
                break;
            }
        }

        REQUIRE(allSame);
    }

    SECTION("Two Random<real> instances with the same seed produce the same sequence")
    {
        Random<real> rand0(seed);
        Random<real> rand1(seed);

        bool allSame = true;
        for(size_t i = 0; i < sequenceLength; i++)
        {
            if(rand0.next() != rand1.next())
            {
                allSame = false;
                break;
            }
        }

        REQUIRE(allSame);
    }
}

TEST_CASE("Random produces different sequences for different seeds", "[common][random]")
{
    const Seed seed0 = 1852UL;
    const Seed seed1 = 95827UL;

    const size_t sequenceLength = 5;

    SECTION("Two Random<int> instances with different seeds produce different sequences")
    {
        Random<int> rand0(seed0);
        Random<int> rand1(seed1);

        bool allSame = true;
        for(size_t i = 0; i < sequenceLength; i++)
        {
            if(rand0.next() != rand1.next())
            {
                allSame = false;
                break;
            }
        }

        REQUIRE(!allSame);
    }

    SECTION("Two Random<real> instances with different seeds produce different sequences")
    {
        Random<real> rand0(seed0);
        Random<real> rand1(seed1);

        bool allSame = true;
        for(size_t i = 0; i < sequenceLength; i++)
        {
            if(rand0.next() != rand1.next())
            {
                allSame = false;
                break;
            }
        }

        REQUIRE(!allSame);
    }
}

template <typename Result>
bool testAutoSeededDifferent(const size_t sequenceLength)
{
    // Checking in triples to ensure that even Randoms created in the same second produce different sequences

    std::array<Random<Result>, 3> rands = {
        makeAutoSeededRandom<Result>(),
        makeAutoSeededRandom<Result>(),
        makeAutoSeededRandom<Result>()
    };

    std::array<std::vector<Result>, 3> seqs;

    for(size_t i = 0; i < 3; i++)
    {
        for(size_t j = 0; j < sequenceLength; j++)
        {
            seqs[i].emplace_back(rands[i].next());
        }
    }

    return seqs[0] != seqs[1] && seqs[0] != seqs[2] && seqs[1] != seqs[2];
}

TEST_CASE("Auto-seeded Random instances produce different sequences", "[common][random]")
{
    const size_t sequenceLength = 100;

    SECTION("Auto-seeded Random<int> instances produce different sequences")
    {
        REQUIRE(testAutoSeededDifferent<int>(sequenceLength));
    }

    SECTION("Auto-seeded Random<real> instances produce different sequences")
    {
        REQUIRE(testAutoSeededDifferent<real>(sequenceLength));
    }
}

template <typename Result, typename SleepDuration>
bool testNoDelayEffect(const Seed seed, const size_t sequenceLength, const SleepDuration sleepDuration)
{
    Random<Result> immediateRandom(seed);
    std::vector<Result> immediateSequence;
    immediateSequence.reserve(sequenceLength);
    for(size_t i = 0; i < sequenceLength; i++)
    {
        immediateSequence.emplace_back(immediateRandom.next());
    }

    Random<Result> delayedRandom(seed);
    for(size_t i = 0; i < sequenceLength; i++)
    {
        std::this_thread::sleep_for(sleepDuration);
        if(delayedRandom.next() != immediateSequence[i])
        {
            return false;
        }
    }

    return true;
}

TEST_CASE("Time between calls to next does not affect the returned value", "[common][random]")
{
    using namespace std::chrono_literals;

    const Seed seed = 74921UL;
    
    const size_t sequenceLength = 2;
    const auto sleepDuration = 50ms;

    SECTION("Random<int> is not affected by time between calls to next()")
    {
        REQUIRE(testNoDelayEffect<int>(seed, sequenceLength, sleepDuration));
    }

    SECTION("Random<real> is not affected by time between calls to next()")
    {
        REQUIRE(testNoDelayEffect<real>(seed, sequenceLength, sleepDuration));
    }
}

template <typename Result>
bool testWithinRange(const Seed seed, const size_t sequenceLength, const Result min, const Result max)
{
    Random<Result> rand(seed, min, max);

    for(size_t i = 0; i < sequenceLength; i++)
    {
        const auto value = rand.next();
        if(value < min || value > max)
        {
            return false;
        }
    }

    return true;
}

TEST_CASE("Random with uniform distribution produces values within range", "[common][random]")
{
    const Seed seed = 56621UL;
    
    const size_t sequenceLength = 1000;

    SECTION("Random<int> with uniform distribution does not produce values outside of the given range")
    {
        const int min = -15;
        const int max = 87;

        REQUIRE(testWithinRange<int>(seed, sequenceLength, min, max));
    }

    SECTION("Random<real> with uniform distribution does not produce values outside of the given range")
    {
        const real min = -0.5;
        const real max = 0.25;

        REQUIRE(testWithinRange<real>(seed, sequenceLength, min, max));
    }
}
