#ifndef BLINK_TIME_RESETTABLEEXECUTIONTIMER_H
#define BLINK_TIME_RESETTABLEEXECUTIONTIMER_H

#include "ExecutionTimer.h"
#include "IResettableTimer.h"


namespace blink
{
    namespace time
    {

#if _MSC_VER && !__INTEL_COMPILER
// Disable MSVC warning C4250 ("<class> inherits <member> via dominance").
// As far as I can tell, the following code is valid and the behaviour,
// described in the warning, is the only one that can reasonably be expected by the programmer.
// Therefore, said warning is pointless in this case.
#pragma warning( push )
#pragma warning( disable : 4250)
#endif

        class ResettableExecutionTimer : public ExecutionTimer, public virtual IResettableTimer
        {
        public:
            inline explicit ResettableExecutionTimer(const bool startPaused = false)
            : ExecutionTimer(startPaused)
            {
                
            }

            virtual void resetImpl(const bool startPaused) override;
        };

#if _MSC_VER && !__INTEL_COMPILER
#pragma warning(pop)
#endif

    }
}

#endif /* BLINK_TIME_RESETTABLEEXECUTIONTIMER_H */

