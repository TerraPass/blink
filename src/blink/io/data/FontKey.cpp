#include "FontKey.h"

#include <boost/format.hpp>

namespace blink
{
    namespace io
    {
        namespace data
        {
            using boost::str;
            using boost::format;

            std::ostream& operator<<(std::ostream& out, const FontKey& fontKey)
            {
                out << str(format("\"%1%\" (size: %2%)") % fontKey.getBasicKey() % fontKey.size);
                return out;
            }
        }
    }
}
