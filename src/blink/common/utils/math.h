#ifndef BLINK_COMMON_UTILS_MATH_H
#define BLINK_COMMON_UTILS_MATH_H

#include <cmath>
#include <limits>

#include "common/numeric.h"

namespace blink
{
    inline bool almostEqual(const real lhs, const real rhs, const real epsilon)
    {
        return std::abs(lhs - rhs) < epsilon;
    }

    inline bool almostEqual(const real lhs, const real rhs)
    {
        // TODO: Ensure that this is a valid use of epsilon()
        return almostEqual(lhs, rhs, std::numeric_limits<real>::epsilon());
    }

    template <typename T>
    inline T floorTo(const real value)
    {
        return static_cast<T>(std::floor(value));
    }

    template <typename Vector2, typename Rect>
    inline bool isInsideRect(const Vector2 position, const Rect rect)
    {
        return position.x >= rect.x && position.x < rect.x + rect.h
            && position.y >= rect.y && position.y < rect.y + rect.h;
    }
}

#endif /* BLINK_COMMON_UTILS_MATH_H */

