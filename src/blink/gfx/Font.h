#ifndef BLINK_GFX_FONT_H
#define BLINK_GFX_FONT_H

#include <array>

#include <sdl2utils/pointers.h>

#include "common/numeric.h"

#include "types.h"
#include "Sprite.h"

namespace blink
{
    namespace gfx
    {
        class Font final
        {
        public:
            enum class RenderMode
            {
                SOLID = 0,
                //SHADED = 1,
                BLENDED = 2
            };
            
            static constexpr const char MAX_PRINTABLE_CHAR = 126;   // '~'; 127 is DEL
            static constexpr const char MIN_PRINTABLE_CHAR = 32;    // ' ' (space)
            
            static constexpr const char CHAR_MAP_SIZE = MAX_PRINTABLE_CHAR - MIN_PRINTABLE_CHAR + 1;

            static constexpr const char UNKNOWN_CHAR_REPLACEMENT = '?';            
            
            using AtlasPtr = sdl2utils::SDL_TexturePtr;
            using CharMap = std::array<Rect, CHAR_MAP_SIZE>;

        private:
            const RenderMode renderMode;
            const U16 height;
            AtlasPtr patlas;    // non-const to allow for move-construction of Font
            CharMap charMap;    // ditto

        public:
            Font(const RenderMode renderMode, const U16 height, AtlasPtr&& patlas, CharMap&& charMap);

            Font(const Font&) = delete;
            Font(Font&&) = default; // sic
            
            Font& operator=(const Font&) = delete;
            Font& operator=(Font&&) = delete;

            inline U16 getHeight() const
            {
                return height;
            }
            
            inline RenderMode getRenderMode() const
            {
                return renderMode;
            }

            Sprite getSpriteForChar(const char ch) const;

        private:
            static inline int getCharMapIndex(const char ch)
            {
                return static_cast<int>(ch) - MIN_PRINTABLE_CHAR;
            }

        public:
            inline bool hasChar(const char ch) const
            {
                return (getCharMapIndex(ch) >= 0 || getCharMapIndex(ch) < static_cast<int>(charMap.size()));
            }
        };
    }
}

#endif /* BLINK_GFX_FONT_H */

