#ifndef BLINK_GFX_MAPTILESCONTAINERFACTORY_H
#define BLINK_GFX_MAPTILESCONTAINERFACTORY_H

#include <memory>

#include "io/data/TilesetData.h"

#include "MapTilesContainer.h"
#include "TilesetFactory.h"

namespace blink
{
    namespace tiling
    {
        class MapTilesContainerFactory final
        {
            const TilesetFactory tilesetFactory;

        public:

            MapTilesContainerFactory()
            : tilesetFactory()
            {
                
            }

            /**
             * Convert the provided vector of TilesetData instances into
             * MapTilesContainer, ready to be used by a Tilemap.
             * 
             * @param mapTilesetsData Vector of TilesetData. CAN be moved from, 
             * so shouldn't be used by the caller after create() has returned!
             * @return unique_ptr to MapTilesContainer for use in the new Tilemap.
             */
            std::unique_ptr<MapTilesContainer> create(std::vector<io::data::TilesetData>&& mapTilesetsData) const;
        };
    }
}

#endif /* BLINK_GFX_MAPTILESCONTAINERFACTORY_H */

