#ifndef BLINK_GFX_TILELAYERDATA_H
#define BLINK_GFX_TILELAYERDATA_H

#include <vector>
#include <string>
#include <stdexcept>

#include "types.h"

namespace blink
{
    namespace tiling
    {
        //template <template <typename...> class Container = std::vector>
        class TileLayerData final
        {
            template <typename... Ts>
            using Container = std::vector<Ts...>;

        public:
            using GidContainer = Container<tilegid>;

            static constexpr const I64 AUTO_DETERMINE = -1;

        private:
            /**
             * Stores map GIDs in row-major order
             */
            GidContainer gids;
            tileunit width;
            tileunit height;

            U64 version;

            template <typename T>
            static const T& ensureNonZero(const T& value, const std::string& valueName)
            {
                if(value == 0)
                {
                    throw std::invalid_argument(valueName + " must not be 0");
                }
                return value;
            }

            void ensureNonNegativeDimensions() const
            {
                if(width < 0 || height < 0)
                {
                    throw std::invalid_argument("Tilemap dimensions must not be negative");
                }
            }

        public:
            explicit TileLayerData(GidContainer&& gids, const tileunit width)
            : gids(std::move(gids)), width(width), height(static_cast<tileunit>(this->gids.size()/ensureNonZero(width, "width"))),
                version(0)
            {
                ensureNonNegativeDimensions();
            }

            template <typename InputIt>
            TileLayerData(InputIt gidsBegin, InputIt gidsEnd, const tileunit width)
            : gids(gidsBegin, gidsEnd), width(width), height(static_cast<tileunit>(gids.size()/ensureNonZero(width, "width"))),
                version(0)
            {
                ensureNonNegativeDimensions();
            }

            inline tileunit getWidth() const
            {
                return width;
            }

            inline tileunit getHeight() const
            {
                return height;
            }

        private:
            inline size_t toIndex(const tileunit x, const tileunit y) const
            {
                return y*width + x;
            }

        public:
            inline tilegid at(const tileunit x, const tileunit y) const
            {
                return gids.at(toIndex(x, y));
            }

            inline tilegid at(const TilePosition position) const
            {
                return gids.at(toIndex(position.x, position.y));
            }

            inline tilegid operator[](const TilePosition position) const
            {
                return gids[toIndex(position.x, position.y)];
            }

            inline void setAt(const tileunit x, const tileunit y, const tilegid gid)
            {
                gids.at(toIndex(x, y)) = gid;
                version++;
            }

            inline void setAt(const TilePosition position, const tilegid gid)
            {
                gids.at(toIndex(position.x, position.y)) = gid;
                version++;
            }

            inline U64 getVersion() const
            {
                return version;
            }
        };
    }
}

#endif /* BLINK_GFX_TILELAYERDATA_H */

