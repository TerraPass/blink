#ifndef BLINK_IO_RESOLVERS_FILEPATHRESOLVER_H
#define BLINK_IO_RESOLVERS_FILEPATHRESOLVER_H

#include <string>

#include "io/types.h"

namespace blink
{
    namespace io
    {
        namespace resolvers
        {
            class FilePathResolver
            {
                const std::string basePath;
                const std::string fileExtension;

            public:
                FilePathResolver(const std::string baseRelativePath, const std::string fileExtension);

                FilePath resolve(const BasicKey& key) const;
            };
        }
    }
}

#endif /* BLINK_IO_RESOLVERS_FILEPATHRESOLVER_H */

