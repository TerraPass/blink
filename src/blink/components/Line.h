#ifndef BLINK_COMPONENTS_LINE_H
#define BLINK_COMPONENTS_LINE_H

#include "common/types.h"
#include "gfx/types.h"

#include "BaseLine.h"

namespace blink
{
    namespace components
    {
        struct Line final : public BaseLine
        {
            static const gfx::Color DEFAULT_COLOR;

            gfx::Color color;

            Line()
            : BaseLine(), color(DEFAULT_COLOR)
            {
                
            }

            Line(const WorldPosition source, const WorldPosition target, const gfx::Color color = DEFAULT_COLOR, const bool ray = false)
            : BaseLine(source, target, ray), color(color)
            {
                
            }
        };
    }
}

#endif /* BLINK_COMPONENTS_LINE_H */

