#ifndef BLINK_TIME_SCALABLERESETTABLEEXECTIMER_H
#define BLINK_TIME_SCALABLERESETTABLEEXECTIMER_H

#include "IScalableResettableTimer.h"
#include "ResettableExecutionTimer.h"

#include "common/numeric.h"

namespace blink
{
    namespace time
    {

#if _MSC_VER && !__INTEL_COMPILER
// Disable MSVC warning C4250 ("<class> inherits <member> via dominance").
// As far as I can tell, the following code is valid and the behaviour,
// described in the warning, is the only one that can reasonably be expected by the programmer.
// Therefore, said warning is pointless in this case.
#pragma warning( push )
#pragma warning( disable : 4250)
#endif

        class ScalableResettableExecTimer 
            : public ResettableExecutionTimer, public virtual IScalableResettableTimer
        {
            static constexpr const real DEFAULT_SCALE = static_cast<real>(1.0);

            real scale;

        public:
            inline explicit ScalableResettableExecTimer(const bool startPaused = false, const real scale = DEFAULT_SCALE)
                : ResettableExecutionTimer(startPaused), scale(scale)
            {
                
            }

        private:
            inline virtual real getScaleImpl() const override
            {
                return this->scale;
            }

            virtual void setScaleImpl(const real scale) override;

            virtual bool pauseImpl() override;

            inline virtual real getElapsedSecondsImpl() const override
            {
                return toSeconds(
                    this->getPaused()
                        ? this->getAccumulatedDuration()
                        : this->getAccumulatedDuration() + (this->scale * (getCurrentTime() - this->getStartTime()))
                );
            }
        };

#if _MSC_VER && !__INTEL_COMPILER
#pragma warning(pop)
#endif

    }
}

#endif /* BLINK_TIME_SCALABLERESETTABLEEXECTIMER_H */

