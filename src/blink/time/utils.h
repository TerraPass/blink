#ifndef BLINK_TIME_UTILS_H
#define BLINK_TIME_UTILS_H

#include "common/numeric.h"

namespace blink
{
    namespace time
    {
        using namespace std::chrono;
        
        template <typename Duration>
        inline constexpr real toSeconds(const Duration& dur)
        {
            return duration<real>(dur).count();
        }
        
        template <typename Clock, typename Duration = typename Clock::duration>
        inline constexpr real secondsSince(const time_point<Clock, Duration>& tp)
        {
            return duration<real>(Clock::now() - tp).count();
        }

    }
}

#endif /* BLINK_TIME_UTILS_H */

