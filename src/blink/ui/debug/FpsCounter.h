#ifndef BLINK_UI_DEBUG_FPSCOUNTER_H
#define BLINK_UI_DEBUG_FPSCOUNTER_H

#include <array>
#include <limits>

#include <SDL.h>

#include "core/ITimeKeeper.h"
#include "common/numeric.h"
#include "camera/UiCamera.h"
#include "gfx/TextRenderer.h"

namespace blink
{
    namespace ui
    {
        namespace debug
        {
            // TODO: Refactor (separate responsibilities, maybe inject renderer etc. from outside)
            class FpsCounter final
            {
                static constexpr const size_t AVG_OVER_FRAMES = 10;
                static constexpr const real REFRESH_INTERVAL_SECONDS = 0.5;

                const core::ITimeKeeper& timeKeeper;
                gfx::TextRenderer<camera::UiCamera> textRenderer;
                const gfx::ScreenPosition screenPosition;
                const gfx::Font& font;

                // Circular buffer for AVG_OVER_FRAMES last frame times (negative values are ignored when calculating avg)
                std::array<real, AVG_OVER_FRAMES> frameTimes;

                size_t currentFrame;
                real lastFrameTime;
                
                real lastRefreshTime;

                real avgFrameSeconds;
            public:
                static const constexpr real USE_TIMER = -1.0f;

                FpsCounter(
                    const core::ITimeKeeper& timeKeeper,
                    SDL_Renderer* const prenderer,
                    const camera::UiCamera& uiCamera,
                    const gfx::ScreenPosition screenPosition,
                    const gfx::Font& font
                )
                : timeKeeper(timeKeeper), textRenderer(prenderer, uiCamera), 
                    screenPosition(screenPosition), font(font),
                    frameTimes(), currentFrame(0), 
                    lastFrameTime(timeKeeper.getRealtimeElapsedSeconds()),
                    lastRefreshTime(std::numeric_limits<double>::min()),
                    avgFrameSeconds(0.0)
                {
                    // Initially fill with negative values
                    frameTimes.fill(-1.0);
                }

                void render(const real deltaTime = USE_TIMER);
            };
        }
    }
}

#endif /* BLINK_UI_DEBUG_FPSCOUNTER_H */

