#ifndef BLINK_IO_RETRIEVAL_TILESETRETRIEVER_H
#define BLINK_IO_RETRIEVAL_TILESETRETRIEVER_H

#include <SDL.h>

#include "io/data/RawTilesetData.h"
#include "io/exceptions.h"

#include "JsonRetriever.h"
#include "TextureRetriever.h"

namespace blink
{
    namespace io
    {
        namespace retrieval
        {
            class RawTilesetDataRetriever final
            {
                JsonRetriever metadataRetriever;
                TextureRetriever imageRetriever;

            public:
                explicit RawTilesetDataRetriever(SDL_Renderer* const prenderer)
                : metadataRetriever(), imageRetriever(prenderer)
                {
                    
                }

                inline data::RawTilesetData retrieve(const data::TilesetResolvedKey& key) const try
                {
                    return data::RawTilesetData(
                        metadataRetriever.retrieve(key.metadataFilePath),
                        imageRetriever.retrieve(key.imageFilePath)
                    );
                }
                catch(const RawDataRetrievalException& e)
                {
                    throw detail::RawDataRetrievalExceptionImpl<data::RawTilesetData>(e.what());
                }
            };
        }
    }
}

#endif /* BLINK_IO_RETRIEVAL_TILESETRETRIEVER_H */

