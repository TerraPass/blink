#ifndef BLINK_IO_RESOLVERS_TILESETKEYRESOLVER_H
#define BLINK_IO_RESOLVERS_TILESETKEYRESOLVER_H

#include "io/data/TilesetKey.h"

#include "FilePathResolver.h"

namespace blink
{
    namespace io
    {
        namespace resolvers
        {
            class TilesetKeyResolver final
            {
                const FilePathResolver metadataPathResolver;
                const FilePathResolver imagePathResolver;

            public:
                TilesetKeyResolver(
                    const std::string metadataBaseRelativePath,
                    const std::string metadataFileExtension,
                    const std::string imageBaseRelativePath,
                    const std::string imageFileExtension
                )
                : metadataPathResolver(metadataBaseRelativePath, metadataFileExtension),
                    imagePathResolver(imageBaseRelativePath, imageFileExtension)
                {
                    
                }

                TilesetKeyResolver(
                    const std::string baseRelativePath,
                    const std::string metadataFileExtension,
                    const std::string imageFileExtension
                )
                : TilesetKeyResolver(baseRelativePath, metadataFileExtension, baseRelativePath, imageFileExtension)
                {
                    
                }

                data::TilesetResolvedKey resolve(const data::TilesetKey& key) const;
            };
        }
    }
}

#endif /* BLINK_IO_RESOLVERS_TILESETKEYRESOLVER_H */

