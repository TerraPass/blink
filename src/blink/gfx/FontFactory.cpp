#include "FontFactory.h"

#include <cassert>
#include <unordered_map>

#include <SDL.h>
#include <SDL_ttf.h>

#include <sdl2utils/guards.h>
#include <sdl2utils/pointers.h>

#include "debug/logging.h"

namespace blink
{
    namespace gfx
    {
        using namespace sdl2utils;
        using namespace sdl2utils::guards::abbr;
        using io::data::FontData;

        using RenderFunc = std::function<SDL_Surface*(TTF_Font*, const char* text, SDL_Color fg)>;

        static std::string getPrintableCharacters()
        {
            std::string printableChars;
            printableChars.reserve(Font::CHAR_MAP_SIZE);
            for(char i = Font::MIN_PRINTABLE_CHAR; i <= Font::MAX_PRINTABLE_CHAR; i++)
            {
                printableChars.push_back(i);
            }
            assert(printableChars.size() == Font::CHAR_MAP_SIZE);
            return printableChars;
        }

        const std::string FontFactory::PRINTABLE_CHARACTERS = getPrintableCharacters();

#if defined(__GNUC__) && !defined(__clang__) && __GNUC__ < 6
        // The following is a workaround for GCC bug #60970, related to a defect in C++11,
        // which prevented enum class values from being used as keys for unordered_map.
        // The defect was fixed in C++14, but GCC only implemented the fix in version 6.0.
        // See: https://stackoverflow.com/questions/18837857/cant-use-enum-class-as-unordered-map-key
        namespace detail
        {
            struct EnumClassHash
            {
                template <typename T>
                size_t operator()(T value) const
                {
                    return static_cast<size_t>(value);
                }
            };
        }

        const std::unordered_map<Font::RenderMode, RenderFunc, detail::EnumClassHash> renderFuncs = {
#else
        const std::unordered_map<Font::RenderMode, RenderFunc> renderFuncs = {
#endif
            {Font::RenderMode::SOLID, TTF_RenderText_Solid},
            {Font::RenderMode::BLENDED, TTF_RenderText_Blended}
        };

        FontFactory::FontFactory(SDL_Renderer * const prenderer)
        : prenderer(prenderer)
        {

        }

        Font FontFactory::create(const FontData& data, const Font::RenderMode renderMode, const Color color) const
        {
            // FIXME: The folowing logic will only work properly for monospace fonts!
            // If characters have different bounding rects, the text rendered from 
            // the produced font atlas will be corrupted!

            const auto pfont = data.fontPtr.get();

#if BLINK_LOG_LEVEL >= BLINK_LOG_ERROR
            // Test the font for monospaceness by sizing up '.' and 'W' and comparing rects.
            {
                int dotW = -1;
                int dotH = -1;
                int wW = -1;
                int wH = -1;
                z(TTF_SizeText(pfont, ".", &dotW, &dotH));
                z(TTF_SizeText(pfont, "W", &wW, &wH));
                BLINK_LOGF_E_IF(
                    dotW != wW || dotH != wH,
                    "This is not a monospace font (\'.\': %1%x%2%, \'W\': %3%x%4%); rendered text might be corrupted",
                    dotW % dotH % wW % wH
                );
            }
#endif

            SDL_SurfacePtr patlasSurf(renderFuncs.at(renderMode)(pfont, PRINTABLE_CHARACTERS.c_str(), color));
            SDL_TexturePtr patlas(SDL_CreateTextureFromSurface(prenderer, patlasSurf.get()));

            int atlasW = -1;
            int atlasH = -1;
            z(SDL_QueryTexture(patlas.get(), nullptr, nullptr, &atlasW, &atlasH));

            assert(atlasW > 0 && atlasH > 0 && "atlas width and height must be positive");

            // FIXME: This is where we break on non-monospace fonts!
            const size_t charW = static_cast<size_t>(atlasW / Font::CHAR_MAP_SIZE);
            BLINK_LOG_E_IF(
                atlasW % Font::CHAR_MAP_SIZE != 0,
                "Non-zero remainder from dividing font atlas width by character map size"
            );

            Font::CharMap charMap;
            for(size_t i = 0; i < charMap.size(); i++)
            {
                auto& charRect = charMap[i];
                charRect.x = static_cast<gfx::unit>(i*charW);
                charRect.y = 0;
                charRect.w = static_cast<gfx::unit>(charW);
                charRect.h = atlasH;
            }

            return Font(renderMode, static_cast<U16>(atlasH), std::move(patlas), std::move(charMap));
        }
    }
}
