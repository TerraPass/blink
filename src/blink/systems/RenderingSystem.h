#ifndef BLINK_SYSTEMS_RENDERINGSYSTEM_H
#define BLINK_SYSTEMS_RENDERINGSYSTEM_H

#include <vector>
#include <cmath>
#include <cassert>

#include <entityx/entityx.h>

#include "common/numeric.h"
#include "common/types.h"
#include "components/Position.h"
#include "components/Sprite.h"
#include "debug/testability.h"
#include "sdl/SDLWindow.h"

#include "rendering/sortingRender.h"

namespace blink
{
    namespace systems
    {
        template <typename Renderer, typename RenderPolicy>
        class RenderingSystem final 
            : public entityx::System<RenderingSystem<Renderer, RenderPolicy>>, 
                public entityx::Receiver<RenderingSystem<Renderer, RenderPolicy>>
        {
            struct Renderable final
            {
                entityx::Entity entity;
                entityx::ComponentHandle<components::Position> hposition;
                entityx::ComponentHandle<components::Sprite> hsprite;

                Renderable(
                    const entityx::Entity& entity, 
                    const entityx::ComponentHandle<components::Position>& hposition,
                    const entityx::ComponentHandle<components::Sprite>& hsprite
                )
                : entity(entity), hposition(hposition), hsprite(hsprite)
                {

                }

                inline typename Renderer::Sprite& getSprite()
                {
                    return this->hsprite->getSprite();
                }

                inline WorldPosition getPosition() const
                {
                    return *hposition;
                }

                inline bool isValid() const
                {
                    return entity && hposition && hsprite;
                }
            };

            const Renderer spriteRenderer;
            const RenderPolicy renderPolicy;
            
            // Collection of renderables.
            // It's maintained sorted in ascending order of getPriority(renderable) values
            // by the update() method.
            std::vector<Renderable> renderables;

        public:
            static constexpr const size_t DEFAULT_INITIAL_CAPACITY = 2048;

            explicit RenderingSystem(Renderer renderer = {}, const size_t initialCapacity = DEFAULT_INITIAL_CAPACITY);

            RenderingSystem(const RenderingSystem&) = delete;
            RenderingSystem(RenderingSystem&&) = default;   // sic
            
            RenderingSystem& operator=(const RenderingSystem&) = delete;
            RenderingSystem& operator=(RenderingSystem&&) = delete;

            virtual ~RenderingSystem() = default;

            virtual void configure(entityx::EntityManager& entities, entityx::EventManager& events) override;
            virtual void update(entityx::EntityManager& entities, entityx::EventManager& events, entityx::TimeDelta dt) override;

            void receive(const entityx::ComponentAddedEvent<components::Position>& event);
            void receive(const entityx::ComponentAddedEvent<components::Sprite>& event);

        private:

            static inline real getPriority(const Renderable& renderable)
            {
                assert(renderable.entity && renderable.hposition && "renderable must be valid");
                return renderable.hposition->y; // TODO: Return "sq. distance to camera"?
            }

            static inline bool lessPriority(const Renderable& renderable0, const Renderable& renderable1)
            {
                return getPriority(renderable0) < getPriority(renderable1);
            }

            // A Renderable is invalid and should be removed from renderables
            // if any of the required components is missing or the entity has been destroyed.
            static inline bool shouldBeRemoved(const Renderable& renderable)
            {
                return !renderable.isValid();
            }

            BLINK_TESTING_ONLY(
            BLINK_TESTABLE_PRIVATE:
                inline const Renderer& getRenderer() const
                {
                    return this->spriteRenderer;
                }

                inline const RenderPolicy& getRenderPolicy() const
                {
                    return this->renderPolicy;
                }
            )
        };

        template <typename Renderer>
        using SortingRenderingSystem = RenderingSystem<Renderer, rendering::SortingRender>;
    }
}

#ifndef BLINK_COMPILE_TEMPLATES
#include "RenderingSystem.cxx"
#endif

#endif /* BLINK_SYSTEMS_RENDERINGSYSTEM_H */

