#ifndef BLINK_IO_CONVERTERS_RAWTILESETDATACONVERTER_H
#define BLINK_IO_CONVERTERS_RAWTILESETDATACONVERTER_H

#include "io/data/RawTilesetData.h"
#include "io/data/TilesetData.h"
#include "io/exceptions.h"

#include "JsonToTilesetMetadataConverter.h"
#include "TextureToTextureDataConverter.h"

namespace blink
{
    namespace io
    {
        namespace converters
        {
            class RawTilesetDataConverter final
            {
                const JsonToTilesetMetadataConverter metadataConverter;
                const TextureToTextureDataConverter textureConverter;

            public:
                RawTilesetDataConverter()
                : metadataConverter(), textureConverter()
                {

                }

                inline data::TilesetData convert(data::RawTilesetData rawData) const try
                {
                    return data::TilesetData(
                        textureConverter.convert(std::move(rawData.ptexture)),
                        metadataConverter.convert(rawData.metadataJson)
                    );
                }
                catch(const DataConversionException& e)
                {
                    throw detail::DataConversionExceptionImpl<data::RawTilesetData, data::TilesetData>(e.what());
                }
            };
        }
    }
}

#endif /* BLINK_IO_CONVERTERS_RAWTILESETDATACONVERTER_H */
