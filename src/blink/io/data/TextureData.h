#ifndef BLINK_IO_DATA_TEXTUREDATA_H
#define BLINK_IO_DATA_TEXTUREDATA_H

#include <SDL.h>
#include <sdl2utils/pointers.h>

namespace blink
{
    namespace io
    {
        namespace data
        {
            struct TextureData final
            {
                static constexpr const auto DISPLAY_NAME = "texture";

                using Texture = SDL_Texture;
                using TexturePtr = sdl2utils::SDL_TexturePtr;

                TexturePtr texturePtr;

                explicit TextureData(SDL_Texture* const ptexture)
                : texturePtr(ptexture)
                {

                }

                explicit TextureData(sdl2utils::SDL_TexturePtr ptexture)
                : texturePtr(std::move(ptexture))
                {

                }
            };
        }
    }
}

#endif /* BLINK_IO_DATA_TEXTUREDATA_H */

