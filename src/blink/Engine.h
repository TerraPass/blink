#ifndef BLINK_ENGINE_H
#define BLINK_ENGINE_H

#include <cassert>
#include <stdexcept>

#include "sdl/ScopedSDLSystem.h"
#include "sdl/SDLWindow.h"
#include "core/ITimeKeeper.h"
#include "core/IGameLoop.h"
#include "time/ExecutionTimer.h"
#include "input/InputSystem.h"
#include "core/ContentManager.h"

namespace blink
{
    class Engine final
    {
#ifndef NDEBUG
      static bool alreadyInstantiated;
#endif

      const sdl::ScopedSDLSystem sdlSystem;
      sdl::SDLWindow sdlWindow;

      // TODO: These shared pointers are not needed here, because the lifetime of Engine
      // will obviously contain lifetimes of all of the game's systems,
      // therefore unique_ptr + raw pointers for dependencies would work just as well.
      const std::shared_ptr<core::ITimeKeeper> ptimeKeeper;
      const std::shared_ptr<input::InputSystem> pinputSystem;
      const std::shared_ptr<core::ContentManager> pcontentManager;  // TODO: Create an interface for ContentManager.
      const std::unique_ptr<core::IGameLoop> pgameLoop;

    public:
        class InitializationFailedException;

        Engine();

        ~Engine();

        void runGameLoop();

    private:
        Engine(const Engine&) = delete;
        Engine(Engine&&) = delete;
        
        Engine& operator=(const Engine&) = delete;
        Engine& operator=(Engine&&) = delete;
    };

    class Engine::InitializationFailedException : public std::runtime_error
    {
        static const constexpr auto MESSAGE_FORMAT = "Failed to initialize Blink engine: %1%";

    public:
        InitializationFailedException(const std::string& detail);
    };
}

#endif /* BLINK_ENGINE_H */

