#ifndef BLINK_TEST_RANDOM_H
#define BLINK_TEST_RANDOM_H

#include <cstdlib>

template <typename Real>
inline Real randreal()
{
    return static_cast<Real>(rand())/static_cast<Real>(RAND_MAX);
}

template <typename Real>
inline Real randreal(const Real min, const Real max)
{
    return min + randreal<Real>()*(max - min);
}

#endif /* BLINK_TEST_RANDOM_H */

