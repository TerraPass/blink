#include "SDLWindow.h"

#include <cassert>
#include <boost/format.hpp>

#include <sdl2utils/exceptions.h>
#include <sdl2utils/guards.h>
#include <SDL_render.h>

namespace blink
{
    namespace sdl
    {
        using namespace sdl2utils::guards::abbr;

        template <typename Ptr>
        static inline Ptr ensureNotNull(Ptr pointer, const std::string& pointerName)
        {
            if(pointer == nullptr)
            {
                throw sdl2utils::SDLErrorException(
                    true,
                    boost::str(
                        boost::format("SDLWindow ctor failed to create %1%") % pointerName
                    )
                );
            }
            return pointer;
        }
        
        SDLWindow::SDLWindow(
            const std::string& title,
            const int width,
            const int height,
            const Uint32 sdlWindowFlags,
            const Uint32 sdlRendererFlags
        ) : pwindow(
                ensureNotNull(
                    SDL_CreateWindow(
                        title.c_str(),
                        WINDOW_X,
                        WINDOW_Y,
                        width,
                        height,
                        sdlWindowFlags
                    ),
                    "SDL_Window"
                )
            ), 
            prenderer(
                ensureNotNull(
                    SDL_CreateRenderer(pwindow.get(), -1, sdlRendererFlags),
                    "SDL_Renderer for an SDL_Window"
                )
            )
        {

        }

        int SDLWindow::getWidth() const
        {
            int result = -1;
            z(SDL_GetRendererOutputSize(this->getRendererPtr(), &result, nullptr), "SDL_GetRendererOutputSize() failed");
            assert(result > 0);
            return result;
        }

        int SDLWindow::getHeight() const
        {
            int result = -1;
            z(SDL_GetRendererOutputSize(this->getRendererPtr(), nullptr, &result), "SDL_GetRendererOutputSize() failed");
            assert(result > 0);
            return result;
        }

        void SDLWindow::trapMouseCursor(const bool value)
        {
            SDL_SetWindowGrab(pwindow.get(), (value ? SDL_TRUE : SDL_FALSE));
        }
    }
}