#ifndef BLINK_CORE_GAMEWORLD_H
#define BLINK_CORE_GAMEWORLD_H

#include <entityx/entityx.h>
#include <sdl2utils/pointers.h>

#include "camera/CameraSystem.h"
#include "sdl/SDLWindow.h"
#include "entities/SparkFactory.h"
#include "input/InputSystem.h"
#include "ui/debug/FpsCounter.h"

#include "IGameWorld.h"
#include "ITimeKeeper.h"

namespace blink
{
    namespace core
    {
        class GameWorld final : public IGameWorld
        {
            static constexpr const size_t NUMBER_OF_ENTITIES = 100;
            
            static constexpr const real MAX_VELOCITY_COORD = 25.0;

            const sdl::SDLWindow& window;
            const ContentManager& contentManager;
            core::ITimeKeeper& timeKeeper;

            camera::CameraSystem cameraSystem;
            
            entityx::EntityX ex;
            
            entities::SparkFactory sparkFactory;

            ui::debug::FpsCounter fpsCounter;
            
        public:
            GameWorld(const sdl::SDLWindow& window, const ContentManager& contentManager, core::ITimeKeeper& timeKeeper, input::InputSystem& inputSystem);

            GameWorld(const GameWorld&) = delete;
            GameWorld(GameWorld&&) = delete;
            
            GameWorld& operator=(const GameWorld&) = delete;
            GameWorld& operator=(GameWorld&&) = delete;

            virtual ~GameWorld() = default;

        private:
            virtual void updateWorldImpl(const real deltaTime) override;
            virtual void renderWorldImpl(const real deltaTime) override;
        };
    }
}

#endif /* BLINK_CORE_GAMEWORLD_H */
