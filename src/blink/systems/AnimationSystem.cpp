#include "AnimationSystem.h"

#include <cmath>
#include <cassert>

#include "components/Sprite.h"
#include "components/Animation.h"
#include "debug/logging.h"

namespace blink
{
    namespace systems
    {
        using namespace entityx;
        using namespace blink::components;        
        
        AnimationSystem::AnimationSystem(const core::ITimeKeeper& timeKeeper)
        : timeKeeper(timeKeeper)
        {
            
        }
        
        void AnimationSystem::update(EntityManager& entities, EventManager& /*events*/, TimeDelta dt)
        {
            static const real MS_PER_SECOND = 1000.0;

            auto currentGameplayTime = timeKeeper.getGameplayElapsedSeconds();
            
            entities.each<Animation, Sprite>(
                [this, dt, currentGameplayTime] (Entity /*entity*/, Animation& animation, Sprite& sprite) {
                    // TODO: Optimize by requesting and reassigning the actual frame sprite
                    // only when it's different (has different index) than the current frame.

                    auto& animationImpl = animation.getAnimation();
                    
                    const real gameplayTimeDelta = currentGameplayTime - animation.lastUpdateGameplayTime;
                    assert(gameplayTimeDelta > 0 && "gameplayTimeDelta must be positive");
                    const real animTimeDelta = gameplayTimeDelta*MS_PER_SECOND;

                    const real currentAnimTime = animation.lastUpdateAnimTime + animTimeDelta;
                    sprite.sprite = animationImpl.getSpriteAtMs(static_cast<U32>(std::floor(currentAnimTime)));
                    
                    animation.lastUpdateAnimTime = currentAnimTime; // TODO: Wrap around?
                    animation.lastUpdateGameplayTime = currentGameplayTime;
                }
            );
        }
    }
}
