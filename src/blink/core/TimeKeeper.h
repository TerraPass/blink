#ifndef TIMEKEEPER_H
#define TIMEKEEPER_H

#include "ITimeKeeper.h"

#include "common/numeric.h"

#include "time/ExecutionTimer.h"
#include "time/ScalableResettableExecTimer.h"

namespace blink
{
    namespace core
    {
        class TimeKeeper : public virtual ITimeKeeper
        {
            const time::ExecutionTimer realTimer;
            time::ScalableResettableExecTimer gameplayTimer;
            
        public:
            inline TimeKeeper(const bool startWithGameplayPaused = false)
            : realTimer(), gameplayTimer(startWithGameplayPaused)
            {
                
            }

        private:
            inline real getRealtimeElapsedSecondsImpl() const override
            {
                return this->realTimer.getElapsedSeconds();
            }

            inline real getGameplayElapsedSecondsImpl() const override
            {
                return this->gameplayTimer.getElapsedSeconds();
            }
        };
    }
}

#endif /* TIMEKEEPER_H */

