#ifndef BLINK_INPUT_MOUSESUBSCRIBER_H
#define BLINK_INPUT_MOUSESUBSCRIBER_H

#include <boost/optional.hpp>

#include "InputSystem.h"

namespace blink
{
    namespace input
    {
        class MouseClickSubscriber final
        {
            InputSystem& inputSystem;

            common::ScopedSubscription mouseButtonDownSub;

            boost::optional<MousePosition> maybeLmbClickPosition;
            boost::optional<MousePosition> maybeRmbClickPosition;

        public:
            explicit MouseClickSubscriber(InputSystem& inputSystem);

            MouseClickSubscriber(const MouseClickSubscriber&);
            MouseClickSubscriber(MouseClickSubscriber&&);

            MouseClickSubscriber& operator=(const MouseClickSubscriber&) = delete;
            MouseClickSubscriber& operator=(MouseClickSubscriber&&) = delete;

            inline void clearClicks()
            {
                maybeLmbClickPosition = boost::none;
                maybeRmbClickPosition = boost::none;
            }

            inline boost::optional<MousePosition> getLmbClickPosition() const
            {
                return maybeLmbClickPosition;
            }

            inline boost::optional<MousePosition> getRmbClickPosition() const
            {
                return maybeRmbClickPosition;
            }

        private:
            void onMouseButtonDown(input::InputSystem&, input::MouseEventArgs args);
        };
    }
}

#endif /* BLINK_INPUT_MOUSESUBSCRIBER_H */

