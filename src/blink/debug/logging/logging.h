#ifndef BLINK_DEBUG_LOGGING_H
#define BLINK_DEBUG_LOGGING_H

#include <boost/log/trivial.hpp>
#include <boost/format.hpp>

#include "config/constants.h"

#define BLINK_LOG_NONE       0
#define BLINK_LOG_FATAL      1
#define BLINK_LOG_ERROR      2
#define BLINK_LOG_WARNING    3
#define BLINK_LOG_INFO       4
#define BLINK_LOG_DEBUG      5
#define BLINK_LOG_TRACE      6

#define BLINK_DEFAULT_LOG_LEVEL BLINK_LOG_INFO

#ifndef BLINK_LOG_LEVEL
#define BLINK_LOG_LEVEL BLINK_DEFAULT_LOG_LEVEL
#endif

// Fatal
#if BLINK_LOG_LEVEL >= BLINK_LOG_FATAL
#define BLINK_LOG_F(X) BLINK_LOG_DETAILED_IMPL(fatal) << X
#define BLINK_LOGF_F(formatStr, args) BLINK_LOG_F(boost::str(boost::format(formatStr) % args))
#define BLINK_LOG_F_IF(condition, X) if(condition) { BLINK_LOG_F(X); }
#define BLINK_LOGF_F_IF(condition, formatStr, args) if(condition) { BLINK_LOGF_F(formatStr, args); }
#else
#define BLINK_LOG_F(X) (void)0
#define BLINK_LOGF_F(formatStr, args) (void)0
#define BLINK_LOG_F_IF(condition, X) (void)0
#define BLINK_LOGF_F_IF(condition, formatStr, args) (void)0
#endif

// Error
#if BLINK_LOG_LEVEL >= BLINK_LOG_ERROR
#define BLINK_LOG_E(X) BLINK_LOG_DETAILED_IMPL(error) << X
#define BLINK_LOGF_E(formatStr, args) BLINK_LOG_E(boost::str(boost::format(formatStr) % args))
#define BLINK_LOG_E_IF(condition, X) if(condition) { BLINK_LOG_E(X); }
#define BLINK_LOGF_E_IF(condition, formatStr, args) if(condition) { BLINK_LOGF_E(formatStr, args); }
#else
#define BLINK_LOG_E(X) (void)0
#define BLINK_LOGF_E(formatStr, args) (void)0
#define BLINK_LOG_E_IF(condition, X) (void)0
#define BLINK_LOGF_E_IF(condition, formatStr, args) (void)0
#endif

// Warning
#if BLINK_LOG_LEVEL >= BLINK_LOG_WARNING
#define BLINK_LOG_W(X) BLINK_LOG_DETAILED_IMPL(warning) << X
#define BLINK_LOGF_W(formatStr, args) BLINK_LOG_W(boost::str(boost::format(formatStr) % args))
#define BLINK_LOG_W_IF(condition, X) if(condition) { BLINK_LOG_W(X); }
#define BLINK_LOGF_W_IF(condition, formatStr, args) if(condition) { BLINK_LOGF_W(formatStr, args); }
#else
#define BLINK_LOG_W(X) (void)0
#define BLINK_LOGF_W(formatStr, args) (void)0
#define BLINK_LOG_W_IF(condition, X) (void)0
#define BLINK_LOGF_W_IF(condition, formatStr, args) (void)0
#endif

// Info
#if BLINK_LOG_LEVEL >= BLINK_LOG_INFO
#define BLINK_LOG_I(X) BLINK_LOG_IMPL(info) << X
#define BLINK_LOGF_I(formatStr, args) BLINK_LOG_I(boost::str(boost::format(formatStr) % args))
#define BLINK_LOG_I_IF(condition, X) if(condition) { BLINK_LOG_I(X); }
#define BLINK_LOGF_I_IF(condition, formatStr, args) if(condition) { BLINK_LOGF_I(formatStr, args); }
#else
#define BLINK_LOG_I(X) (void)0
#define BLINK_LOGF_I(formatStr, args) (void)0
#define BLINK_LOG_I_IF(condition, X) (void)0
#define BLINK_LOGF_I_IF(condition, formatStr, args) (void)0
#endif

// Debug
#if BLINK_LOG_LEVEL >= BLINK_LOG_DEBUG
#define BLINK_LOG_D(X) BLINK_LOG_IMPL(debug) << X
#define BLINK_LOGF_D(formatStr, args) BLINK_LOG_D(boost::str(boost::format(formatStr) % args))
#define BLINK_LOG_D_IF(condition, X) if(condition) { BLINK_LOG_D(X); }
#define BLINK_LOGF_D_IF(condition, formatStr, args) if(condition) { BLINK_LOGF_D(formatStr, args); }
#else
#define BLINK_LOG_D(X) (void)0
#define BLINK_LOGF_D(formatStr, args) (void)0
#define BLINK_LOG_D_IF(condition, X) (void)0
#define BLINK_LOGF_D_IF(condition, formatStr, args) (void)0
#endif

// Trace
#if BLINK_LOG_LEVEL >= BLINK_LOG_TRACE
#define BLINK_LOG_T(X) BLINK_LOG_IMPL(trace) << X
#define BLINK_LOGF_T(formatStr, args) BLINK_LOG_T(boost::str(boost::format(formatStr) % args))
#define BLINK_LOG_T_IF(condition, X) if(condition) { BLINK_LOG_T(X); }
#define BLINK_LOGF_T_IF(condition, formatStr, args) if(condition) { BLINK_LOGF_T(formatStr, args); }
#else
#define BLINK_LOG_T(X) (void)0
#define BLINK_LOGF_T(formatStr, args) (void)0
#define BLINK_LOG_T_IF(condition, X) (void)0
#define BLINK_LOGF_T_IF(condition, formatStr, args) (void)0
#endif

namespace blink
{
    namespace debug
    {
        namespace detail
        {
            // The following basename_index() function was taken from
            // the following answer on StackOverflow by Chetan Reddy:
            // https://stackoverflow.com/a/19004720/1051764
            constexpr int32_t basename_index (const char * const path, const int32_t index = 0, const int32_t slash_index = -1)
            {
                return path[index]
                    ? ((path[index] == '/' || path[index] == '\\')
                        ? basename_index (path, index + 1, index)
                        : basename_index (path, index + 1, slash_index)
                      )
                    : (slash_index + 1);
            }

            // The following template is used to force a constexpr calculation
            // to be performed at compile time.
            // Since parameters for a template instantiation must be known at compile time,
            // wrapping a call to constexpr function into require_at_compile_time<> ensures
            // that said function will be called at compile time.
            template <int32_t Value>
            struct require_at_compile_time
            {
                static constexpr const int32_t value = Value;
            };
        }
    }
}

// Using requre_at_compile_time<> to force the compiler to calculate basename_index()
// at compile time, instead of deferring it to runtime.
#define BLINK_FILENAME (__FILE__ + blink::debug::detail::require_at_compile_time<blink::debug::detail::basename_index(__FILE__)>::value)

#define BLINK_LOG_IMPL(level) BOOST_LOG_TRIVIAL(level) << '[' << BLINK_FILENAME << ':' << __LINE__ << "] "
#define BLINK_LOG_DETAILED_IMPL(level) BLINK_LOG_IMPL(level)

#endif /* BLINK_DEBUG_LOGGING_H */

