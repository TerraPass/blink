#include "FullTileLayerRenderer.h"

#include "camera/UiCamera.h"

#include "SpriteRenderer.h"

namespace blink
{
    namespace gfx
    {
        constexpr const ScreenPosition FullTileLayerRenderer::DEFAULT_OFFSET;

        namespace
        {
            using ScaleOnlyCamera = camera::UiCamera;
        }

        void FullTileLayerRenderer::renderLayer(const tiling::TileLayer& tileLayer, const U8 scale, const ScreenPosition offset) const
        {
            using tiling::tileunit;

            static ScaleOnlyCamera camera(0, 0);
            static SpriteRenderer<ScaleOnlyCamera> spriteRenderer(prenderer, camera);

            camera.setScale(scale);

            for(tileunit y = 0; y < tileLayer.getHeight(); y++)
            {
                for(tileunit x = 0; x < tileLayer.getWidth(); x++)
                {
                    const auto maybeTile = tileLayer.atMaybe(x, y);
                    if(maybeTile)
                    {
                        spriteRenderer.renderSprite(
                            maybeTile->getSprite(),
                            offset + ScreenPosition{static_cast<unit>(maybeTile->getWidth()*x), static_cast<unit>(maybeTile->getHeight()*y)}
                        );
                    }
                }
            }
        }
    }
}
