#ifndef BLINK_GFX_LINERENDERER_H
#define BLINK_GFX_LINERENDERER_H

#include <SDL.h>
#include <sdl2utils/raii.h>

#include "camera/TopDownCamera.h"

#include "types.h"

namespace blink
{
    namespace gfx
    {
        template <typename Camera>
        class LineRenderer final
        {
            SDL_Renderer* prenderer;
            const Camera& camera;

        public:
            LineRenderer(SDL_Renderer* prenderer, const Camera& camera)
            : prenderer(prenderer), camera(camera)
            {

            }

            void renderLine(const typename Camera::InputPosition source, const typename Camera::InputPosition target, const bool ray, const Color color) const
            {
                const auto screenSource = camera.toScreenPosition(source);
                const auto screenTarget = camera.toScreenPosition(
                    ray
                        ? target + (target - source)*1000.0 // FIXME: Hack: extending the segment far enough for it to reach the screen bounds
                        : target
                );

                sdl2utils::raii::ScopedRenderDrawColor srdc(prenderer, color);

                SDL_RenderDrawLine(prenderer, screenSource.x, screenSource.y, screenTarget.x, screenTarget.y);
            }
        };

        using TopDownLineRenderer = LineRenderer<camera::TopDownCamera>;
    }
}

#endif /* BLINK_GFX_LINERENDERER_H */

