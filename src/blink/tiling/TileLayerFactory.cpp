#include "TileLayerFactory.h"

#include <stdexcept>
#include <boost/format.hpp>

#include "debug/typeinfo.h"

namespace blink
{
    namespace tiling
    {
        TileLayer TileLayerFactory::create(const io::data::TilemapData::LayerData& data, ConstMapTilesContainerPtr pmapTilesContainer) const
        {
            using boost::str;
            using boost::format;

            if(data.type != io::data::TilemapData::LayerData::Type::TILE)
            {
                throw std::invalid_argument(str(format("%1% expects layer data of type TILE") % debug::getTypename(*this)));
            }

            return TileLayer(
                pmapTilesContainer,
                TileLayerData(std::vector<tilegid>(data.data), static_cast<tileunit>(data.width)),
                data.opacity,
                data.visible
            );
        }
    }
}
