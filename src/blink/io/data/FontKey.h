#ifndef BLINK_IO_DATA_FONTKEY_H
#define BLINK_IO_DATA_FONTKEY_H

#include <iostream>

#include "io/types.h"
#include "io/data/FontData.h"

namespace blink
{
    namespace io
    {
        namespace data
        {
            namespace detail
            {
                template <typename Inner>
                struct BaseFontKey
                {
                protected:
                    const Inner inner;
                    
                public:
                    const FontData::size size;

                    BaseFontKey(const Inner& inner, FontData::size size)
                    : inner(inner), size(size)
                    {

                    }

                protected:
                    ~BaseFontKey() = default;
                };
            }

            struct FontKey final : public detail::BaseFontKey<BasicKey>
            {
                using detail::BaseFontKey<BasicKey>::BaseFontKey;

                inline const BasicKey& getBasicKey() const
                {
                    return inner;
                }
            };

            std::ostream& operator<<(std::ostream& out, const FontKey& fontKey);

            struct FontResolvedKey final : public detail::BaseFontKey<FilePath>
            {
                using detail::BaseFontKey<FilePath>::BaseFontKey;

                inline const FilePath& getFilePath() const
                {
                    return inner;
                }
            };
        }
    }
}

#endif /* BLINK_IO_DATA_FONTKEY_H */

