#ifndef BLINK_COMMON_NUMERIC_H
#define BLINK_COMMON_NUMERIC_H

#include <SDL_types.h>

namespace blink
{
    // May be aliased to either float or double,
    // allowing to trade off performance for accuracy.
    using real = double;

    // Portable integral types
    using U8    = Uint8;
    using I8    = Sint8;
    using U16   = Uint16;
    using I16   = Sint16;
    using U32   = Uint32;
    using I32   = Sint32;
    using U64   = Uint64;
    using I64   = Sint64;
}

#endif /* BLINK_COMMON_NUMERIC_H */

