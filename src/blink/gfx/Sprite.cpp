#include "Sprite.h"

#include "utils.h"

namespace blink
{
    namespace gfx
    {
        Sprite::Sprite(SDL_Texture* const pspriteTexture)
            : rect(getEntireTextureRect(pspriteTexture)), pspriteSheet(pspriteTexture)
        {
            
        }
    }
}
