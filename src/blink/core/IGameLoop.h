#ifndef BLINK_CORE_IGAMELOOP_H
#define BLINK_CORE_IGAMELOOP_H

namespace blink
{
    namespace core
    {
        class IGameLoop
        {
        public:
            IGameLoop() = default;
            virtual ~IGameLoop() = default;
            
            void operator()()
            {
                this->run();
            }
            
            void requestExit()
            {
                this->requestExitImpl();
            }
            
        private:
            virtual void run() = 0;
            virtual void requestExitImpl() = 0;
        };
    }
}

#endif /* BLINK_CORE_IGAMELOOP_H */

