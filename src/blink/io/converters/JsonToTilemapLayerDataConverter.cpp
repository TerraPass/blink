#include "JsonToTilemapLayerDataConverter.h"

#include <vector>
#include <algorithm>
#include <functional>
#include <boost/any.hpp>

#include <nlohmann_json.hpp>

#include "debug/logging.h"
#include "io/exceptions.h"

#include "utils.h"

namespace blink
{
    namespace io
    {
        namespace converters
        {
            const std::string JsonToTilemapLayerDataConverter::LAYER_TYPE_TILE = "tilelayer";
            const std::string JsonToTilemapLayerDataConverter::LAYER_TYPE_OBJECT = "objectgroup";

            JsonToTilemapLayerDataConverter::LayerData JsonToTilemapLayerDataConverter::convert(const nlohmann::json& layerJson) const try
            {
                using namespace std::placeholders;

                LayerData data;

                data.rawType = layerJson.at("type");
                data.type = (data.rawType == LAYER_TYPE_TILE)
                    ? LayerData::Type::TILE
                    : ((data.rawType == LAYER_TYPE_OBJECT)
                        ? LayerData::Type::OBJECT
                        : LayerData::Type::UNKNOWN);

                data.name = layerJson.at("name");
                data.visible = layerJson.at("visible");
                data.x = layerJson.at("x");
                data.y = layerJson.at("y");
                data.opacity = layerJson.at("opacity");

                if(data.type == LayerData::Type::TILE)
                {
                    data.width = layerJson.at("width");
                    data.height = layerJson.at("height");

                    auto dataJson = layerJson.at("data");
                    data.data.insert(data.data.cbegin(), dataJson.cbegin(), dataJson.cend());
                }
                else if(data.type == LayerData::Type::OBJECT)
                {
                    auto objectsJson = layerJson.at("objects");
                    std::transform(
                        objectsJson.cbegin(),
                        objectsJson.cend(),
                        std::back_inserter(data.objects),
                        std::bind(&JsonToTilemapObjectDataConverter::convert, objectDataConverter, _1)
                    );

                    data.draworder = layerJson.at("draworder");
                }
                else
                {
                    BLINK_LOGF_W("Encountered unknown tilemap layer type \"%1%\"", data.rawType);
                }

                readTiledProperties(data.properties, layerJson, false);

                return data;
            }
            catch(const std::out_of_range& e)
            {
                throw detail::DataConversionExceptionImpl<nlohmann::json, LayerData>(e.what());
            }
            catch(const std::domain_error& e)
            {
                throw detail::DataConversionExceptionImpl<nlohmann::json, LayerData>(e.what());
            }
        }
    }
}
