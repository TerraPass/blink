#include "ContentManager.h"

#include <iterator>

#include "gfx/Animation.h"
#include "io/types.h"
#include "io/data/FontKey.h"

namespace blink
{
    namespace core
    {
        using namespace blink::io;

        const data::FontKey ContentManager::FPS_COUNTER_FONT_KEY(
            "Inconsolata",
            14
        );
        const gfx::Color ContentManager::FPS_COUNTER_FONT_COLOR = {0xFF, 0xFF, 0xFF, 0xFF}; // white
        
        ContentManager::ContentManager(SDL_Renderer * const prenderer)
        : textureDataLoader(makeTextureDataLoader(prenderer)), 
                animDataLoader(makeAnimationDataLoader()),
                fontDataLoader(makeFontDataLoader()),
                tilemapDataLoader(makeTilemapDataLoader(prenderer)),
                animatedSpriteFactory(),
                fontFactory(prenderer),
                tilemapFactory(),
                sparkAnimatedSprite(animatedSpriteFactory.create(animDataLoader.load(SPARK_KEY), textureDataLoader.load(SPARK_KEY).texturePtr)),
                fpsCounterFont( // FIXME: Hardcoded (and poorly placed) values!
                    fontFactory.create(
                        fontDataLoader.load(FPS_COUNTER_FONT_KEY),
                        FPS_COUNTER_FONT_RENDER_MODE,
                        FPS_COUNTER_FONT_COLOR
                    )
                ),
                testTilemap(tilemapFactory.create(tilemapDataLoader.load(TEST_TILEMAP_KEY)))
        {

        }
    }
}
