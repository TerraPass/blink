#ifndef BLINK_IO_DATA_EXTRACTBASICKEY_H
#define BLINK_IO_DATA_EXTRACTBASICKEY_H

namespace blink
{
    namespace io
    {
        namespace data
        {
            template <typename Key>
            inline const BasicKey& extractBasicKey(const Key& key)
            {
                return key.getBasicKey();
            }

            template <>
            inline const BasicKey& extractBasicKey<BasicKey>(const BasicKey& key)
            {
                return key;
            }
        }
    }
}

#endif /* BLINK_IO_DATA_EXTRACTBASICKEY_H */

