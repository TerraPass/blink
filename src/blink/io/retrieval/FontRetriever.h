#ifndef BLINK_IO_RETRIEVAL_FONTRETRIEVER_H
#define BLINK_IO_RETRIEVAL_FONTRETRIEVER_H

#include "io/data/FontData.h"
#include "io/data/FontKey.h"

namespace blink
{
    namespace io
    {
        namespace retrieval
        {
            class FontRetriever final
            {
            public:
                using FontPtr = data::FontData::FontPtr;

                FontPtr retrieve(const data::FontResolvedKey& resolvedFontKey) const;
            };
        }
    }
}

#endif /* BLINK_IO_RETRIEVAL_FONTRETRIEVER_H */

