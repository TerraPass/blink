#ifndef BLINK_GFX_ANIMATEDSPRITE_H
#define BLINK_GFX_ANIMATEDSPRITE_H

#include <string>

#include <sdl2utils/pointers.h>

#include "AnimationContainer.h"

namespace blink
{
    namespace gfx
    {
        // TODO: Naming! This is a temporary name.
        // It may be taken to mean that AnimatedSprite and Sprite
        // may be used interchangeably, while actually
        // AnimatedSprite owns the spritesheet and a collection of a particular
        // "2D model"'s animations, whereas Sprite is merely a lightweight view
        // of a particular frame of the spritesheet.
        class AnimatedSprite final
        {
            // Shared ptr is not needed, because Animation instances
            // will exist as long as AnimatedSprite and so
            // a raw pointer will be sufficient for them.
            sdl2utils::SDL_TexturePtr pspriteSheet;
            const AnimationContainer animations;

        public:
            AnimatedSprite(sdl2utils::SDL_TexturePtr pspriteSheet, AnimationContainer&& animations)
            : pspriteSheet(std::move(pspriteSheet)), animations(std::move(animations))
            {

            }

            AnimatedSprite(const AnimatedSprite&) = delete;
            AnimatedSprite(AnimatedSprite&&) = default; // sic

            AnimatedSprite& operator=(const AnimatedSprite&) = delete;
            AnimatedSprite& operator=(AnimatedSprite&&) = delete;

            inline Animation getAnimation(const std::string& key) const
            {
                return animations.at(key);
            }

            inline Sprite getFirstSprite() const
            {
                return animations.cbegin()->second.getSpriteAtFrame(0);
            }
        };
    }
}

#endif /* BLINK_GFX_ANIMATEDSPRITE_H */

