#ifndef BLINK_IO_DATA_ANIMATIONDATA_H
#define BLINK_IO_DATA_ANIMATIONDATA_H

#include <vector>
#include <string>

#include <SDL.h>

#include "common/numeric.h"

namespace blink
{
    namespace io
    {
        namespace data
        {
            struct AnimationData final
            {
                static constexpr const auto DISPLAY_NAME = "animation data";

                struct FrameData final
                {
                    using Rect = SDL_Rect;

                    Rect frame;
                    U32 duration;

                    FrameData() = default;

                    FrameData(const Rect frame, const U32 duration)
                    : frame(frame), duration(duration)
                    {

                    }
                };

                struct MetaData final
                {
                    struct FrameTagData final
                    {
                        std::string name;
                        size_t from;
                        size_t to;
                        std::string direction;

                        FrameTagData() = default;

                        FrameTagData(const std::string& name, const size_t from, const size_t to, const std::string& direction)
                        : name(name), from(from), to(to), direction(direction)
                        {

                        }
                    };

                    std::string image;
                    std::string format;
                    U32 sizeW;
                    U32 sizeH;
                    std::vector<FrameTagData> frameTags;

                    MetaData() = default;

                    template <typename FrameTagDataInputIt>
                    MetaData(
                        const std::string& image,
                        const std::string& format,
                        const U32 sizeW,
                        const U32 sizeH,
                        FrameTagDataInputIt frameTagsBegin, FrameTagDataInputIt frameTagsEnd
                    )
                    : image(image), format(format), sizeW(sizeW), sizeH(sizeH), frameTags(frameTagsBegin, frameTagsEnd)
                    {

                    }

                    MetaData(const std::string& image, const std::string& format, const U32 sizeW, const U32 sizeH)
                    : image(image), format(format), sizeW(sizeW), sizeH(sizeH), frameTags()
                    {

                    }
                };

                std::vector<FrameData> frames;
                MetaData meta;

                AnimationData() = default;

                template <typename FrameDataInputIt>
                AnimationData(FrameDataInputIt framesBegin, FrameDataInputIt framesEnd, const MetaData& meta)
                : frames(framesBegin, framesEnd), meta(meta)
                {

                }
            };
        }
    }
}

#endif /* BLINK_IO_DATA_ANIMATIONDATA_H */

