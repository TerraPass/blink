#ifndef BLINK_GFX_FONTFACTORY_H
#define BLINK_GFX_FONTFACTORY_H

#include <string>

#include "io/data/FontData.h"

#include "Font.h"

namespace blink
{
    namespace gfx
    {
        class FontFactory final
        {
            static const std::string PRINTABLE_CHARACTERS;

            SDL_Renderer* const prenderer;

        public:
            explicit FontFactory(SDL_Renderer* const prenderer);

            Font create(const io::data::FontData& data, const Font::RenderMode renderMode, const Color color) const;
        };
    }
}

#endif /* BLINK_GFX_FONTFACTORY_H */

