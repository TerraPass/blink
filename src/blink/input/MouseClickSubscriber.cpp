#include "MouseClickSubscriber.h"

namespace blink
{
    namespace input
    {
        MouseClickSubscriber::MouseClickSubscriber(InputSystem& inputSystem)
        : inputSystem(inputSystem), mouseButtonDownSub(
            inputSystem.getMouseButtonDownEvent().subscribe(&MouseClickSubscriber::onMouseButtonDown, this)
        )
        {

        }

        MouseClickSubscriber::MouseClickSubscriber(const MouseClickSubscriber& other)
        : inputSystem(other.inputSystem), mouseButtonDownSub(
            inputSystem.getMouseButtonDownEvent().subscribe(&MouseClickSubscriber::onMouseButtonDown, this)
        )
        {

        }

        MouseClickSubscriber::MouseClickSubscriber(MouseClickSubscriber&& other)
        : inputSystem(other.inputSystem), mouseButtonDownSub(
            inputSystem.getMouseButtonDownEvent().subscribe(&MouseClickSubscriber::onMouseButtonDown, this)
        )
        {
            other.mouseButtonDownSub.cancel();
        }

        void MouseClickSubscriber::onMouseButtonDown(input::InputSystem&, input::MouseEventArgs args)
        {
            if(args.button == input::MouseButton::LEFT)
            {
                maybeLmbClickPosition = args.position;
            }
            else if(args.button == input::MouseButton::RIGHT)
            {
                maybeRmbClickPosition = args.position;
            }
        }
    }
}
