#ifndef BLINK_CAMERA_ICAMERASYSTEM_H
#define BLINK_CAMERA_ICAMERASYSTEM_H

namespace blink
{
    namespace camera
    {
        class TopDownCamera;
        class UiCamera;

        class ICameraSystem
        {            
        public:
            inline TopDownCamera& getTopDownCamera()
            {
                return getTopDownCameraImpl();
            }

            inline const TopDownCamera& getTopDownCamera() const
            {
                return getTopDownCameraImpl();
            }
            
            inline UiCamera& getUiCamera()
            {
                return getUiCameraImpl();
            }

            inline const UiCamera& getUiCamera() const
            {
                return getUiCameraImpl();
            }

            virtual ~ICameraSystem() = default;

        private:
            virtual TopDownCamera& getTopDownCameraImpl() = 0;
            virtual const TopDownCamera& getTopDownCameraImpl() const = 0;
            
            virtual UiCamera& getUiCameraImpl() = 0;
            virtual const UiCamera& getUiCameraImpl() const = 0;
        };
    }
}

#endif /* BLINK_CAMERA_ICAMERASYSTEM_H */

