#include "MovementSystem.h"

#include "components/Position.h"
#include "components/Velocity.h"

namespace blink
{
    namespace systems
    {
        using namespace entityx;
        using namespace blink::components;
        
        void MovementSystem::update(EntityManager& entities, EventManager& /*events*/, TimeDelta dt)
        {
            entities.each<Position, Velocity>(
                [dt](Entity /*entity*/, Position& position, Velocity& velocity) {
                    position.x += static_cast<real>(dt*velocity.x);
                    position.y += static_cast<real>(dt*velocity.y);
                }
            );
        }
    }
}
