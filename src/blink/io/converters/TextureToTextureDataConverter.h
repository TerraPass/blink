#ifndef BLINK_IO_CONVERTERS_TEXTURETOTEXTUREDATACONVERTER_H
#define BLINK_IO_CONVERTERS_TEXTURETOTEXTUREDATACONVERTER_H

#include "io/data/TextureData.h"

namespace blink
{
    namespace io
    {
        namespace converters
        {
            class TextureToTextureDataConverter final
            {
            public:
                inline data::TextureData convert(sdl2utils::SDL_TexturePtr ptexture) const
                {
                    return data::TextureData(std::move(ptexture));
                }
            };
        }
    }
}

#endif /* BLINK_IO_CONVERTERS_TEXTURETOTEXTUREDATACONVERTER_H */

