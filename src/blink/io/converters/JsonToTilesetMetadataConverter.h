#ifndef BLINK_IO_CONVERTERS_JSONTOTILESETMETADATACONVERTER_H
#define BLINK_IO_CONVERTERS_JSONTOTILESETMETADATACONVERTER_H

#include <nlohmann_json.hpp>

#include "io/data/TilesetData.h"

namespace blink
{
    namespace io
    {
        namespace converters
        {
            // TODO: Replace with from_json() global function in io::data namespace?
            class JsonToTilesetMetadataConverter final
            {
            public:
                data::TilesetMetadata convert(const nlohmann::json& metaJson) const;
            };
        }
    }
}

#endif /* BLINK_IO_CONVERTERS_JSONTOTILESETMETADATACONVERTER_H */

