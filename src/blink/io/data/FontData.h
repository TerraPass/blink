#ifndef BLINK_IO_DATA_FONTDATA_H
#define BLINK_IO_DATA_FONTDATA_H

#include <memory>

#include <SDL_ttf.h>

//#include "sdl/pointers.h"

namespace blink
{
    namespace io
    {
        namespace data
        {
            struct FontData final
            {
                static constexpr const auto DISPLAY_NAME = "font";

                //using FontPtr = sdl::TTFFontSharedPtr;
                using FontPtr = std::unique_ptr<TTF_Font, void(*)(TTF_Font*)>;
                using size = int;

                FontPtr fontPtr;
                //size size;

                explicit FontData(FontPtr pfont/*, const size size*/);

                explicit FontData(TTF_Font* const pfont/*, const size size*/)
                : FontData(FontPtr(pfont, TTF_CloseFont)/*, size*/)
                {

                }
            };
        }
    }
}

#endif /* BLINK_IO_DATA_FONTDATA_H */

